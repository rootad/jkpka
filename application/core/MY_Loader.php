<?php
class MY_Loader extends CI_Loader {
    public function template($template_name, $vars = array(), $return = FALSE) {
        if($return):
            $content  = $this->view('templates/header', $vars, $return);
            $content .= $this->view($template_name, $vars, $return);
            $content .= $this->view('templates/footer', $vars, $return);

            return $content;
        else:
            $this->view('templates/header', $vars);
            $this->view($template_name, $vars);
            $this->view('templates/footer', $vars);
        endif;
    }

    public function template_admin($template_name, $vars = array(), $return = FALSE) {
        $CI =& get_instance();
        $CI->load->library('session');

        $data_ueser = array(
            "id" => $CI->session->userdata('id'),
            "name" => $CI->session->userdata('name'),
            "email" => $CI->session->userdata('email')
        );

        $vars = array_merge($vars, $data_ueser);

        if($return):
            $content  = $this->view('templates/admin/header', $vars, $return);
            $content .= $this->view($template_name, $vars, $return);
            $content .= $this->view('templates/admin/footer', $vars, $return);

            return $content;
        else:
            $this->view('templates/admin/header', $vars);
            $this->view($template_name, $vars);
            $this->view('templates/admin/footer', $vars);
        endif;
    }

    public function template_member($template_name, $vars = array(), $return = FALSE) {
        $CI =& get_instance();
        $CI->load->library('session');

        $data_ueser = array(
            "id" => $CI->session->userdata('member_id'),
            "name" => $CI->session->userdata('member_name'),
            "email" => $CI->session->userdata('member_email')
        );

        $vars = array_merge($vars, $data_ueser);

        if($return):
            $content  = $this->view('templates/member/header', $vars, $return);
            $content .= $this->view($template_name, $vars, $return);
            $content .= $this->view('templates/member/footer', $vars, $return);

            return $content;
        else:
            $this->view('templates/member/header', $vars);
            $this->view($template_name, $vars);
            $this->view('templates/member/footer', $vars);
        endif;
    }
}
