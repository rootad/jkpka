<?php
class MY_Controller extends CI_Controller {
    public function __construct() {
       parent::__construct();
       
       date_default_timezone_set('Asia/Jakarta');
    }
}

class Member_Controller extends MY_Controller {
    public function __construct() {
       parent::__construct();

       $logged_in = $this->session->userdata('member_logged_in');

       if (!$logged_in) {
		       redirect("member/login", 'location');
       }
    }
}

class Admin_Controller extends MY_Controller {
    public function __construct() {
       parent::__construct();

       $logged_in = $this->session->userdata('logged_in');

       if (!$logged_in) {
		       redirect("admin/login", 'location');
       }
    }
}
