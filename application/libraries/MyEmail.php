<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MyEmail {

	protected $CI;

	public function __construct() {
		$this->CI =& get_instance();
		$this->CI->load->library("MyEncrypt");
		$this->CI->load->library("Mailgun");
	}

	public function sendVerifyEmail($email, $name) {
		// $configEmailNoReplay = array();
		// $configEmailNoReplay['protocol'] = 'ssmtp';
		// $configEmailNoReplay['smtp_host'] = 'ssl://tambakbayan.idwebhost.com';
		// $configEmailNoReplay['smtp_user'] = 'noreplay@jkpka.com';
		// $configEmailNoReplay['smtp_pass'] = "vw9P4CdQrRV3";
		// $configEmailNoReplay['smtp_port'] = "465";
		// $configEmailNoReplay['smtp_timeout'] = '7';
		// $configEmailNoReplay['charset'] = 'utf-8';
		// $configEmailNoReplay['newline'] = "\r\n";
		// $configEmailNoReplay['mailtype'] = 'html';
		// $configEmailNoReplay['validation'] = TRUE;

		// $this->CI->email->initialize($configEmailNoReplay);

		// $this->CI->email->from('noreplay@jkpka.com', 'JKPKA');
		// $this->CI->email->to($email);
		

		$base = $this->CI->myencrypt->base64url_encode($email);
		$hash = password_hash($email, PASSWORD_DEFAULT);

		$this->CI->session->set_tempdata($email, $hash, 86400);

		// $this->CI->email->subject('Selamat datang di JKPKA');
		$message = '<!DOCTYPE html>
		<html lang="id" dir="ltr">
		<head>
		<meta charset="utf-8">
		<title></title>
		</head>
		<body>
		<div class="">
		<img src="http://jkpka.com/media/images/logo.png" alt="Logo JKPKA">
		<br>
		<hr>
		<br>
		<p>Hallo ' . $name . ', akun anda berhasil didaftarkan. Silakan virifikasi akun anda dengan membuka link dibawah ini</p>
		<p><a href="' . $this->CI->config->item("base_url") . 'member/Register/verify?code=' . $base . '">' . $this->CI->config->item("base_url") . 'member/Register/verify?code=' . $base . '</a></p>
		<p>Segera aktivasi, link kadarluarsa 24 jam setelah email dikirim.</p>
		</div>
		</body>
		</html>';
		// $this->CI->email->message($message);

		$this->CI->mailgun
			->to($email)
			->from("noreplay@jkpka.com")
			->subject("Selamat datang di JKPKA")
			->message($message)
			->send();

		// $this->CI->email->send();
		// print_r($this->CI->email->print_debugger());
		// die();
	}

	public function sendForgotEmail($email, $name) {
		// $configEmailNoReplay = array();
		// $configEmailNoReplay['protocol'] = 'ssmtp';
		// $configEmailNoReplay['smtp_host'] = 'ssl://tambakbayan.idwebhost.com';
		// $configEmailNoReplay['smtp_user'] = 'noreplay@jkpka.com';
		// $configEmailNoReplay['smtp_pass'] = "vw9P4CdQrRV3";
		// $configEmailNoReplay['smtp_port'] = "465";
		// $configEmailNoReplay['smtp_timeout'] = '7';
		// $configEmailNoReplay['charset'] = 'utf-8';
		// $configEmailNoReplay['newline'] = "\r\n";
		// $configEmailNoReplay['mailtype'] = 'html';
		// $configEmailNoReplay['validation'] = TRUE;

		// $this->CI->email->initialize($configEmailNoReplay);

		// $this->CI->email->from('noreplay@jkpka.com', 'JKPKA');
		// $this->CI->email->to($email);

		$base = $this->CI->myencrypt->base64url_encode($email);
		$hash = password_hash($email, PASSWORD_DEFAULT);

		$this->CI->session->set_userdata($email, $hash);

		// $this->CI->email->subject('JKPKA, instruksi email lupa password');

		$message = '<!DOCTYPE html>
		<html lang="id" dir="ltr">
		<head>
		<meta charset="utf-8">
		<title></title>
		</head>
		<body>
		<div class="">
		<img src="http://jkpka.com/media/images/logo.png" alt="Logo JKPKA">
		<br>
		<hr>
		<br>
		<p>Hallo ' . $name . '. Silakan isi formulir password dengan membuka link dibawah ini</p>
		<p><a href="' . $this->CI->config->item("base_url") . 'member/ForgotPassword?code=' . $base . '">' . $this->CI->config->item("base_url") . 'member/ForgotPassword?code=' . $base . '</a></p>
		<p>Link kadarluarsa 24 jam setelah email dikirim.</p>
		</div>
		</body>
		</html>';

		$this->CI->email->message($message);

		$this->CI->mailgun
			->to($email)
			->from("noreplay@jkpka.com")
			->subject("JKPKA, instruksi email lupa password")
			->message($message)
			->send();

		// $this->CI->email->send();
		// print_r($this->CI->email->print_debugger());
		// die();
	}
}
