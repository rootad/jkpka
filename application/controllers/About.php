<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	public function index() {
		$this->load->template(
			'pages/about', 
			array(
				"title" => "JKPKA | Siapa Kami",
				"page" => "about"
			)
		);
	}

	public function activities() {
		$this->load->template(
			'pages/activities', 
			array(
				"title" => "JKPKA | Kegiatan Kami",
				"page" => "activities"
			)
		);
	}

	public function achievement() {
		$this->load->template(
			'pages/achievement', 
			array(
				"title" => "JKPKA | Pencapaian Kami",
				"page" => "achievement"
			)
		);
	}

	public function partner() {
		$this->load->template(
			'pages/partner', 
			array(
				"title" => "JKPKA | Partner Kami",
				"page" => "partner"
			)
		);
	}
}
