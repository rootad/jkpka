<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

	public function __construct() {
       parent::__construct();
       $this->load->model('MonitoringModel');
    }

 //    [
 //      { y: '2006', a: 100, b: 90 },
 //      { y: '2007', a: 75,  b: 65 },
 //      { y: '2008', a: 50,  b: 40 },
 //      { y: '2009', a: 75,  b: 65 },
 //      { y: '2010', a: 50,  b: 40 },
 //      { y: '2011', a: 75,  b: 65 },
 //      { y: '2012', a: 100, b: 90 }
	// ]

	public function index() {
		$monitoringGraph = array(
			"query" => array(
				0 => array(
					"y" => "2016",
					"a" => "100",
					"b" => "90",
				),
				1 => array(
					"y" => "2007",
					"a" => "75",
					"b" => "65",
				),
			),
			"xkey" => "y",
			"ykeys" => array("a", "b"),
			"labels" => array("Series A", "Series B"),
		);
		$this->load->template_admin(
			'pages/admin/dashboard',
			array(
				"title" => "JKPKA",
				"page" => "dashboard",
				"dataFooter" => array("monitoringGraph" => $monitoringGraph),
			)
		);
	}

	public function sign_out() {
		$this->session->unset_userdata(array('id', 'email', 'name'));

		$this->session->set_userdata('logged_in', FALSE);

		redirect("admin", 'location');
	}

	public function data() {
		$request = array_merge($_POST, $_GET);
		$draw = $request["draw"];
		$start = $request["start"];
		$length = $request["length"];
		$column = $request["order"][0]["column"];
		$dir = $request["order"][0]["dir"];
		$search = $request["search"]["value"];
		$columns = $request["columns"];

		$result = $this->MonitoringModel->get_monitoring_list(0, 10, $column, $dir, $search, $columns);
		$result_wo_limit = $this->MonitoringModel->get_monitoring_list(1, 10, $column, $dir, $search, $columns);

		$data = array();

		$arr = array(
			"cacing_larva" => "Cacing Larva",
			"larva_mrutu_biasa" => "Larva Mrutu Biasa",
			"belatung_ekor_tikus" => "Belatung Ekor Tikus",
			"lintah" => "Lintah",
			"kepiting_sungai" => "Kepiting Sungai",
			"kerang" => "Kerang",
			"siput_tanpa_pintu" => "Siput Tanpa 'Pintu'",
			"nimfa_capung_jarum_ekor_tebal" => "Nimfa Capung Jarum Ekor Tebal",
			"nimfa_capung_dobson" => "Nimfa Capung Dobson",
			"nimfa_capung_sialid" => "Nimfa Capung Sialid",
			"nimfa_lalat_sehari_perenang" => "Nimfa Lalat Sehari Perenang",
			"larva_lalat_atau_nyamuk_lainnya" => "Larva Lalat atau Nyamuk Lainnya",
			"cacing_pipih" => "Cacing Pipih",
			"larva_kumbang" => "Larva Kumbang",
			"kumbang_dewasa" => "Kumbang Dewasa",
			"kepik_pejalan_kaki" => "Kepik - Pejalan Kaki",
			"anggang_anggang" => "Anggang - Anggang",
			"kepik_perenang_punggung" => "Kepik Perenang Punggung",
			"kepik_pendayung" => "Kepik Pendayung",
			"kepik_air_lainnya" => "Kepik Air Lainnya",
			"siput_berpintu" => "Siput Berpintu, lebih besar dari 15 mm",
			"kijing" => "Kijing",
			"limpet_air_tawar" => "Limpet Air Tawar",
			"nimfa_capung_biasa" => "Nimfa Capung Biasa",
			"nimfa_capung_jarum_lainnya" => "Nimfa Capung Jarum lainnya",
			"larva_ulat_air_tanpa_kantung" => "Larva Ulat Air (Tanpa Kantung)",
			"larva_ulat_kantung_air_kantung_terbuat_dari_dedaunan" => "Larva Ulat Kantung Air (Kantung terbuat dari dedaunan)",
			"nimfa_lalat_sehari_insang_segi_empat" => "Nimfa Lalat Sehari Insang Segi Empat",
			"udang_air_tawar_dan_udang_biasa" => "Udang Air Tawar dan Udang Biasa",
			"kepik_pinggan_bermoncong_panjang" => "Kepik Pinggan Bermoncong Panjang",
			"larva_ulat_kantung_air_kantung_dari_pasir_atau_kerikil" => "Larva Ulat Kantung Air (kantung dari pasir atau kerikil)",
			"nimfa_lalat_sehari_pipih" => "Nimfa Lalat Sehari Pipih",
			"nimfa_lalat_sehari_insang_bercabang" => "Nimfa Lalat Sehari Insang Bercabang",
			"nimfa_lalat_sehari_penggali" => "Nimfa Lalat Sehari Penggali",
			"nimfa_plekoptera" => "Nimfa Plekoptera",
		);

		foreach ($result as $key => $value) {
			$biologyResult = $this->MonitoringModel->biology_result($value->monitoring_id);
			$biology = "<div><table class='table monitoring-sub-table table-condensed'>";
			$biology .= "<tr><td>Skor:</td><td>" . $biologyResult["total"] . "</td></tr>";
			$biology .= "<tr><td>Jumlah Binatang:</td><td>" . $biologyResult["number"] . "</td></tr>";
			$biology .= "<tr><td>Indeks Kualitas Air:</td><td>" . $biologyResult["index"] . "</td></tr>";
			$biology .= "<tr><td>Kesimpulan:</td><td>" . $biologyResult["indexText"] . "</td></tr>";
			$biology .= "</table></div>";
			
			$chemistry = "<div><table class='table monitoring-sub-table table-condensed'>";
			$chemistry .= "<tr><td>Suhu:</td><td>" . $value->temprature . "</td></tr>";
			$chemistry .= "<tr><td>pH:</td><td>" . $value->ph . "</td></tr>";
			$chemistry .= "<tr><td>DO (Dissolved Oxygen):</td><td>" . $value->do . "</td></tr>";
			$chemistry .= "<tr><td>COD (Chemical Oxygen Demand):</td><td>" . $value->cod . "</td></tr>";
			$chemistry .= "<tr><td>Transparansi:</td><td>" . $value->transparency . "</td></tr>";
			$chemistry .= "</table></div>";
			$age = "";
			switch ($value->age) {
				case 1:
					$age = "0 - 10";
					break;
				case 2:
					$age = "11 - 14";
					break;
				case 3:
					$age = "15 - 17";
					break;
				case 4:
					$age = "18 - Keatas";
					break;
				
				default:
					$age = "";
					break;
			}

			$biologyAnimal = $this->MonitoringModel->biology_animal($value->monitoring_id);
			$animalList = "";
			foreach ($biologyAnimal[0] as $key1 => $value1) {
				if ($value1 > 0) {
					$animalList .= "<tr><td>" . $arr[$key1] . "</td><td>&nbsp;</td></tr>";
				}
			}

			$data[] = array(
				"date" => $value->date,
				"name" => $value->name,
				"biology" => $biology,
				"chemistry" => $chemistry,
				"menu" => '<div class="btn-group-vertical">
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-post-' . $key . '">Lihat</button>
				<a href="' . $this->config->base_url() . 'member/Monitoring?delete=' . $value->monitoring_id . '" onclick="confirm(this); return false;"  class="btn btn-danger">Hapus</a>
				</div>
				<div class="modal" id="modal-post-' . $key . '" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
	            <div class="modal-content">
	              <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  <span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title">' . $value->name . '</h4>
	              </div>
	              <div class="modal-body">
	              <table class="table monitoring-sub-dialog">
	              	<tr><td>Lokasi:</td><td>' . $value->name . '</td></tr>
	              	<tr><td>Tanggal:</td><td>' . $value->date . '</td></tr>
	              	<tr><td>Sekolah / Instansi:</td><td>' . $value->nama_anggota . '</td></tr>
	              	<tr><td>Jumlah Perserta:</td><td>' . $value->participants . '</td></tr>
	              	<tr><td>Rata-rata Usia Peserta:</td><td>' . $age . '</td></tr>
	              </table>
	              <p><strong>Biologi</strong></p>
	              <table class="table monitoring-sub-dialog">
	              	<tr><td>Skor:</td><td>' . $biologyResult["total"] . '</td></tr>
	              	<tr><td>Skor:</td><td>' . $biologyResult["total"] . '</td></tr>
	              	<tr><td>Jumlah Binatang:</td><td>' . $biologyResult["number"] . '</td></tr>
					<tr><td>Indeks Kualitas Air:</td><td>' . $biologyResult["index"] . '</td></tr>
					<tr><td>Kesimpulan:</td><td>' . $biologyResult["indexText"] . '</td></tr>
	              </table>
	              <p><strong>Daftar Binatang</strong></p>
	              <table class="table monitoring-sub-dialog">
	              '. $animalList . '
	              </table>
	              <p><strong>Kimia</strong></p>
	              <table class="table table-striped">
	              	<tr><td>Suhu:</td><td>' . $value->temprature . '</td></tr>
					<tr><td>pH:</td><td>' . $value->ph . '</td></tr>
					<tr><td>DO (Dissolved Oxygen):</td><td>' . $value->do . '</td></tr>
					<tr><td>COD (Chemical Oxygen Demand):</td><td>' . $value->cod . '</td></tr>
					<tr><td>Transparansi:</td><td>' . $value->transparency . '</td></tr>
	              </table>
	                <p><img src="' . $this->config->base_url() . $value->image_url . '" style="width:100%;"></p>
	                <div id="map-' . $key . '" style="position: relative; overflow: hidden; height: 300px; width: 100%;"></div>
	                <script>
			        var uluru = {lat: parseFloat(' . $value->lat . '), lng: parseFloat(' . $value->lng . ')};

			        $("#modal-post-' . $key . '").on("shown.bs.modal", function (e) {
			        	console.log("aaaaass")
						map = new google.maps.Map(document.getElementById("map-' . $key . '"), {
						  center: uluru,
						  zoom: 14,
						  mapTypeId: "satellite"
						});
						var marker = null;
			            marker = new google.maps.Marker({
			                position: uluru, 
			                map: map
			            });
			        });
			        </script>
	              </div>
	              <div class="modal-footer">
	                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
	              </div>
	            </div>
	          </div>
	        </div>',);
		}
		$return_data = array(
			"draw" => $draw,
			"recordsTotal" => 10,
			"recordsFiltered" => count($result_wo_limit),
			"data" => $data,
		);
		echo json_encode($return_data);
	}
}
