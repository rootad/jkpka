<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
       parent::__construct();
       $this->load->model('UserModel');
    }

	public function index($request = array()) {
		$logged_in = $this->session->userdata('logged_in');

		$data = array(
			"email" => ""
		);

		if (isset($request["email"])) {
			$data = array(
				"email" => $request["email"]
			);
		}

		if ($logged_in) {
			redirect("admin/dashboard", 'location');
		} else {
			$this->load->view("pages/admin/login", $data);
		}
	}

	public function login() {

		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == false) {
			$this->index();
		} else {
			$email = $this->input->post('email');
			$password = $this->input->post('password');

			if ($this->UserModel->resolve_user_login($email, $password)) {
				$user = $this->UserModel->get_user_by_email($email);

				$_SESSION['id'] = (int)$user->id;
				$_SESSION['email'] = (string)$user->email;
				$_SESSION['logged_in'] = (bool)true;

				$data = array(
				        'id'  => $user->id,
				        'name'  => $user->name,
				        'email'     => $user->email,
				        'logged_in' => TRUE
				);

				$this->session->set_userdata($data);

				redirect("admin/dashboard", 'location');
			} else {
				$data = array(
					"email" => $email
				);

				$this->session->set_flashdata('error', 'Invalid login credentials');

				$this->index($data);
			}
		}
	}
}
