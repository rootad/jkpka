<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends Admin_Controller {

	public function __construct() {
       parent::__construct();
    }

	public function index() {
		$this->load->template_admin(
			'pages/admin/data/request',
			array(
				"title" => "JKPKA",
				"page" => "request"
			)
		);
	}

	public function data() {
		$array_items = array('id', 'email', 'name');
		$this->session->unset_userdata($array_items);

		$this->session->set_userdata('logged_in', FALSE);

		redirect("admin/login", 'location');
	}
}
