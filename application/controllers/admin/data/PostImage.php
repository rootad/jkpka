<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PostImage extends Admin_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('Post');
		$this->load->helper(array('form', 'url'));
	}

	public function index() {
		$request = array_merge($_POST, $_GET);

		if (isset($request["delete"])) {
			$image = $this->Post->get_image_by_id($request["delete"]);

			$this->session->set_flashdata('delete_image', 'hapus gambar berhasil (' . $image->post_title  . ')');

			unlink(FCPATH . $image->guid);

			$this->Post->delete_image($request["delete"]);

			redirect("admin/data/PostImage", 'location');
		}

		$error = "";

		if (!empty($_FILES)) {
			$config['upload_path']          = FCPATH . 'wp-content/uploads/post/';
	        $config['allowed_types']        = 'jpg|png';
	        $config['max_size']             = 1000;
	        // $config['max_width']            = 1024;
	        // $config['max_height']           = 768;

	        $this->load->library('upload', $config);

	        if ( ! $this->upload->do_upload('image')) {
	                $error = $this->upload->display_errors();

	        } else {
	        	$this->Post->add_image($this->upload->data('file_name'), "inherit", $this->upload->data('file_name'), 'wp-content/uploads/post' . DIRECTORY_SEPARATOR . $this->upload->data('file_name'));
	        	$error = "0";
	        }
	    }

		$this->load->template_admin(
			'pages/admin/data/post/post_image',
			array(
				"title" => "JKPKA",
				"page" => "post_image",
				"error" => $error
			)
		);
	}

	public function data() {
		$request = array_merge($_POST, $_GET);
		$draw = $request["draw"];
		$start = $request["start"];
		$length = $request["length"];
		$column = $request["order"][0]["column"];
		$dir = $request["order"][0]["dir"];
		$search = $request["search"]["value"];
		$columns = $request["columns"];

		$result = $this->Post->get_image_list($start, $length, $column, $dir, $search, $columns);
		$result_wo_limit = $this->Post->get_image_list($start, 999, $column, $dir, $search, $columns);

		$data = array();
		foreach ($result as $key => $value) {
			$data[] = array(
				"ID" => $value->ID,
				"post_title" => $value->post_title,
				"image" => '<button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-image-' . $key . '"><img src="' . $this->config->base_url() . $value->guid . '" height="50" width="50"></button>
				<div class="modal" id="modal-image-' . $key . '">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">' . $value->post_title . '</h4>
              </div>
              <div class="modal-body">
                <img src="' . $this->config->base_url() . $value->guid . '" style="width: 100%;">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>',
				"guid" => $this->config->base_url() . $value->guid,
				"menu" => '<div class="btn-group-vertical">
                      <a href="' . $this->config->base_url() . 'admin/data/PostImage?delete=' . $value->ID . '" onclick="confirm(this); return false;"  class="btn btn-danger">Hapus</a>


                    </div>',
			);
		}
		$return_data = array(
			"draw" => $draw,
			"recordsTotal" => $this->Post->get_image_list_count(),
			"recordsFiltered" => count($result_wo_limit),
			"data" => $data,
		);
		echo json_encode($return_data);
	}
}
