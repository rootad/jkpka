<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PostData extends Admin_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('Post');
		$this->load->helper(array('form', 'url'));
    }

	public function index() {
		$request = array_merge($_GET, $_POST);

		$status_delete = "0";

		if (isset($request["delete"])) {
			$result = $this->Post->delete($request["delete"]);
			$status_delete = "1";
		}

		$this->load->template_admin(
			'pages/admin/data/post/post',
			array(
				"title" => "JKPKA",
				"page" => "post",
				"status_delete" => $status_delete
			)
		);
	}

	public function add() {
		# code...
		$this->edit();
	}

	public function edit() {
		$request = array_merge($_GET, $_POST);
		$this->form_validation->set_rules('post_title', 'Judul', 'required');
		$this->form_validation->set_rules('post_content', 'Content', 'required');
		$this->form_validation->set_rules('image_cover', 'Gambar Cover', 'required');

		$post_title = "";
		$post_content = "";
		$image_cover = "";
		$id = "";

		if (isset($request["post_title"])) {
			$post_title = $request["post_title"];
		}

		if (isset($request["post_content"])) {
			$post_content = $request["post_content"];
		}

		if (isset($request["image_cover"])) {
			$image_cover = $request["image_cover"];
		}

		$result = null;

		if (count($request) > 1) {
			if ($this->form_validation->run() == false) {
			} else {
				if (isset($request['edit'])) {
					$result = $this->Post->update($request["post_title"], $request["post_content"], $request["image_cover"], $request['edit']);
				} else {
					$result = $this->Post->create($request["post_title"], $request["post_content"], $request["image_cover"]);
					$post_title = "";
					$post_content = "";
					$image_cover = "";
				}
			}
		}

		if (isset($request['edit'])) {
			$id = $request['edit'];
			$data = $this->Post->get_by_id($request['edit']);
			$image = $this->Post->get_image($request['edit']);

			$post_title = $data->post_title;
			$post_content = $data->post_content;
			if (count($image) > 0) {
				$image_cover = $image->guid;
			}
		}


		$this->load->template_admin(
			'pages/admin/data/post/post_add',
			array(
				"title" => "JKPKA | Tambah Post!",
				"page" => "post_add",
				"result" => $result,
				"post_title" => $post_title,
				"post_content" => $post_content,
				"image_cover" => $image_cover,
				"edit" => $id,
			)
		);
	}

	public function data() {
		$request = array_merge($_POST, $_GET);
		$draw = $request["draw"];
		$start = $request["start"];
		$length = $request["length"];
		$column = $request["order"][0]["column"];
		$dir = $request["order"][0]["dir"];
		$search = $request["search"]["value"];
		$columns = $request["columns"];

		$result = $this->Post->get_list($start, $length, $column, $dir, $search, $columns);
		$result_wo_limit = $this->Post->get_list($start, 999, $column, $dir, $search, $columns);

		$data = array();
		foreach ($result as $key => $value) {
			$data[] = array(
				"post_title" => $value->post_title,
				"post_date" => $value->post_date,
				"menu" => '<div class="btn-group-vertical">
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-post-' . $key . '">Lihat</button>
                      <a href="' . $this->config->base_url() . 'admin/data/PostData/edit?edit=' . $value->ID . '" onclick="confirm(this); return false;"  class="btn btn-warning">Ubah</a>
                      <a href="' . $this->config->base_url() . 'admin/data/PostData?delete=' . $value->ID . '" onclick="confirm(this); return false;"  class="btn btn-danger">Hapus</a>
                    </div>
                    <div class="modal" id="modal-post-' . $key . '" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">' . $value->post_title . '</h4>
              </div>
              <div class="modal-body">
                <div style="max-width: 100%;">' . $value->post_content . '</div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>',
			);
		}
		$return_data = array(
			"draw" => $draw,
			"recordsTotal" => $this->Post->count(),
			"recordsFiltered" => count($result_wo_limit),
			"data" => $data,
		);
		echo json_encode($return_data);
	}
}
