<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class School extends Admin_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('MonitoringModel');
		$this->load->model('UserModel');
	}

	public function index() {
		$request = array_merge($_GET, $_POST);

		$status_delete = "0";

		if (isset($request["delete"])) {
			$result = $this->MonitoringModel->delete_monitoring($request["delete"]);
			$status_delete = "1";
		}

		$this->load->template_admin(
			'pages/admin/school_data',
			array(
				"title" => "JKPKA",
				"page" => "school_data",
				"status_delete" => $status_delete
			)
		);
	}

	public function chpass() {
		$this->load->database();
		$data = array(
			'password' => password_hash("B3b3kP@ngg@ng", PASSWORD_DEFAULT)
		);
		$this->db->where('email', "admin@jkpka.com");
		$this->db->update('users', $data);
	}

	public function add() {
		$this->edit();
	}

	public function edit() {
		$request = array_merge($_GET, $_POST);

		$result = "0";
		$id = "";
		$name = "";
		$area = "";
		$address = "";
		$phone = "";
		$position = "";
		$error_message = "";
		$edit = false;

		if (isset($request["edit"])) {
			$edit = true;
			$id = $request["edit"];
			$member = $this->MonitoringModel->get_school_by_id($request["edit"]);
			$name = $member->nama_anggota;
			$area = $member->area;
			$address = $member->almt_sekolah;
			$phone = $member->telp_anggota;
			$position = $member->kp_anggota;
		}

		if (count($request) > 1) {
			$this->form_validation->set_rules('name', 'Nama', 'required|max_length[100]');
			$this->form_validation->set_rules('area', 'Wilayah', 'required|max_length[100]');
			$this->form_validation->set_rules('address', 'Alamat', 'required|max_length[100]');
			$this->form_validation->set_rules('phone', 'Telepon', 'required|max_length[100]');

			$name = $request["name"];
			$area = $request["area"];
			$address = $request["address"];
			$phone = $request["phone"];
			$position = $request["position"];

			if ($this->form_validation->run() == false) {

			} else {
				$result = "1";

		    	if ($edit) {
		    		$this->MonitoringModel->update_school(
						$request["edit"],
						$name,
						$area,
						$address,
						$phone,
						$position
					);
		    	} else {
		    		$this->MonitoringModel->create_school(
						$name,
						$area,
						$address,
						$phone,
						$position
					);
		    	}
				

				if ($result == "1" && !$edit) {
					$name = "";
					$area = "";
					$address = "";
					$phone = "";
					$position = "";
				}
			}
		} else {
			
		}

		$data = array(
			"title" => "JKPKA",
			"page" => "school",
			"id_anggota" => $id,
			"edit" => $edit,
			"error_message" => $error_message,
			"nameSchool" => $name,
			"area" => $area,
			"address" => $address,
			"phone" => $phone,
			"position" => $position,
			"result" => $result
		);

		$this->load->template_admin(
			'pages/admin/school',
			$data
		);
	}

	public function data() {
		$request = array_merge($_POST, $_GET);
		$draw = $request["draw"];
		$start = $request["start"];
		$length = $request["length"];
		$column = $request["order"][0]["column"];
		$dir = $request["order"][0]["dir"];
		$search = $request["search"]["value"];
		$columns = $request["columns"];

		$result = $this->MonitoringModel->get_school_list($start, $length, $column, $dir, $search, $columns);
		$result_wo_limit = $this->MonitoringModel->get_school_list($start, 999, $column, $dir, $search, $columns);

		$data = array();

		foreach ($result as $key => $value) {
			$data[] = array(
				"nama_anggota" => $value->nama_anggota,
				"wilayah" => $value->wilayah,
				"almt_sekolah" => $value->almt_sekolah,
				"telp_anggota" => $value->telp_anggota,
				"kp_anggota" => $value->kp_anggota,
				"menu" => '<div class="btn-group-vertical">
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-post-' . $key . '">Lihat</button>
				<a href="' . $this->config->base_url() . 'admin/School/edit?edit=' . $value->id_anggota . '" onclick="confirm(this); return false;"  class="btn btn-warning">Ubah</a>
				<a href="' . $this->config->base_url() . 'admin/School?delete=' . $value->id_anggota . '" onclick="confirm(this); return false;"  class="btn btn-danger">Hapus</a>
				</div>
				<div class="modal" id="modal-post-' . $key . '" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
	            <div class="modal-content">
	              <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  <span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title">' . $value->nama_anggota . '</h4>
	              </div>
	              <div class="modal-body">
	              <table class="table monitoring-sub-dialog">
	              	<tr><td>Lokasi:</td><td>' . $value->almt_sekolah . '</td></tr>
	              	<tr><td>Tanggal:</td><td>' . $value->almt_sekolah . '</td></tr>
	              	<tr><td>Sekolah / Instansi:</td><td>' . $value->telp_anggota . '</td></tr>
	              	<tr><td>Jumlah Perserta:</td><td>' . $value->kp_anggota . '</td></tr>
	              </table>	              
	              </div>
	              <div class="modal-footer">
	                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
	              </div>
	            </div>
	          </div>
	        </div>',);
		}
		$return_data = array(
			"draw" => $draw,
			"recordsTotal" => $this->MonitoringModel->school_count(),
			"recordsFiltered" => count($result_wo_limit),
			"data" => $data,
		);
		echo json_encode($return_data);
	}
}
