<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends Admin_Controller {

	public function __construct() {
       parent::__construct();
    }

	public function index() {
		$this->load->template_admin(
			'pages/admin/setting/home/main',
			array(
				"title" => "JKPKA",
				"page" => "main"
			)
		);
	}
}
