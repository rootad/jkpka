<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Join extends CI_Controller {

	public function __construct() {
       parent::__construct();
       $this->load->model('Member');
    }

	public function index() {
		$request = array_merge($_GET, $_POST);
		$this->form_validation->set_rules('name', 'Nama Anda', 'required');
		$this->form_validation->set_rules('nameSchool', 'Nama Sekolah', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('message', 'Pesan', 'required');

		$result = null;

		if (count($request) > 0) {
			if ($this->form_validation->run() == false) {
			} else {
				$result = $this->Member->request($request["name"], $request["nameSchool"], $request["email"], $request["message"]);
			}
		}


		$this->load->template(
			'pages/join', 
			array(
				"title" => "JKPKA | Ayo Bergabung!",
				"page" => "join",
				"result" => $result,
			)
		);
	}
}
