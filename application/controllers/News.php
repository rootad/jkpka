<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	public function __construct() {
       parent::__construct();
       $this->load->model('Post');
    }

	public function index($page = 1) {
		$request = array_merge($_POST, $_GET);

		$limit = 12;
		$start = ($page - 1) * $limit;

		$result = $this->Post->get($start, $limit);
		$total = $this->Post->count();
		$pages = floor($total / $limit);

		$return = array();
		foreach ($result as $key => $value) {
			$temp = $this->Post->get_image($value->ID);
			$image = "";

			if ($temp != null) {
				$image = $temp->guid;
			}
			$return[] = array(
				'post_date' => date ("d F Y", strtotime($value->post_date)),
				'ID' => $value->ID,
				'post_title' => $value->post_title,
				'post_name' => $value->post_name,
				'image' => $image,
			);
		}

		$this->load->template(
			'pages/news', 
			array(
				"title" => "JKPKA | Dari Lapangan",
				"page" => "news",
				"result" => $return,
				"pages" => $pages,
				"page_current" => $page
			)
		);
	}

	public function content($id = 1) {
		$request = array_merge($_POST, $_GET);

		$result = $this->Post->get_by_id($id);

		$temp = $this->Post->get_image($id);
		$image = "";

		if ($temp != null) {
			$image = $temp->guid;
		}

		$this->load->template(
			'pages/content', 
			array(
				"title" => "JKPKA | Berita",
				"page" => "content",
				"result" => $result,
				"image" => $image
			)
		);
	}
}
