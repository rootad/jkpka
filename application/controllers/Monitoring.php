<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring extends CI_Controller {

	public function __construct() {
       parent::__construct();
       $this->load->model('MonitoringModel');
    }

    public function index($page = 1) {
    	$request = array_merge($_POST, $_GET);

		$limit = 12;
		$start = ($page - 1) * $limit;

		$result = $this->MonitoringModel->get($start, $limit);
		$total = $this->MonitoringModel->count();
		$pages = floor($total / $limit);

		$return = array();
		foreach ($result as $key => $value) {
			$bid_pantau = "Biologi";
			if ($value->bid_pantau == "K") {
				$bid_pantau = "Kimia";
			}
			$return[] = array(
				'tgl_posting' => date ("d F Y", strtotime($value->tgl_posting)),
				'hasil_pantau' => $value->hasil_pantau,
				'id_pantau' => $value->id_pantau,
				'nama_anggota' => $value->nama_anggota,
				'lokasi' => $value->lokasi,
				'bid_pantau' => $bid_pantau,
			);
		}

		$this->load->template(
			'pages/monitoring', 
			array(
				"title" => "JKPKA | Pemantauan",
				"page" => "monitoring",
				"result" => $return,
				"pages" => $pages,
				"page_current" => $page
			)
		);
    }
}