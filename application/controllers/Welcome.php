<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct() {
       parent::__construct();
       $this->load->model('Post');
    }

	public function index() {
		$result = $this->Post->get_last();
		$resultMin = $this->Post->get_last_min();

		$this->load->template(
			'pages/home', 
			array(
				"title" => "Selamat Datang Di Situs JKPKA | Jaringan Komunikasi Pemantauan Kualitas Air",
				"page" => "home",
				"result" => $result,
				"resultMin" => $resultMin
			)
		);
	}
	
	// public function test(Type $var = null) {
	// 	$this->load->library("Mailgun");

	// 	$this->mailgun
	// 		->to("robin <robinyonathan@yahoo.com>")
	// 		->from("noreplay@jkpka.com")
	// 		->subject("Selamat datang di JKPKA")
	// 		->message("<html><body><p>hello from jkpka</p></body></html>")
	// 		->send();
	// }
}
