<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct() {
       parent::__construct();
       	$this->load->model('UserModel');
		$this->load->model('MonitoringModel');
		$this->load->library('MyEmail');
    }

	public function index() {
		$request = array_merge($_POST, $_GET);
		$result = "0";

		$fullName = "";
		$gender = "";
		$birthday = "";
		$nameSchool = "";
		$job = "";
		$institute = "";
		$email = "";
		$password = "";
		$re_password = "";
		$error_message = "";

		if (isset($request["email"])) {
			$this->form_validation->set_rules('fullName', 'Nama Lengkap', 'required|max_length[100]');
			$this->form_validation->set_rules('gender', 'Jenis Kelamin', 'required|max_length[100]');
			$this->form_validation->set_rules('birthday', 'Tanggal Lahir', 'required|max_length[100]');
			// $this->form_validation->set_rules('nameSchool', 'Nama Sekolah', 'required|max_length[100]');
			$this->form_validation->set_rules('institute', 'Instansi / Sekolah', 'required|max_length[100]');
			$this->form_validation->set_rules('job', 'Pekerjaan', 'required|max_length[100]');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[100]');
			$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[100]');
			$this->form_validation->set_rules('re_password', 'Ulangi Password', 'required|matches[password]|max_length[100]');

			if ($this->form_validation->run() == false) {
				$fullName = $request["fullName"];
				$gender = $request["gender"];
				$birthday = $request["birthday"];
				// $nameSchool = $request["nameSchool"];
				$job = $request["job"];
				$email = $request["email"];
				$password = $request["password"];
				$re_password = $request["re_password"];
				$institute = $request["institute"];
			} else {
				if ($this->UserModel->get_member_by_email($request["email"]) != null) {
					$fullName = $request["fullName"];
					$gender = $request["gender"];
					$birthday = $request["birthday"];
					// $nameSchool = $request["nameSchool"];
					$job = $request["job"];
					$email = $request["email"];
					$password = $request["password"];
					$re_password = $request["re_password"];
					$institute = $request["institute"];
					$result = "0";
					$error_message = "Sudah ada yang menggunakan email tersebut";
				} else {
					$this->UserModel->create_member(
						$request["fullName"],
						$request["gender"],
						$request["birthday"],
						"",
						$request["institute"],
						$request["job"],
						$request["email"],
						$request["password"]
					);

					$this->myemail->sendVerifyEmail($request["email"], $request["fullName"]);

					$result = "1";
				}
			}
		}

		$data = array(
			"email" => "",
			"error_message" => $error_message,
			"fullName" => $fullName,
			"gender" => $gender,
			"birthday" => $birthday,
			"nameSchool" => $nameSchool,
			"institute" => $institute,
			"job" => $job,
			"email" => $email,
			"password" => $password,
			"re_password" => $re_password,
			"result" => $result,
			"school" => $this->MonitoringModel->get_school()
		);

		$this->load->view("pages/member/register", $data);
	}

	public function verify() {
		$request = array_merge($_POST, $_GET);

		$base = $this->myencrypt->base64url_decode($request['code']);
		$hash = $this->session->tempdata($base);
		if (password_verify($base, $hash)) {
			$this->session->unset_tempdata($base);
			$this->UserModel->member_set_verified(1, $base);
			$this->session->set_flashdata("from", "emailVerifySuccess");
		} else {
			$member = $this->UserModel->get_member_by_email($base);
			if ($member != null) {
				if ($member->verified == 1) {
					$this->session->set_flashdata("from", "emailVerifySuccess");
				} else {
					$this->session->set_flashdata("from", "emailVerifyFail");
					$this->session->set_flashdata("fromEmail", $base);
				}
			} else {
				$this->session->set_flashdata("from", "emailVerifyFail");
				$this->session->set_flashdata("fromEmail", $base);
			}
		}
		redirect('member/login', 'location');
	}


}
