<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Member_Controller {

	public function __construct() {
       parent::__construct();

       $this->load->model('MonitoringModel');
    }

	public function index() {
		$monitoring = $this->MonitoringModel->get_last_monitoring($this->session->userdata('member_id'));

		$last_monitoring = "";

		if ($monitoring != null) {
			$last_monitoring = $monitoring->created;
		}

		$this->load->template_member(
			'pages/member/dashboard',
			array(
				"title" => "JKPKA",
				"page" => "dashboard",
				"member_last_login_date" => $this->session->userdata('member_last_login_date'),
				"last_monitoring" => $last_monitoring,
			)
		);
	}

	public function sign_out() {
		$this->session->unset_userdata(array('member_id', 'member_name', 'member_email'));

		$this->session->set_userdata('member_logged_in', FALSE);

		redirect("member", 'location');
	}
}
