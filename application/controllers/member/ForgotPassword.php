<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ForgotPassword extends CI_Controller {

	public function __construct() {
       parent::__construct();
       	$this->load->model('UserModel');
		$this->load->library('MyEmail');
		$this->load->library('MyEncrypt');
    }

	public function index($request = array()) {
		$request = array_merge($_GET, $_POST);


		$data = array(
			"email" => "",
			"error_message" => "",
			"message" => "",
			"color" => "",
			"code" => ""
		);

		if (isset($request["email"])) {
			$data["email"] = $request["email"];
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			if ($this->form_validation->run() == false) {
			} else {
				$member = $this->UserModel->get_member_by_email($request["email"]);
				if ($member != null) {
					$this->myemail->sendForgotEmail($request["email"], $member->name);
					$data["message"] = 'Email berisi instruksi lupa password berhasil dikirim, ikuti intsruksi di email.';
					$data["color"] = 'success';
				} else {
					$data["error_message"] = 'Gagal login, akun tidak ditemukan';
				}
			}
		}

		if (isset($request["code"])) {
			$base = $this->myencrypt->base64url_decode($request['code']);
			$hash = '';
			if (isset($_SESSION[$base])) {
				$hash = $_SESSION[$base];
			}
			if (password_verify($base, $hash)) {
				if (isset($request["password"]) && isset($request["re_password"])) {
					$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[100]');
					$this->form_validation->set_rules('re_password', 'Ulangi Password', 'required|matches[password]|max_length[100]');
					if ($this->form_validation->run() == false) {
						$data["code"] = $request["code"];
						$this->load->view("pages/member/password", $data);
					} else {
						$this->UserModel->member_set_password($request["password"], $base);
						$this->session->unset_userdata($base);
						$this->session->set_flashdata("from", "emailForgotSuccess");
						redirect('member/login', 'location');
					}
					$this->session->unset_userdata('some_name');
				} else {
					$data["code"] = $request["code"];
					$this->load->view("pages/member/password", $data);
				}
			} else {
				$this->load->view("pages/member/forgot_password", $data);
			}

		} else {
			$this->load->view("pages/member/forgot_password", $data);
		}


	}

	public function FunctionName($value='') {
		// code...
	}
}
