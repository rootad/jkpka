<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring extends Member_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->load->model('MonitoringModel');
		$this->load->model('UserModel');
	}

	public function index() {
		$request = array_merge($_GET, $_POST);

		$status_delete = "0";

		if (isset($request["delete"])) {
			$result = $this->MonitoringModel->delete_monitoring($request["delete"]);
			$status_delete = "1";
		}

		$this->load->template_member(
			'pages/member/monitoring_data',
			array(
				"title" => "JKPKA",
				"page" => "monitoring_data",
				"status_delete" => $status_delete
			)
		);
	}

	public function add() {
		$this->edit();
	}

	public function edit() {
		$request = array_merge($_GET, $_POST);

		$result = "0";
		$id = "";
		$showAll = false;
		$arr = array(
			"locationId" => "",
			"date" => "",
			"participants" => "",
			"age" => "",
			"weather" => "",
			"surroundingEnvironment" => "",
			"addOnInfo" => "",
			"temprature" => "",
			"ph" => "",
			"do" => "",
			"cod" => "",
			"transparency" => "",
			"cacing_larva" => "0",
			"larva_mrutu_biasa" => "0",
			"belatung_ekor_tikus" => "0",
			"lintah" => "0",
			"kepiting_sungai" => "0",
			"kerang" => "0",
			"siput_tanpa_pintu" => "0",
			"nimfa_capung_jarum_ekor_tebal" => "0",
			"nimfa_capung_dobson" => "0",
			"nimfa_capung_sialid" => "0",
			"nimfa_lalat_sehari_perenang" => "0",
			"larva_lalat_atau_nyamuk_lainnya" => "0",
			"cacing_pipih" => "0",
			"larva_kumbang" => "0",
			"kumbang_dewasa" => "0",
			"kepik_pejalan_kaki" => "0",
			"anggang_anggang" => "0",
			"kepik_perenang_punggung" => "0",
			"kepik_pendayung" => "0",
			"kepik_air_lainnya" => "0",
			"siput_berpintu" => "0",
			"kijing" => "0",
			"limpet_air_tawar" => "0",
			"nimfa_capung_biasa" => "0",
			"nimfa_capung_jarum_lainnya" => "0",
			"larva_ulat_air_tanpa_kantung" => "0",
			"larva_ulat_kantung_air_kantung_terbuat_dari_dedaunan" => "0",
			"nimfa_lalat_sehari_insang_segi_empat" => "0",
			"udang_air_tawar_dan_udang_biasa" => "0",
			"kepik_pinggan_bermoncong_panjang" => "0",
			"larva_ulat_kantung_air_kantung_dari_pasir_atau_kerikil" => "0",
			"nimfa_lalat_sehari_pipih" => "0",
			"nimfa_lalat_sehari_insang_bercabang" => "0",
			"nimfa_lalat_sehari_penggali" => "0",
			"nimfa_plekoptera" => "0",
		);
		
		$error_message = "";
		$edit = false;

		if (isset($request["edit"])) {
			$edit = true;
			$id = $request["edit"];
			$showAll = true;
			$monitoringData = $this->MonitoringModel->get_monitoring_by_id($request["edit"]);
			$arr = array(
				"locationId" => $monitoringData->name,
				"date" => $monitoringData->date,
				"participants" => $monitoringData->participants,
				"age" => $monitoringData->age,
				"weather" => $monitoringData->weather,
				"surroundingEnvironment" => $monitoringData->add_on_info,
				"addOnInfo" => $monitoringData->surrounding_environment,
				"temprature" => $monitoringData->temprature,
				"ph" => $monitoringData->ph,
				"do" => $monitoringData->do,
				"cod" => $monitoringData->cod,
				"transparency" => $monitoringData->transparency,
				"cacing_larva" => $monitoringData->cacing_larva,
				"larva_mrutu_biasa" => $monitoringData->larva_mrutu_biasa,
				"belatung_ekor_tikus" => $monitoringData->belatung_ekor_tikus,
				"lintah" => $monitoringData->lintah,
				"kepiting_sungai" => $monitoringData->kepiting_sungai,
				"kerang" => $monitoringData->kerang,
				"siput_tanpa_pintu" => $monitoringData->siput_tanpa_pintu,
				"nimfa_capung_jarum_ekor_tebal" => $monitoringData->nimfa_capung_jarum_ekor_tebal,
				"nimfa_capung_dobson" => $monitoringData->nimfa_capung_dobson,
				"nimfa_capung_sialid" => $monitoringData->nimfa_capung_sialid,
				"nimfa_lalat_sehari_perenang" => $monitoringData->nimfa_lalat_sehari_perenang,
				"larva_lalat_atau_nyamuk_lainnya" => $monitoringData->larva_lalat_atau_nyamuk_lainnya,
				"cacing_pipih" => $monitoringData->cacing_pipih,
				"larva_kumbang" => $monitoringData->larva_kumbang,
				"kumbang_dewasa" => $monitoringData->kumbang_dewasa,
				"kepik_pejalan_kaki" => $monitoringData->kepik_pejalan_kaki,
				"anggang_anggang" => $monitoringData->anggang_anggang,
				"kepik_perenang_punggung" => $monitoringData->kepik_perenang_punggung,
				"kepik_pendayung" => $monitoringData->kepik_pendayung,
				"kepik_air_lainnya" => $monitoringData->kepik_air_lainnya,
				"siput_berpintu" => $monitoringData->siput_berpintu,
				"kijing" => $monitoringData->kijing,
				"limpet_air_tawar" => $monitoringData->limpet_air_tawar,
				"nimfa_capung_biasa" => $monitoringData->nimfa_capung_biasa,
				"nimfa_capung_jarum_lainnya" => $monitoringData->nimfa_capung_jarum_lainnya,
				"larva_ulat_air_tanpa_kantung" => $monitoringData->larva_ulat_air_tanpa_kantung,
				"larva_ulat_kantung_air_kantung_terbuat_dari_dedaunan" => $monitoringData->larva_ulat_kantung_air_kantung_terbuat_dari_dedaunan,
				"nimfa_lalat_sehari_insang_segi_empat" => $monitoringData->nimfa_lalat_sehari_insang_segi_empat,
				"udang_air_tawar_dan_udang_biasa" => $monitoringData->udang_air_tawar_dan_udang_biasa,
				"kepik_pinggan_bermoncong_panjang" => $monitoringData->kepik_pinggan_bermoncong_panjang,
				"larva_ulat_kantung_air_kantung_dari_pasir_atau_kerikil" => $monitoringData->larva_ulat_kantung_air_kantung_dari_pasir_atau_kerikil,
				"nimfa_lalat_sehari_pipih" => $monitoringData->nimfa_lalat_sehari_pipih,
				"nimfa_lalat_sehari_insang_bercabang" => $monitoringData->nimfa_lalat_sehari_insang_bercabang,
				"nimfa_lalat_sehari_penggali" => $monitoringData->nimfa_lalat_sehari_penggali,
				"nimfa_plekoptera" => $monitoringData->nimfa_plekoptera,
			);
		}

		if (count($request) > 1) {
			$this->form_validation->set_rules('locationId', 'Nama Lokasi', 'required|max_length[100]');
			$this->form_validation->set_rules('date', 'Tanggal/Waktu', 'required|max_length[100]');
			// $this->form_validation->set_rules('nameSchool', 'Jenis Permukaan air', 'required|max_length[100]');
			$this->form_validation->set_rules('participants', 'Jumlah Perserta', 'required|max_length[5]');
			$this->form_validation->set_rules('age', 'Rata-rata Usia Peserta', 'required|max_length[5]');
			$this->form_validation->set_rules('weather', 'Cuaca', 'required|max_length[100]');
			$this->form_validation->set_rules('surroundingEnvironment', 'Lingkungan Sekitar', 'required|max_length[100]');
			$this->form_validation->set_rules('addOnInfo', 'Informasi Tambahan', 'required|max_length[100]');
			// $this->form_validation->set_rules('temprature', 'Suhu Lokasi', 'required|max_length[5]');
			// $this->form_validation->set_rules('ph', 'pH', 'required|max_length[5]');
			// $this->form_validation->set_rules('do', 'DO (Dissolved Oxygen)', 'required|max_length[5]');
			// $this->form_validation->set_rules('cod', 'COD (Chemical Oxygen Demand)', 'required|max_length[5]');
			// $this->form_validation->set_rules('transparency', 'Transparansi', 'required|max_length[5]');
			
			if(isset($request["locationId"])) {
				$arr["locationId"] = $request["locationId"];
			}
			if(isset($request["date"])) {
				$arr["date"] = $request["date"];
			}
			if(isset($request["participants"])) {
				$arr["participants"] = $request["participants"];
			}
			if(isset($request["addOnInfo"])) {
				$arr["addOnInfo"] = $request["addOnInfo"];
			}
			if(isset($request["temprature"])) {
				$arr["temprature"] = $request["temprature"];
			}
			if(isset($request["ph"])) {
				$arr["ph"] = $request["ph"];
			}
			if(isset($request["do"])) {
				$arr["do"] = $request["do"];
			}
			if(isset($request["cod"])) {
				$arr["cod"] = $request["cod"];
			}
			if(isset($request["transparency"])) {
				$arr["transparency"] = $request["transparency"];
			}

			if (isset($request["age"])) {
				$arr["age"] = $request["age"];
			}
			if (isset($request["weather"])) {
				$arr["weather"] = $request["weather"];
			}
			if (isset($request["surroundingEnvironment"])) {
				$arr["surroundingEnvironment"] = $request["surroundingEnvironment"];
			}
			if (isset($request["cacing_larva"])) {
				$arr["cacing_larva"] = $request["cacing_larva"];
			}
			if (isset($request["larva_mrutu_biasa"])) {
				$arr["larva_mrutu_biasa"] = $request["larva_mrutu_biasa"];
			}
			if (isset($request["belatung_ekor_tikus"])) {
				$arr["belatung_ekor_tikus"] = $request["belatung_ekor_tikus"];
			}
			if (isset($request["lintah"])) {
				$arr["lintah"] = $request["lintah"];
			}
			if (isset($request["kepiting_sungai"])) {
				$arr["kepiting_sungai"] = $request["kepiting_sungai"];
			}
			if (isset($request["kerang"])) {
				$arr["kerang"] = $request["kerang"];
			}
			if (isset($request["siput_tanpa_pintu"])) {
				$arr["siput_tanpa_pintu"] = $request["siput_tanpa_pintu"];
			}
			if (isset($request["nimfa_capung_jarum_ekor_tebal"])) {
				$arr["nimfa_capung_jarum_ekor_tebal"] = $request["nimfa_capung_jarum_ekor_tebal"];
			}
			if (isset($request["nimfa_capung_dobson"])) {
				$arr["nimfa_capung_dobson"] = $request["nimfa_capung_dobson"];
			}
			if (isset($request["nimfa_capung_sialid"])) {
				$arr["nimfa_capung_sialid"] = $request["nimfa_capung_sialid"];
			}
			if (isset($request["nimfa_lalat_sehari_perenang"])) {
				$arr["nimfa_lalat_sehari_perenang"] = $request["nimfa_lalat_sehari_perenang"];
			}
			if (isset($request["larva_lalat_atau_nyamuk_lainnya"])) {
				$arr["larva_lalat_atau_nyamuk_lainnya"] = $request["larva_lalat_atau_nyamuk_lainnya"];
			}
			if (isset($request["cacing_pipih"])) {
				$arr["cacing_pipih"] = $request["cacing_pipih"];
			}
			if (isset($request["larva_kumbang"])) {
				$arr["larva_kumbang"] = $request["larva_kumbang"];
			}
			if (isset($request["kumbang_dewasa"])) {
				$arr["kumbang_dewasa"] = $request["kumbang_dewasa"];
			}
			if (isset($request["kepik_pejalan_kaki"])) {
				$arr["kepik_pejalan_kaki"] = $request["kepik_pejalan_kaki"];
			}
			if (isset($request["anggang_anggang"])) {
				$arr["anggang_anggang"] = $request["anggang_anggang"];
			}
			if (isset($request["kepik_perenang_punggung"])) {
				$arr["kepik_perenang_punggung"] = $request["kepik_perenang_punggung"];
			}
			if (isset($request["kepik_pendayung"])) {
				$arr["kepik_pendayung"] = $request["kepik_pendayung"];
			}
			if (isset($request["kepik_air_lainnya"])) {
				$arr["kepik_air_lainnya"] = $request["kepik_air_lainnya"];
			}
			if (isset($request["siput_berpintu"])) {
				$arr["siput_berpintu"] = $request["siput_berpintu"];
			}
			if (isset($request["kijing"])) {
				$arr["kijing"] = $request["kijing"];
			}
			if (isset($request["limpet_air_tawar"])) {
				$arr["limpet_air_tawar"] = $request["limpet_air_tawar"];
			}
			if (isset($request["nimfa_capung_biasa"])) {
				$arr["nimfa_capung_biasa"] = $request["nimfa_capung_biasa"];
			}
			if (isset($request["nimfa_capung_jarum_lainnya"])) {
				$arr["nimfa_capung_jarum_lainnya"] = $request["nimfa_capung_jarum_lainnya"];
			}
			if (isset($request["larva_ulat_air_tanpa_kantung"])) {
				$arr["larva_ulat_air_tanpa_kantung"] = $request["larva_ulat_air_tanpa_kantung"];
			}
			if (isset($request["larva_ulat_kantung_air_kantung_terbuat_dari_dedaunan"])) {
				$arr["larva_ulat_kantung_air_kantung_terbuat_dari_dedaunan"] = $request["larva_ulat_kantung_air_kantung_terbuat_dari_dedaunan"];
			}
			if (isset($request["nimfa_lalat_sehari_insang_segi_empat"])) {
				$arr["nimfa_lalat_sehari_insang_segi_empat"] = $request["nimfa_lalat_sehari_insang_segi_empat"];
			}
			if (isset($request["udang_air_tawar_dan_udang_biasa"])) {
				$arr["udang_air_tawar_dan_udang_biasa"] = $request["udang_air_tawar_dan_udang_biasa"];
			}
			if (isset($request["kepik_pinggan_bermoncong_panjang"])) {
				$arr["kepik_pinggan_bermoncong_panjang"] = $request["kepik_pinggan_bermoncong_panjang"];
			}
			if (isset($request["larva_ulat_kantung_air_kantung_dari_pasir_atau_kerikil"])) {
				$arr["larva_ulat_kantung_air_kantung_dari_pasir_atau_kerikil"] = $request["larva_ulat_kantung_air_kantung_dari_pasir_atau_kerikil"];
			}
			if (isset($request["nimfa_lalat_sehari_pipih"])) {
				$arr["nimfa_lalat_sehari_pipih"] = $request["nimfa_lalat_sehari_pipih"];
			}
			if (isset($request["nimfa_lalat_sehari_insang_bercabang"])) {
				$arr["nimfa_lalat_sehari_insang_bercabang"] = $request["nimfa_lalat_sehari_insang_bercabang"];
			}
			if (isset($request["nimfa_lalat_sehari_penggali"])) {
				$arr["nimfa_lalat_sehari_penggali"] = $request["nimfa_lalat_sehari_penggali"];
			}
			if (isset($request["nimfa_plekoptera"])) {
				$arr["nimfa_plekoptera"] = $request["nimfa_plekoptera"];
			}

			if ($this->form_validation->run() == false) {
				$showAll = true;
			} else {
				$imageName = "";
				$imageUrl = "";
				$result = "1";

				if (isset($_FILES["image"])) {
					if (strlen($_FILES["image"]["name"]) > 0) {
						$config['upload_path']          = FCPATH . 'wp-content/uploads/location/';
						$config['allowed_types']        = 'jpg|png';
						$config['max_size']             = 100;
				        // $config['max_width']            = 1024;
				        // $config['max_height']           = 768;

						$this->load->library('upload', $config);

						if (!$this->upload->do_upload('image')) {
							$error_message = $this->upload->display_errors();
							$result = "0";
							$showAll = true;
						} else {
							$imageUrl = 'wp-content/uploads/monitoring' . DIRECTORY_SEPARATOR . $this->upload->data('file_name');
							$imageName = $this->upload->data('file_name');
							$result = "1";
						}
					}
				} else {
					$result = "0";
					$error_message = "Field Foto Kegitatan is required";
				}

				if ($result == "1") {
					if ($edit) {
						$this->MonitoringModel->update_monitoring(
							$request["edit"],
							$arr,
							$imageName,
							$imageUrl,
							$this->session->userdata('member_id')
						);
					} else {
						$this->MonitoringModel->create_monitoring(
							$this->session->userdata('member_id'),
							$arr,
							$imageName,
							$imageUrl
						);
					}
				}
				

				if ($result == "1" && !$edit) {
					$arr = array(
						"locationId" => "",
						"date" => "",
						"participants" => "",
						"age" => "",
						"weather" => "",
						"surroundingEnvironment" => "",
						"addOnInfo" => "",
						"temprature" => "",
						"ph" => "",
						"do" => "",
						"cod" => "",
						"transparency" => "",
						"cacing_larva" => "",
						"larva_mrutu_biasa" => "",
						"belatung_ekor_tikus" => "",
						"lintah" => "",
						"kepiting_sungai" => "",
						"kerang" => "",
						"siput_tanpa_pintu" => "",
						"nimfa_capung_jarum_ekor_tebal" => "",
						"nimfa_capung_dobson" => "",
						"nimfa_capung_sialid" => "",
						"nimfa_lalat_sehari_perenang" => "",
						"larva_lalat_atau_nyamuk_lainnya" => "",
						"cacing_pipih" => "",
						"larva_kumbang" => "",
						"kumbang_dewasa" => "",
						"kepik_pejalan_kaki" => "",
						"anggang_anggang" => "",
						"kepik_perenang_punggung" => "",
						"kepik_pendayung" => "",
						"kepik_air_lainnya" => "",
						"siput_berpintu" => "",
						"kijing" => "",
						"limpet_air_tawar" => "",
						"nimfa_capung_biasa" => "",
						"nimfa_capung_jarum_lainnya" => "",
						"larva_ulat_air_tanpa_kantung" => "",
						"larva_ulat_kantung_air_kantung_terbuat_dari_dedaunan" => "",
						"nimfa_lalat_sehari_insang_segi_empat" => "",
						"udang_air_tawar_dan_udang_biasa" => "",
						"kepik_pinggan_bermoncong_panjang" => "",
						"larva_ulat_kantung_air_kantung_dari_pasir_atau_kerikil" => "",
						"nimfa_lalat_sehari_pipih" => "",
						"nimfa_lalat_sehari_insang_bercabang" => "",
						"nimfa_lalat_sehari_penggali" => "",
						"nimfa_plekoptera" => "",
					);
				}
			}
		} else {
			
		}

		$data = array(
			"title" => "JKPKA",
			"page" => "monitoring",
			"edit" => $edit,
			"showAll" => $showAll,
			"monitoringId" => $id,
			"locationId" => $arr["locationId"],
			"date" => $arr["date"],
			"participants" => $arr["participants"],
			"age" => $arr["age"],
			"weather" => $arr["weather"],
			"addOnInfo" => $arr["addOnInfo"],
			"surroundingEnvironment" => $arr["surroundingEnvironment"],
			"ph" => $arr["ph"],
			"do" => $arr["do"],
			"cod" => $arr["cod"],
			"transparency" => $arr["transparency"],
			"cacing_larva" => $arr["cacing_larva"],
			"larva_mrutu_biasa" => $arr["larva_mrutu_biasa"],
			"belatung_ekor_tikus" => $arr["belatung_ekor_tikus"],
			"lintah" => $arr["lintah"],
			"kepiting_sungai" => $arr["kepiting_sungai"],
			"kerang" => $arr["kerang"],
			"siput_tanpa_pintu" => $arr["siput_tanpa_pintu"],
			"nimfa_capung_jarum_ekor_tebal" => $arr["nimfa_capung_jarum_ekor_tebal"],
			"nimfa_capung_dobson" => $arr["nimfa_capung_dobson"],
			"nimfa_capung_sialid" => $arr["nimfa_capung_sialid"],
			"nimfa_lalat_sehari_perenang" => $arr["nimfa_lalat_sehari_perenang"],
			"larva_lalat_atau_nyamuk_lainnya" => $arr["larva_lalat_atau_nyamuk_lainnya"],
			"cacing_pipih" => $arr["cacing_pipih"],
			"larva_kumbang" => $arr["larva_kumbang"],
			"kumbang_dewasa" => $arr["kumbang_dewasa"],
			"kepik_pejalan_kaki" => $arr["kepik_pejalan_kaki"],
			"anggang_anggang" => $arr["anggang_anggang"],
			"kepik_perenang_punggung" => $arr["kepik_perenang_punggung"],
			"kepik_pendayung" => $arr["kepik_pendayung"],
			"kepik_air_lainnya" => $arr["kepik_air_lainnya"],
			"siput_berpintu" => $arr["siput_berpintu"],
			"kijing" => $arr["kijing"],
			"limpet_air_tawar" => $arr["limpet_air_tawar"],
			"nimfa_capung_biasa" => $arr["nimfa_capung_biasa"],
			"nimfa_capung_jarum_lainnya" => $arr["nimfa_capung_jarum_lainnya"],
			"larva_ulat_air_tanpa_kantung" => $arr["larva_ulat_air_tanpa_kantung"],
			"larva_ulat_kantung_air_kantung_terbuat_dari_dedaunan" => $arr["larva_ulat_kantung_air_kantung_terbuat_dari_dedaunan"],
			"nimfa_lalat_sehari_insang_segi_empat" => $arr["nimfa_lalat_sehari_insang_segi_empat"],
			"udang_air_tawar_dan_udang_biasa" => $arr["udang_air_tawar_dan_udang_biasa"],
			"kepik_pinggan_bermoncong_panjang" => $arr["kepik_pinggan_bermoncong_panjang"],
			"larva_ulat_kantung_air_kantung_dari_pasir_atau_kerikil" => $arr["larva_ulat_kantung_air_kantung_dari_pasir_atau_kerikil"],
			"nimfa_lalat_sehari_pipih" => $arr["nimfa_lalat_sehari_pipih"],
			"nimfa_lalat_sehari_insang_bercabang" => $arr["nimfa_lalat_sehari_insang_bercabang"],
			"nimfa_lalat_sehari_penggali" => $arr["nimfa_lalat_sehari_penggali"],
			"nimfa_plekoptera" => $arr["nimfa_plekoptera"],
			"temprature" => $arr["temprature"],
			"error_message" => $error_message,
			"result" => $result,
			"location" => $this->MonitoringModel->get_location_list_simple()
		);

		$this->load->template_member(
			'pages/member/monitoring',
			$data
		);
	}

	public function data() {
		$request = array_merge($_POST, $_GET);
		$draw = $request["draw"];
		$start = $request["start"];
		$length = $request["length"];
		$column = $request["order"][0]["column"];
		$dir = $request["order"][0]["dir"];
		$search = $request["search"]["value"];
		$columns = $request["columns"];

		$result = $this->MonitoringModel->get_monitoring_list($start, $length, $column, $dir, $search, $columns, $this->session->userdata('member_id'));
		$result_wo_limit = $this->MonitoringModel->get_monitoring_list($start, 999, $column, $dir, $search, $columns, $this->session->userdata('member_id'));

		$data = array();

		$arr = array(
			"cacing_larva" => "Cacing Larva",
			"larva_mrutu_biasa" => "Larva Mrutu Biasa",
			"belatung_ekor_tikus" => "Belatung Ekor Tikus",
			"lintah" => "Lintah",
			"kepiting_sungai" => "Kepiting Sungai",
			"kerang" => "Kerang",
			"siput_tanpa_pintu" => "Siput Tanpa 'Pintu'",
			"nimfa_capung_jarum_ekor_tebal" => "Nimfa Capung Jarum Ekor Tebal",
			"nimfa_capung_dobson" => "Nimfa Capung Dobson",
			"nimfa_capung_sialid" => "Nimfa Capung Sialid",
			"nimfa_lalat_sehari_perenang" => "Nimfa Lalat Sehari Perenang",
			"larva_lalat_atau_nyamuk_lainnya" => "Larva Lalat atau Nyamuk Lainnya",
			"cacing_pipih" => "Cacing Pipih",
			"larva_kumbang" => "Larva Kumbang",
			"kumbang_dewasa" => "Kumbang Dewasa",
			"kepik_pejalan_kaki" => "Kepik - Pejalan Kaki",
			"anggang_anggang" => "Anggang - Anggang",
			"kepik_perenang_punggung" => "Kepik Perenang Punggung",
			"kepik_pendayung" => "Kepik Pendayung",
			"kepik_air_lainnya" => "Kepik Air Lainnya",
			"siput_berpintu" => "Siput Berpintu, lebih besar dari 15 mm",
			"kijing" => "Kijing",
			"limpet_air_tawar" => "Limpet Air Tawar",
			"nimfa_capung_biasa" => "Nimfa Capung Biasa",
			"nimfa_capung_jarum_lainnya" => "Nimfa Capung Jarum lainnya",
			"larva_ulat_air_tanpa_kantung" => "Larva Ulat Air (Tanpa Kantung)",
			"larva_ulat_kantung_air_kantung_terbuat_dari_dedaunan" => "Larva Ulat Kantung Air (Kantung terbuat dari dedaunan)",
			"nimfa_lalat_sehari_insang_segi_empat" => "Nimfa Lalat Sehari Insang Segi Empat",
			"udang_air_tawar_dan_udang_biasa" => "Udang Air Tawar dan Udang Biasa",
			"kepik_pinggan_bermoncong_panjang" => "Kepik Pinggan Bermoncong Panjang",
			"larva_ulat_kantung_air_kantung_dari_pasir_atau_kerikil" => "Larva Ulat Kantung Air (kantung dari pasir atau kerikil)",
			"nimfa_lalat_sehari_pipih" => "Nimfa Lalat Sehari Pipih",
			"nimfa_lalat_sehari_insang_bercabang" => "Nimfa Lalat Sehari Insang Bercabang",
			"nimfa_lalat_sehari_penggali" => "Nimfa Lalat Sehari Penggali",
			"nimfa_plekoptera" => "Nimfa Plekoptera",
		);

		foreach ($result as $key => $value) {
			$biologyResult = $this->MonitoringModel->biology_result($value->monitoring_id);
			$biology = "<div><table class='table monitoring-sub-table table-condensed'>";
			$biology .= "<tr><td>Skor:</td><td>" . $biologyResult["total"] . "</td></tr>";
			$biology .= "<tr><td>Jumlah Binatang:</td><td>" . $biologyResult["number"] . "</td></tr>";
			$biology .= "<tr><td>Indeks Kualitas Air:</td><td>" . $biologyResult["index"] . "</td></tr>";
			$biology .= "<tr><td>Kesimpulan:</td><td>" . $biologyResult["indexText"] . "</td></tr>";
			$biology .= "</table></div>";
			
			$chemistry = "<div><table class='table monitoring-sub-table table-condensed'>";
			$chemistry .= "<tr><td>Suhu:</td><td>" . $value->temprature . "</td></tr>";
			$chemistry .= "<tr><td>pH:</td><td>" . $value->ph . "</td></tr>";
			$chemistry .= "<tr><td>DO (Dissolved Oxygen):</td><td>" . $value->do . "</td></tr>";
			$chemistry .= "<tr><td>COD (Chemical Oxygen Demand):</td><td>" . $value->cod . "</td></tr>";
			$chemistry .= "<tr><td>Transparansi:</td><td>" . $value->transparency . "</td></tr>";
			$chemistry .= "</table></div>";
			$age = "";
			switch ($value->age) {
				case 1:
					$age = "0 - 10";
					break;
				case 2:
					$age = "11 - 14";
					break;
				case 3:
					$age = "15 - 17";
					break;
				case 4:
					$age = "18 - Keatas";
					break;
				
				default:
					$age = "";
					break;
			}

			$biologyAnimal = $this->MonitoringModel->biology_animal($value->monitoring_id);
			$animalList = "";
			foreach ($biologyAnimal[0] as $key1 => $value1) {
				if ($value1 > 0) {
					$animalList .= "<tr><td>" . $arr[$key1] . "</td><td>&nbsp;</td></tr>";
				}
			}

			$institute = $value->institute;
			if (strlen($institute) == 0) {
				$institute = $value->nama_anggota;
			}

			$data[] = array(
				"date" => $value->date,
				"location" => $value->location_name,
				"biology" => $biology,
				"chemistry" => $chemistry,
				"menu" => '<div class="btn-group-vertical">
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-post-' . $key . '">Lihat</button>
				<a href="' . $this->config->base_url() . 'member/Monitoring/edit?edit=' . $value->monitoring_id . '" onclick="confirm(this); return false;"  class="btn btn-warning">Ubah</a>
				<a href="' . $this->config->base_url() . 'member/Monitoring?delete=' . $value->monitoring_id . '" onclick="confirm(this); return false;"  class="btn btn-danger">Hapus</a>
				</div>
				<div class="modal" id="modal-post-' . $key . '" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
	            <div class="modal-content">
	              <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  <span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title">' . $value->location_name . '</h4>
	              </div>
	              <div class="modal-body">
	              <table class="table monitoring-sub-dialog">
	              	<tr><td>Lokasi:</td><td>' . $value->location_name . '</td></tr>
	              	<tr><td>Tanggal:</td><td>' . $value->date . '</td></tr>
	              	<tr><td>Sekolah / Instansi:</td><td>' . $institute . '</td></tr>
	              	<tr><td>Jumlah Perserta:</td><td>' . $value->participants . '</td></tr>
	              	<tr><td>Rata-rata Usia Peserta:</td><td>' . $age . '</td></tr>
	              </table>
	              <p><strong>Biologi</strong></p>
	              <table class="table monitoring-sub-dialog">
	              	<tr><td>Skor:</td><td>' . $biologyResult["total"] . '</td></tr>
	              	<tr><td>Skor:</td><td>' . $biologyResult["total"] . '</td></tr>
	              	<tr><td>Jumlah Binatang:</td><td>' . $biologyResult["number"] . '</td></tr>
					<tr><td>Indeks Kualitas Air:</td><td>' . $biologyResult["index"] . '</td></tr>
					<tr><td>Kesimpulan:</td><td>' . $biologyResult["indexText"] . '</td></tr>
	              </table>
	              <p><strong>Daftar Binatang</strong></p>
	              <table class="table monitoring-sub-dialog">
	              '. $animalList . '
	              </table>
	              <p><strong>Kimia</strong></p>
	              <table class="table table-striped">
	              	<tr><td>Suhu:</td><td>' . $value->temprature . '</td></tr>
					<tr><td>pH:</td><td>' . $value->ph . '</td></tr>
					<tr><td>DO (Dissolved Oxygen):</td><td>' . $value->do . '</td></tr>
					<tr><td>COD (Chemical Oxygen Demand):</td><td>' . $value->cod . '</td></tr>
					<tr><td>Transparansi:</td><td>' . $value->transparency . '</td></tr>
	              </table>
	                <p><img src="' . $this->config->base_url() . $value->image_url . '" style="width:100%;"></p>
	                <div id="map-' . $key . '" style="position: relative; overflow: hidden; height: 300px; width: 100%;"></div>
	                <script>
			        var uluru = {lat: parseFloat(' . $value->lat . '), lng: parseFloat(' . $value->lng . ')};

			        $("#modal-post-' . $key . '").on("shown.bs.modal", function (e) {
			        	console.log("aaaaass")
						map = new google.maps.Map(document.getElementById("map-' . $key . '"), {
						  center: uluru,
						  zoom: 14,
						  mapTypeId: "satellite"
						});
						var marker = null;
			            marker = new google.maps.Marker({
			                position: uluru, 
			                map: map
			            });
			        });
			        </script>
	              </div>
	              <div class="modal-footer">
	                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
	              </div>
	            </div>
	          </div>
	        </div>',);
		}
		$return_data = array(
			"draw" => $draw,
			"recordsTotal" => $this->MonitoringModel->monitoring_count($this->session->userdata('member_id')),
			"recordsFiltered" => count($result_wo_limit),
			"data" => $data,
		);
		echo json_encode($return_data);
	}
}
