<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
       parent::__construct();
       	$this->load->model('UserModel');
		$this->load->library('MyEmail');
		$this->load->library('MyEncrypt');
    }

	public function index($request = array()) {
		$request = array_merge($_GET, $_POST);

		$logged_in = $this->session->userdata('member_logged_in');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required');

		$data = array(
			"email" => "",
			"error_message" => "",
			"message" => "",
			"color" => ""
		);

		$from = $this->session->flashdata("from");

		if ($from != null) {
			if ($from == "emailVerifySuccess") {
				$data["message"] = "Email berhasil di verifikasi";
				$data["color"] = "success";
			}

			if ($from == "emailForgotSuccess") {
				$data["message"] = "Password berhasil diubah, silakan login kembali.";
				$data["color"] = "success";
			}

			if ($from == "emailVerifyFail") {
				$fromEmail = $this->session->flashdata("fromEmail");
				$data["color"] = "danger";
				$data["message"] = 'Email gagal di verifikasi, <a href="' . $this->config->base_url() . 'member/login/resend?email=' . $fromEmail . '">Kirim ulang email aktivasi.</a>';
			}

			if ($from == "emailVerifyResend") {
				$data["color"] = "success";
				$data["message"] = "Email berhasil di kirim ulang";
			}
		}

		if (isset($request["email"]) && isset($request["password"])) {
			$data["email"] = $request["email"];
			if ($this->form_validation->run() == false) {
			} else {
				if ($this->UserModel->resolve_member_login($request["email"], $request["password"])) {
					$member = $this->UserModel->get_member_by_email($request["email"]);
					if ($member->verified == 1) {
						$this->session->set_userdata(array(
						        'member_id'  => $member->member_id,
						        'member_name'  => $member->name,
						        'member_email'     => $member->email,
						        'member_last_login_date'     => $member->last_login_date,
						        'member_logged_in' => TRUE
						));

						$this->UserModel->member_last_login($member->member_id);

						redirect("member/dashboard", 'location');
					} else {
						$data["error_message"] = 'Gagal login, akun belum diverivikasi. <a href="' . $this->config->base_url() . 'member/login/resend?email=' . $request["email"] . '">Kirim ulang email aktivasi.</a>';
					}
				} else {
					$data["error_message"] = "Gagal login, email atau password salah";
				}
			}
		}

		if ($logged_in) {
			redirect("member/dashboard", 'location');
		} else {
			$this->load->view("pages/member/login", $data);
		}
	}

	public function resend() {
		$request = array_merge($_POST, $_GET);
		$member = $this->UserModel->get_member_by_email($request["email"]);
		$this->session->set_flashdata("from", "emailVerifyResend");
		if ($member != null) {
			$this->myemail->sendVerifyEmail($request["email"], $member->name);
		}
		redirect("member/Login/index", 'location');
	}
}
