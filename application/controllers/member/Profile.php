<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends Member_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('MonitoringModel');
		$this->load->model('UserModel');
    }

	public function index() {
		$request = array_merge($_GET, $_POST);

		$result = "0";
		$fullName = "";
		$gender = "";
		$birthday = "";
		$nameSchool = "";
		$job = "";
		$password = "";
		$re_password = "";
		$error_message = "";

		if (count($request) > 0) {
			$this->form_validation->set_rules('fullName', 'Nama Lengkap', 'required|max_length[100]');
			$this->form_validation->set_rules('gender', 'Jenis Kelamin', 'required|max_length[100]');
			$this->form_validation->set_rules('birthday', 'Tanggal Lahir', 'required|max_length[100]');
			$this->form_validation->set_rules('nameSchool', 'Nama Sekolah', 'required|max_length[100]');
			$this->form_validation->set_rules('job', 'Pekerjaan', 'required|max_length[100]');

			if (strlen($request['password']) > 0 && strlen($request['re_password']) > 0) {
				$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[100]');
				$this->form_validation->set_rules('re_password', 'Ulangi Password', 'required|matches[password]|max_length[100]');
			}

			$fullName = $request["fullName"];
			$gender = $request["gender"];
			$birthday = $request["birthday"];
			$nameSchool = $request["nameSchool"];
			$job = $request["job"];
			$password = $request["password"];
			$re_password = $request["re_password"];

			if ($this->form_validation->run() == false) {

			} else {
				$this->UserModel->member_update_profile(
					$request["fullName"],
					$request["gender"],
					$request["birthday"],
					$request["nameSchool"],
					$request["job"],
					$this->session->userdata('member_email'),
					$request["password"]
				);
				$result = "1";
			}
		} else {
			$member = $this->UserModel->get_member_by_email($this->session->userdata('member_email'));
			$fullName = $member->name;
			$gender = $member->gender;
			$birthday = $member->birthday;
			$nameSchool = $member->id_anggota;
			$job = $member->job;
		}

		$data = array(
			"title" => "JKPKA",
			"page" => "profile",
			"email" => "",
			"error_message" => $error_message,
			"fullName" => $fullName,
			"gender" => $gender,
			"birthday" => $birthday,
			"nameSchool" => $nameSchool,
			"job" => $job,
			"password" => $password,
			"re_password" => $re_password,
			"result" => $result,
			"school" => $this->MonitoringModel->get_school()
		);

		$this->load->template_member(
			'pages/member/profile',
			$data
		);
	}
}
