<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Location extends Member_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('MonitoringModel');
		$this->load->model('UserModel');
    }

	public function index() {
		$request = array_merge($_GET, $_POST);

		$status_delete = "0";

		if (isset($request["delete"])) {
			$result = $this->MonitoringModel->delete_location($request["delete"]);
			$status_delete = "1";
		}

		$this->load->template_member(
			'pages/member/location_data',
			array(
				"title" => "JKPKA",
				"page" => "location_data",
				"status_delete" => $status_delete
			)
		);
	}

	public function add() {
		$this->edit();
	}

	public function edit() {
		$request = array_merge($_GET, $_POST);

		$result = "0";
		$id = "";
		$name = "";
		$surface = "";
		$locationLon = "";
		$locationLat = "";
		$information = "";
		$error_message = "";
		$edit = false;

		if (isset($request["edit"])) {
			$edit = true;
			$id = $request["edit"];
			$member = $this->MonitoringModel->get_location_by_id($request["edit"]);
			$name = $member->name;
			$information = $member->information;
			$surface = $member->water_surface;
			$locationLat = $member->lat;
			$locationLon = $member->lng;
		}

		if (count($request) > 1) {
			$this->form_validation->set_rules('name', 'Nama Lokasi', 'required|max_length[100]');
			$this->form_validation->set_rules('information', 'Informasi Lokasi', 'required|max_length[100]');
			$this->form_validation->set_rules('surface', 'Jenis Permukaan air', 'required|max_length[100]');
			$this->form_validation->set_rules('locationLon', 'Koordinat', 'required|max_length[100]');
			$this->form_validation->set_rules('locationLat', 'Koordinat', 'required|max_length[100]');

			$name = $request["name"];
			$information = $request["information"];
			$surface = $request["surface"];
			$locationLon = $request["locationLon"];
			$locationLat = $request["locationLat"];

			if ($this->form_validation->run() == false) {

			} else {
				$imageName = "";
				$imageUrl = "";
				$result = "1";

				if (isset($_FILES["image"])) {
					if (strlen($_FILES["image"]["name"]) > 0) {
						$config['upload_path']          = FCPATH . 'wp-content/uploads/location/';
				        $config['allowed_types']        = 'jpg|png';
				        $config['max_size']             = 100;
				        // $config['max_width']            = 1024;
				        // $config['max_height']           = 768;

				        $this->load->library('upload', $config);

				        if (!$this->upload->do_upload('image')) {
			                $error_message = $this->upload->display_errors();
			                $result = "0";
				        } else {
				        	$imageUrl = 'wp-content/uploads/location' . DIRECTORY_SEPARATOR . $this->upload->data('file_name');
				        	$imageName = $this->upload->data('file_name');
				        	$result = "1";
				        }
					}
			    }

		    	if ($edit) {
		    		$this->MonitoringModel->update_location(
						$request["edit"],
						$name,
						$information,
						$surface,
						$locationLat,
						$locationLon,
						$imageName,
						$imageUrl,
						$this->session->userdata('member_id')
					);
		    	} else {
		    		$this->MonitoringModel->create_location(
						$this->session->userdata('member_id'),
						$name,
						$information,
						$surface,
						$locationLat,
						$locationLon,
						$imageName,
						$imageUrl
					);
		    	}
				

				if ($result == "1" && !$edit) {
					$name = "";
					$surface = "";
					$locationLon = "";
					$locationLat = "";
					$information = "";
				}
			}
		} else {
			
		}

		$data = array(
			"title" => "JKPKA",
			"page" => "location",
			"location_id" => $id,
			"edit" => $edit,
			"error_message" => $error_message,
			"nameLocation" => $name,
			"surface" => $surface,
			"locationLon" => $locationLon,
			"locationLat" => $locationLat,
			"information" => $information,
			"result" => $result
		);

		$this->load->template_member(
			'pages/member/location',
			$data
		);
	}

	public function data() {
		$request = array_merge($_POST, $_GET);
		$draw = $request["draw"];
		$start = $request["start"];
		$length = $request["length"];
		$column = $request["order"][0]["column"];
		$dir = $request["order"][0]["dir"];
		$search = $request["search"]["value"];
		$columns = $request["columns"];

		$result = $this->MonitoringModel->get_location_list($start, $length, $column, $dir, $search, $columns, $this->session->userdata('member_id'));
		$result_wo_limit = $this->MonitoringModel->get_location_list($start, 999, $column, $dir, $search, $columns, $this->session->userdata('member_id'));

		$data = array();
		foreach ($result as $key => $value) {
			$image = "http://via.placeholder.com/100x100?text=Tidak%20Ada%20Gambar";
			if ($value->image_url != null) {
				if (strlen($value->image_url) > 0) {
					$image = $this->config->base_url() . $value->image_url;
				}
			}
			$data[] = array(
				"name" => $value->name,
				"surface" => $value->water_surface,
				"image" => '<img src="' . $image . '" height="100" width="100">',
				"menu" => '<div class="btn-group-vertical">
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-post-' . $key . '">Lihat</button>
                      <a href="' . $this->config->base_url() . 'member/Location/edit?edit=' . $value->location_id . '" onclick="confirm(this); return false;"  class="btn btn-warning">Ubah</a>
                      <a href="' . $this->config->base_url() . 'member/Location?delete=' . $value->location_id . '" onclick="confirm(this); return false;"  class="btn btn-danger">Hapus</a>
                    </div>
                    <div class="modal" id="modal-post-' . $key . '" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">' . $value->name . '</h4>
              </div>
              <div class="modal-body">
                <div style="max-width: 100%;">Nama Lokasi: ' . $value->name . '</div>
                <div style="max-width: 100%;">Informasi Lokasi: ' . $value->information . '</div>
                <div style="max-width: 100%;">Jenis Permukaan air: ' . $value->water_surface . '</div>
                <p><img src="' . $image . '" style="width:100%;"></p>
                <div id="map-' . $key . '" style="position: relative; overflow: hidden; height: 300px; width: 100%;"></div>
                <script>
		        var uluru = {lat: parseFloat(' . $value->lat . '), lng: parseFloat(' . $value->lng . ')};

		        $("#modal-post-' . $key . '").on("shown.bs.modal", function (e) {
					map = new google.maps.Map(document.getElementById("map-' . $key . '"), {
					  center: uluru,
					  zoom: 14,
					  mapTypeId: "satellite"
					});
					var marker = null;
		            marker = new google.maps.Marker({
		                position: uluru, 
		                map: map
		            });
		        });
		        </script>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>',);
		}
		$return_data = array(
			"draw" => $draw,
			"recordsTotal" => $this->MonitoringModel->location_count($this->session->userdata('member_id')),
			"recordsFiltered" => count($result_wo_limit),
			"data" => $data,
		);
		echo json_encode($return_data);
	}
}
