<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	public function index()
	{
		$this->load->template(
			'pages/contact', 
			array(
				"title" => "JKPKA | Hubungi Kami",
				"page" => "contact"
			)
		);
	}
}
