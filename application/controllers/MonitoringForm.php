<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MonitoringForm extends CI_Controller {

	public function __construct() {
       parent::__construct();
       $this->load->model('MonitoringModel');
    }

	public function index() {
		$request = array_merge($_GET, $_POST);

		$this->form_validation->set_rules('nameSchool', 'Nama Sekolah', 'required');
		$this->form_validation->set_rules('location', 'Lokasi Pemantauan', 'required');
		$this->form_validation->set_rules('dateDay', 'Tanggal Pemantauan', 'required');
		$this->form_validation->set_rules('dateMonth', 'Tanggal Pemantauan', 'required');
		$this->form_validation->set_rules('dateYear', 'Tanggal Pemantauan', 'required');
		$this->form_validation->set_rules('time', 'Pukul', 'required');

		$result = null;

		$value = array(
			"lokasi" => "",
			"bid_pantau" => "Biologi",
			"hasil_pantau" => "",
			"tgl_posting" => "",
			"nama_anggota" => "",
		);

		if (count($request) > 0) {
			if ($this->form_validation->run() == false) {
			} else {
				$number = count($request['check']);
	            $total = array_sum($request['check']);
	            $indeks = number_format($total / $number, 1);

	            $result_index = "";
	            switch ($indeks){
					case 0:
						$result_index = 'Luar biasa kotor (tidak ada kehidupan sama sekali)';
						break;
					case (1.0 <= $indeks and $indeks <= 2.9):
						$result_index = 'Sangat Kotor';
						break;
					case (3.0 <= $indeks and $indeks <=4.9):
						$result_index = 'Kotor';
						break;
					case (5.0 <= $indeks and $indeks <=5.9):
						$result_index = 'Sedang (rata-rata)';
						break;
					case (6.0 <= $indeks and $indeks <=7.9):
						$result_index = 'Agak bersih sampai bersih';
						break;
					case (8.0 <= $indeks and $indeks <=10):
						$result_index = 'Sangat Bersih';
						break;
				}

	            $result_array = array (
	            	$result_index, 
	            	$request['dateDay'],
	            	$request['dateMonth'],
	            	$request['dateYear'],
	            	$request['time'],
	            	$total,
	            	$number,
	            	$indeks
	            );
	            
	            $text = implode('<<', $result_array);
	            
	            $now = date('Y-m-d H:i:s');

				$result = $this->MonitoringModel->create_biology($request["nameSchool"], $request["location"], $text);

				$single_school = $this->MonitoringModel->get_school_by_id($request["nameSchool"]);
				
				$value = array(
					"lokasi" => $request["location"],
					"bid_pantau" => "Biologi",
					"hasil_pantau" => $text,
					"tgl_posting" => $now,
					"nama_anggota" => $single_school->nama_anggota,
				);
			}
		}

		$school = $this->MonitoringModel->get_school();
		$this->load->template(
			'pages/monitoring_form', 
			array(
				"title" => "JKPKA | Formulir Pemantauan Biologi",
				"page" => "monitoring_form",
				"school" => $school,
				"result" => $result,
				"value" => $value,
			)
		);
	}

	public function chemistry() {
		$request = array_merge($_GET, $_POST);

		$this->form_validation->set_rules('nameSchool', 'Nama Sekolah', 'required');
		$this->form_validation->set_rules('location', 'Lokasi Pemantauan', 'required');
		$this->form_validation->set_rules('dateDay', 'Tanggal Pemantauan', 'required');
		$this->form_validation->set_rules('dateMonth', 'Tanggal Pemantauan', 'required');
		$this->form_validation->set_rules('dateYear', 'Tanggal Pemantauan', 'required');
		$this->form_validation->set_rules('time', 'Pukul', 'required');
		$this->form_validation->set_rules('temprature', 'Temperatur', 'required');
		$this->form_validation->set_rules('ph', 'pH', 'required');
		$this->form_validation->set_rules('do', 'DO (Disolve Oxygen)', 'required');
		$this->form_validation->set_rules('cod', 'COD (Chemical Oxygen Demand)', 'required');
		$this->form_validation->set_rules('tr', 'Transparansi', 'required');

		$result = null;

		$value = array(
			"lokasi" => "",
			"bid_pantau" => "Biologi",
			"hasil_pantau" => "",
			"tgl_posting" => "",
			"nama_anggota" => "",
		);

		if (count($request) > 0) {
			if ($this->form_validation->run() == false) {
			} else {
				$result_array = array(
					$_POST['dateDay'],
                  	$_POST['dateMonth'],
                  	$_POST['dateYear'],
                  	$_POST['time'],
                  	$_POST['temprature'],
                  	$_POST['ph'],
                  	$_POST['do'],
                  	$_POST['cod'],
                  	$_POST['tr']
                );
	            
	            $text = implode('<<', $result_array);
	            
	            $now = date('Y-m-d H:i:s');

				$result = $this->MonitoringModel->create_biology($request["nameSchool"], $request["location"], $text);

				$single_school = $this->MonitoringModel->get_school_by_id($request["nameSchool"]);

				$value = array(
					"lokasi" => $request["location"],
					"bid_pantau" => "Kimia",
					"hasil_pantau" => $text,
					"tgl_posting" => $now,
					"nama_anggota" => $single_school->nama_anggota,
				);
			}
		}

		$school = $this->MonitoringModel->get_school();
		$this->load->template(
			'pages/monitoring_form_chemistry', 
			array(
				"title" => "JKPKA | Formulir Pemantauan Kimia Kimia",
				"page" => "monitoring_form_chemistry",
				"school" => $school,
				"result" => $result,
				"value" => $value,
			)
		);
	}
}
