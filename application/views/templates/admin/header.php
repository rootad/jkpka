<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
	<title><?php echo $title; ?></title>
	<meta charset="utf-8">
	<meta name="description" content="JKPKA adalah singkatan dari Jaring-jaring Komunikasi Pemantauan Kualitas Air (Water Quality Monitoring Communication Network) adalah organisasi non profit, beranggotakan guru dan siswa yang peduli terhadap pelestarian sumber daya air.">
	<meta name="keywords" content="JKPKA,pine,Jaring-jaring Komunikasi Pemantauan Kualitas Air, jasa tirta">
	<meta name="twitter::author" content="pine">
	<meta name="twitter:site" content="JKPKA">
	<meta property="og:url" content="<?php echo current_url(); ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:locale" content="id_ID" />
	<meta property="og:title"content="<?php echo $title; ?>" />
	<meta property="og:description" content="JKPKA adalah singkatan dari Jaring-jaring Komunikasi Pemantauan Kualitas Air (Water Quality Monitoring Communication Network) adalah organisasi non profit, beranggotakan guru dan siswa yang peduli terhadap pelestarian sumber daya air." />
	<meta name="author" content="pine">
	<link rel="apple-touch-icon" href="<?php echo $this->config->base_url();?>media/images/logo.png" sizes="180x180">
	<link rel="icon" href="<?php echo $this->config->base_url();?>media/images/logo.png" sizes="32x32" type="image/png">
	<link rel="icon" href="<?php echo $this->config->base_url();?>media/images/logo.png">
	<link rel="shortcut icon" type="image/png" href="<?php echo $this->config->base_url();?>media/images/logo.png"/>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
	<!-- <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"> -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha256-916EbMg70RQy9LHiGkXzG8hSg9EdNy97GazNG/aiY1w=" crossorigin="anonymous" />
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.2/css/AdminLTE.min.css"> -->
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.2/css/skins/skin-blue.min.css"> -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" integrity="sha256-JDBcnYeV19J14isGd3EtnsCQK05d8PczJ5+fvEvBJvI=" crossorigin="anonymous" />
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.1/css/select2.min.css" integrity="sha256-MeSf8Rmg3b5qLFlijnpxk6l+IJkiR91//YGPCrCmogU=" crossorigin="anonymous" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/flat/blue.css" integrity="sha256-XM7s+h73EWY2tzW4pk09cZAcxLcVmOjUrEGEssRT9q0=" crossorigin="anonymous" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css" integrity="sha256-szHusaozbQctTn4FX+3l5E0A5zoxz7+ne4fr8NgWJlw=" crossorigin="anonymous" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.2/css/AdminLTE.min.css" integrity="sha256-sZVMQjkm0Ho60NDzV4Ot0OTBAK7UcaqkQOxLkTT93kQ=" crossorigin="anonymous" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.2/css/skins/skin-blue.min.css" integrity="sha256-dDi4GN+hJjMVQmkbeVpXkn3/qwQrL3oWvW8ukATCaPc=" crossorigin="anonymous" />
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	<link rel="stylesheet" href="<?php echo $this->config->base_url();?>media/css/admin.css?v=3">
</head>
<body class="skin-blue sidebar-mini">
	<div class="wrapper" style="height: auto; min-height: 100%;">
		<header class="main-header">

			<!-- Logo -->
			<a href="#" class="logo">
				<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIMAAABkCAYAAAHS27PKAAAABGdBTUEAALGPC/xhBQAACjppQ0NQUGhvdG9zaG9wIElDQyBwcm9maWxlAABIiZ2Wd1RU1xaHz713eqHNMBQpQ++9DSC9N6nSRGGYGWAoAw4zNLEhogIRRUQEFUGCIgaMhiKxIoqFgGDBHpAgoMRgFFFReTOyVnTl5b2Xl98fZ31rn733PWfvfda6AJC8/bm8dFgKgDSegB/i5UqPjIqmY/sBDPAAA8wAYLIyMwJCPcOASD4ebvRMkRP4IgiAN3fEKwA3jbyD6HTw/0malcEXiNIEidiCzclkibhQxKnZggyxfUbE1PgUMcMoMfNFBxSxvJgTF9nws88iO4uZncZji1h85gx2GlvMPSLemiXkiBjxF3FRFpeTLeJbItZMFaZxRfxWHJvGYWYCgCKJ7QIOK0nEpiIm8cNC3ES8FAAcKfErjv+KBZwcgfhSbukZuXxuYpKArsvSo5vZ2jLo3pzsVI5AYBTEZKUw+Wy6W3paBpOXC8DinT9LRlxbuqjI1ma21tZG5sZmXxXqv27+TYl7u0ivgj/3DKL1fbH9lV96PQCMWVFtdnyxxe8FoGMzAPL3v9g0DwIgKepb+8BX96GJ5yVJIMiwMzHJzs425nJYxuKC/qH/6fA39NX3jMXp/igP3Z2TwBSmCujiurHSU9OFfHpmBpPFoRv9eYj/ceBfn8MwhJPA4XN4oohw0ZRxeYmidvPYXAE3nUfn8v5TE/9h2J+0ONciURo+AWqsMZAaoALk1z6AohABEnNAtAP90Td/fDgQv7wI1YnFuf8s6N+zwmXiJZOb+DnOLSSMzhLysxb3xM8SoAEBSAIqUAAqQAPoAiNgDmyAPXAGHsAXBIIwEAVWARZIAmmAD7JBPtgIikAJ2AF2g2pQCxpAE2gBJ0AHOA0ugMvgOrgBboMHYASMg+dgBrwB8xAEYSEyRIEUIFVICzKAzCEG5Ah5QP5QCBQFxUGJEA8SQvnQJqgEKoeqoTqoCfoeOgVdgK5Cg9A9aBSagn6H3sMITIKpsDKsDZvADNgF9oPD4JVwIrwazoML4e1wFVwPH4Pb4Qvwdfg2PAI/h2cRgBARGqKGGCEMxA0JRKKRBISPrEOKkUqkHmlBupBe5CYygkwj71AYFAVFRxmh7FHeqOUoFmo1ah2qFFWNOoJqR/WgbqJGUTOoT2gyWgltgLZD+6Aj0YnobHQRuhLdiG5DX0LfRo+j32AwGBpGB2OD8cZEYZIxazClmP2YVsx5zCBmDDOLxWIVsAZYB2wglokVYIuwe7HHsOewQ9hx7FscEaeKM8d54qJxPFwBrhJ3FHcWN4SbwM3jpfBaeDt8IJ6Nz8WX4RvwXfgB/Dh+niBN0CE4EMIIyYSNhCpCC+ES4SHhFZFIVCfaEoOJXOIGYhXxOPEKcZT4jiRD0ie5kWJIQtJ20mHSedI90isymaxNdiZHkwXk7eQm8kXyY/JbCYqEsYSPBFtivUSNRLvEkMQLSbyklqSL5CrJPMlKyZOSA5LTUngpbSk3KabUOqkaqVNSw1Kz0hRpM+lA6TTpUumj0lelJ2WwMtoyHjJsmUKZQzIXZcYoCEWD4kZhUTZRGiiXKONUDFWH6kNNppZQv6P2U2dkZWQtZcNlc2RrZM/IjtAQmjbNh5ZKK6OdoN2hvZdTlnOR48htk2uRG5Kbk18i7yzPkS+Wb5W/Lf9ega7goZCisFOhQ+GRIkpRXzFYMVvxgOIlxekl1CX2S1hLipecWHJfCVbSVwpRWqN0SKlPaVZZRdlLOUN5r/JF5WkVmoqzSrJKhcpZlSlViqqjKle1QvWc6jO6LN2FnkqvovfQZ9SU1LzVhGp1av1q8+o66svVC9Rb1R9pEDQYGgkaFRrdGjOaqpoBmvmazZr3tfBaDK0krT1avVpz2jraEdpbtDu0J3XkdXx08nSadR7qknWddFfr1uve0sPoMfRS9Pbr3dCH9a30k/Rr9AcMYANrA67BfoNBQ7ShrSHPsN5w2Ihk5GKUZdRsNGpMM/Y3LjDuMH5homkSbbLTpNfkk6mVaappg+kDMxkzX7MCsy6z3831zVnmNea3LMgWnhbrLTotXloaWHIsD1jetaJYBVhtseq2+mhtY823brGestG0ibPZZzPMoDKCGKWMK7ZoW1fb9banbd/ZWdsJ7E7Y/WZvZJ9if9R+cqnOUs7ShqVjDuoOTIc6hxFHumOc40HHESc1J6ZTvdMTZw1ntnOj84SLnkuyyzGXF66mrnzXNtc5Nzu3tW7n3RF3L/di934PGY/lHtUejz3VPRM9mz1nvKy81nid90Z7+3nv9B72UfZh+TT5zPja+K717fEj+YX6Vfs98df35/t3BcABvgG7Ah4u01rGW9YRCAJ9AncFPgrSCVod9GMwJjgouCb4aYhZSH5IbyglNDb0aOibMNewsrAHy3WXC5d3h0uGx4Q3hc9FuEeUR4xEmkSujbwepRjFjeqMxkaHRzdGz67wWLF7xXiMVUxRzJ2VOitzVl5dpbgqddWZWMlYZuzJOHRcRNzRuA/MQGY9czbeJ35f/AzLjbWH9ZztzK5gT3EcOOWciQSHhPKEyUSHxF2JU0lOSZVJ01w3bjX3ZbJ3cm3yXEpgyuGUhdSI1NY0XFpc2imeDC+F15Oukp6TPphhkFGUMbLabvXu1TN8P35jJpS5MrNTQBX9TPUJdYWbhaNZjlk1WW+zw7NP5kjn8HL6cvVzt+VO5HnmfbsGtYa1pjtfLX9j/uhal7V166B18eu612usL1w/vsFrw5GNhI0pG38qMC0oL3i9KWJTV6Fy4YbCsc1em5uLJIr4RcNb7LfUbkVt5W7t32axbe+2T8Xs4mslpiWVJR9KWaXXvjH7puqbhe0J2/vLrMsO7MDs4O24s9Np55Fy6fK88rFdAbvaK+gVxRWvd8fuvlppWVm7h7BHuGekyr+qc6/m3h17P1QnVd+uca1p3ae0b9u+uf3s/UMHnA+01CrXltS+P8g9eLfOq669Xru+8hDmUNahpw3hDb3fMr5talRsLGn8eJh3eORIyJGeJpumpqNKR8ua4WZh89SxmGM3vnP/rrPFqKWuldZachwcFx5/9n3c93dO+J3oPsk42fKD1g/72ihtxe1Qe277TEdSx0hnVOfgKd9T3V32XW0/Gv94+LTa6ZozsmfKzhLOFp5dOJd3bvZ8xvnpC4kXxrpjux9cjLx4qye4p/+S36Urlz0vX+x16T13xeHK6at2V09dY1zruG59vb3Pqq/tJ6uf2vqt+9sHbAY6b9je6BpcOnh2yGnowk33m5dv+dy6fnvZ7cE7y+/cHY4ZHrnLvjt5L/Xey/tZ9+cfbHiIflj8SOpR5WOlx/U/6/3cOmI9cmbUfbTvSeiTB2Ossee/ZP7yYbzwKflp5YTqRNOk+eTpKc+pG89WPBt/nvF8frroV+lf973QffHDb86/9c1Ezoy/5L9c+L30lcKrw68tX3fPBs0+fpP2Zn6u+K3C2yPvGO9630e8n5jP/oD9UPVR72PXJ79PDxfSFhb+BQOY8/wldxZ1AAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAACxMAAAsTAQCanBgAAAAHdElNRQfiBgEJMgvV1zSaAAAABmJLR0QA/wD/AP+gvaeTAAAhy0lEQVR42u1dh1tUV9PfXUwzthijJtYk9thFJYqNLsKCiIjGGIOxxCgiotLbLnX7LttYYMGlIyAivSzrFWPXGEtIjKnv+z5f+Ru+8905t+yuosGIEWTnee6z7e65986ZM2fOnJnfcDj/BDnxlei5G3mbr2x5rgbOXf5xTL89klu2LgVea4gfxj/zn5enitHM6HQjvHLdFQTHV9F3/lRffICvuCxFhJzJBuDguJNHQM4zMNlDRCwTUH+GhpYLJOjDLwvM0Mj48MY+NsTXII5fDpoRnWpcLpAhr4JyNHa7/hY0MiXa3LdGeAEq6tbJQ9zU4QKNcPnkZ79nlZkAJdWQmxy56kquOAVq0OQY8i7cs4k+txGV1xXIvJ8eb0ay9h4fJx8V4jjo79ERw/ngv/1n7uo0tutmfq6LeqY/Rxq7gmbGC/AgY4VtvcjyTI18HCssgjECjazPNeVzA1XIaWMfRmvz9YcjFidmqubFZ7Cj1C2vjBZ1JVp/slrw1AZcj1dmYJGGoU6P1PW6U1QD/grkG18d24cxQQ0sjqec8DZWoMiGjn3s6PQQE31qgBegRpxAJVqtLUZv+svRtPjzzzge/LT4DyNDDJfHRbajwzltsx0j46k8jzESvi/t+lxf6ctVeFx3yRNF84i6PfjFXt1dyl689fv7w5n3c2LTEXHnz5H/GBdA56xMkWLd40zO0XPjhQZ4n9pBBHP81Xhgc0nLapiv3Pzx53lF/Xrx+bHpWuZ9afeVqT5iLavHliZnwwyLnPiKX0EhMpYHl9QynLXpxHNffENctQAaHr9VV53d0L7KWZCN8EHfAHDBTX+KVKglaMFBUx2r5kh9OS22CyVXXPZ47puo6b4/nmmUecrhATkt8jaLM74RIWXxuJA3wvGQsEbMB8ea0Ogj7aTOlfXTKPKxNm77pO5xp1M8jeXIM7+MYj+f+m1qXBcaf6ILbUlviOhXmdieXX+Q4y1Do4M0p3ibZIjnQemJtbkVqPvun+/yfIH9FjT5ZBd1Q4Ev2nByI4cqORLe3aapO6TpCt2S0RjB8RQRYJTK6q45D61pY4/s3O6BcSduUuKlXp+fUnMi6/SlVZzVGS/xRmwXMCuFL+dGhm9W2S2a3/aWtPyzdxDQ+zIu2UR4/CPXn7O3wOAwKRnK6TAvtv28xXQ6y8lPzi6KGq78POqFGzXLGa8FSXMTMgyuGiMaHqjuodwBahSh7QyWn7n8YtS38cKVGfgmSMvKOVWKDpkqQ8G62qAuRK3f/Taew1eTKx4tPvpfMSkNJ1hOCMHtIsOuF+dkCb4Z91wTslu38akZ9MOdBaZ+uQGel8KySijJsv3uWGVdoDPtTALOwEKRHbp8BXonxIBeiE6ALliYmIEWJKQjZonLOKTAsrLjBH0o2h8sbbz2Rz8IaSCYdWp8EXKRrqeEUsreBHDEi7asbG8ArKzdhTe+WnWiPKt/OMGnbMuZewqM527eG4M5QBu7KzPkrHnHnAc3PTWmC+VdeDhn5bEySb+sNylh09BPqMEXXJwsQkuElDywNmagmhXMaScpF19p572pz38THuSSL0Bh19dM96wQKpA3KQ8gE9ce/A+PssiBC2bS1uxAnnGVif0nnJtVj99EICWwcBOfaotI017yJ8jBa1vz0JToTsRa6f1FvA1yCzbn4cIgIwzbSWJGBtwYd7MO+zgnnyRvwkv0AmwM+snJpR32Oq05WpnhbijBrm+OrxR9EN6InUnL0iyiiaEatD2r6eALUdvgdJsQrK62/W58eAOaFN2B3o1qoVjvLUXHDWb+C53AZKfJdQWf5Ia7FHnF1SYe03cFfvglqZ59yO88ZC/X8HXQYKbzd37ncdzkDhEqMZMzCsxCsC/mJh66DOF6qQiOt/12A3eFcGgxZNoX+SU8f/mTZ9fV6a8+Q94L0dVyvfq2Tbzs6zLFq8kFNwkxOljb8Ey6xNIz9ZV5fk3Ddws56/5aIe4zle560m87JM0HBzUTnkUBLohL0xZ/d3+R6/HTp14ZKViwx6i1c3HFphnAgwBL2GWpWWhmbIrR9vdcc/c8WF+vzMkrpdZaYlTadZ8dFjszGw4w74UVF9YPSqYsTRajFeBKEUooT4ZAhh0JiwUiJG4xuzDngLtlrboAtT74efyOrKYUWEYFCk9H8bzF1ArYX4t4/pSiPf/9H7wB+8Cys5d69ZAtS85GjDQwDg3s7BJkI1jS236/XluIgstqM/CwgrAGkgF4XckseuG9t3UfKyj1bOSAYsIRvRnvBkOoRW+/f3RcULQ0lXroFakyKzNoX9dy2t3kmVeKVutLqAf1kiLGxYA9AJshzEmOeF4SS2/XHhhKkewlYdm363M6OxbDQ82ISzVKWztW2J5TdeXmROcUkcw2coz1uZH6ASQHnD3r9MWYEWNDNbce33iE8BMVgm234TuK8HmkcWYZEEwI150PcfIneyxAgd1p2HkkkEhWJGfZORdB/BkXmy0zGMf0KrEWM2KDoRQ/4CcHis4xu6v2vkAl9opMiTWjQ6V3QqPyzIH7FW07XzojlkWUKUB8sROb3i7muYktltv/xgqNVI4y/NDJEuvDk8PCTjLI9z6Fldjrti6PkohZYUWt+KEfZUSgCo0MK0HTotvR0cq7QdGFF/wWHqpQv3RG7JG37KZuUsOOZ3AzYiUXoEdcTymRXXFpFeXO0sYuo+MtgBHLBBRDPAzFmAng/VutNzEi/7u1PaunkLeZCjqceLwVnzdue15tSFZj+MBQEqDJaRd2b+Oa1fx0ZNjZW7fHLkpKp2LXDCbKB1tYwfhitVZlaS8JTkFaNCWmHTNimZCg1iLe4oETRLk1sykcM8Ff1SsjHjvoDW2v/LJEH1IvMNIAXvFLv//xxub0s2ImRATahZjWsftOY8/o5JhO9F5Ucy2+sKeICEitixpQU6hzRKmMDfPDMbQKeqgorQ5wm7ATxskJQ8E7vxJBCMo3dS3ocs+/38AM3UxKAKmEIf52aoIF+6bBSR6gvYwfnOcjR4u/MakGpFFVcaHnA5jaQHFO+JzUD5to5zsYQ3zyla+jGEF+XnWM2gHyyCtHbsZSVHu/Z3laGXEI7yyRSvL17fl4ZoCHn3oC3NRteAhMCNHjNirM9z4Y8Ga1Z0xVIh4qG6VPXHY7qwtk20pq8aaArOLafI6nHI0OK0dT48+jabj3zWjk4Zar7B9AZ5CWJrQ9OFdgEGNEPgDPOwfNCivMt/2p/s5/xsSfuetzsPw7cnnRs6qz539fZ37bK2+L5LiJ73ODcl7NvYJNcVXR5JAhOJ4ShDdEIJIUnLjU5gjiblQTfvHV0RwHOchBQ47k1dedhzwTeJtklpP55/2GNBNGbFE3cfyVaKfI6nsccuTkKbNgn6OPAg0419o/uip1o1aHb/jKO/o9bH2gU2nnD1NhzTBrv5G1JJceNCl2ilqGztAYHaJrYKIdbckvsTY6xnjBd2hwgTSVX98k7+jtpy2pZyL0zXfmvdLPjwOp/J+eNeJ58nTiqy0Ffjlo3v5C/V+dNvfLvFczsn2Yj8z8LFnb7/Jz6l4pBkTqzwdx/Z49feyDkJyKV8dM9pKgjMpLrn/nv9/IO0IHPQO43mLCP7n2RF/ODc06u/KV0wML9hZqV0WW9CnMPqCgRKqsu+X+SjEgydTtsSayPKMv50Kq8sI0KVaE4wJUta8EA7q++21YYGKt3YZJ0YUrH+ks3fN7O3+nofjAUiGlOLm+st97O0fbdGv+oGJCyinCjXkfW9vgOzcxDTEZTIuSspGPQhdve/68mHQ97HCbbtxdEiZriQbwB+a3xFMXvAQlV9bD+4yqbtdBwYCWq7+wWfgNt++NWkanbsHmLCSoUMyQoYiy0zggw9LzEw9HuJC/76w6S1mLmxQopuiCr7Hl9gwqqwK29aiMiS3C2sG14lyanK2A9HMI9lqaag38gtimWdECvIpckJCphZCfpckStM5Qjr/jeogQx0NBHFSbd1DhPUq87ffmJkUb/P7R5/qiQcOE2TFpVBAHxDCwwRwSkiFZaEFsNqq9fnec7Tb+GgO1de/kLf2VG6BFTpvUiIJRUFIpT3xriHFmxaXBMTQWJWSpmWAuJoUOB3LQ75ck2Ue+uOdTyUPz9hc0Wjd1NfSeJsWM1VEVeMpdcKBYO6AeNiCJmgnO3PxunO33OKUTdEKyxCbyjTmkdmE+8AqhPRd//e2tPfLWaGbXm419oHe5Z+wrZGMo/ZOrTwwYJozdosVzvEuaRPTob5+R09/ClCxrHiMwg9QJTMQbG+ojkGEmFNy4vbyo4+4yJrXQdpuft0GKWq79xCpervsA2a9svvFgxNjtesyEJUkQ3Nnp0tt5CWfOecVUNfouS6KGxgqbWCcc5JWtxoEcSW3EdvwHOv2RlQY/OTpWYMFwYAc1bTt25V3ddyyfCGy99uvwl86EJRElKo4nhZIzIybZCGLtlq1JefS8/acqd34SJ9Tb9T6tI+DVVZWHmXCgrvko7uVNTwgNgjgIUkFOjjGXYd3wTenL1w1cb8gZ1SDR6asuXT/+NAxyieGhYEoE3C04HlWAj8Y9wvmQdQ1M2FFWm8K44XoNAwL94K9Co49SwV2cdRkvf0iQ45ISV29KGpru3h0xJz7dAFPjcvbB7UX/UUYwShGYEFJam4Yb3ijthQFUOOFboflWJrhkvPzALq6nmMLogTHsISMqiR4cWrM4MVu1TJiJLcEVgl6iXplZQUDNFI8xAeIWnhAU9l74WTT6CBXSw1k9AJiAE8WYIC1QYuSYXRlRghPEAfdhdrwgfxm2DaTs+McMAPxHOkB8jSwXR7bBsb3ijOBJwwEHgZIK8sMYC3r7CCUJ3DXpA0AScIwjhdgEczvELuF53S2LKOm6i3MefMTq+GX0Q4PZjMEPmNkB0v6NZVgKPI2V6EBN41HKCOglLZyUtje25uMAL1YS1ooGgCRgOCqltacwzICG/k6D3g3W4emz64cHwz6OFxZhGAqsDLNZ+wAjSxmppPuUTiLk4r1/vfW4nUAdk0604yCvGXGdlNG0TjwQdIKUsO8t6/DAEW3AEC8xIa66hO2HOQlCg7ONYnTTmzAABBPsWXH7zjzZ2dveLM6oTWwkBHwCJgNEvZ6s/t4vvfLaWoxo8LIJ7ykGKp8a3cqlp7UVB01YVyxOEqtAF8DwYPDFgBGeeVR+xPqT1fkMMAEbKEpKxuQoKuR3YlQHDfeY2zR9dz8BlzwPmTruTe9TmC99zAgzYjGecSLVCA/PzApeNgHgb/orH7AILXTY7+vbjVgXAP7ZhONt1cwMktd0Z87A8Ch7Stk5vFcpsP2OHB575a04jdCjoIwF4ABJWJtbUsjYHtbAcVLh+qnQ9ITzmAkfnGzHjNqZ3XYAX3egkHNEsaxPDAjU0/HOFLCHR15pCiMFcIgt3RuLO3sWYYYyQ8xPSQ6DNswAOKbHdFDi7yknFh0yqTkDitwViEE7oVIC6DGNk0aU6FFcGEPTzTlJrec9YFr0JiXCVU+l/7zhr3qAz99M6ZEJRxqpYRDTCbYBxhubvbcwH6JcBpwDJTirLoJjE+2Ooco357CIj7YGFUydsCOVbbm4ihoKpehYvVlw4e5/cNA4dSjQ+MPn8MMDNg0ow84f/ut1Xf2t+cDIrWn14ZyBSE7ecgs8/DDA4/ZR0pKhwYsejNFNfsbjfEMWntZ8jKXx3oXlyEVNpQbhEB7wJ26S4yEA6EAA0DPhuBmVXP5j6bf3/z0GW6heAx3PYYOEgAd1PnwKfa1q+RSP/400ThOsB7yoZBDzz78M88mzDoNxWyk9MGybAU2L68R5D5OjLWjkkVZsbHXceLgEZ/C7ZQ2OwG/uetKAIXv9Nb6yprff080X1zprjEh/6cba/0PI43V/CeJtUePex6BE5BAgp0G0SGDBu9Lhuq5ADIq4QTTIIt/JHsf6gFxZiiov9+pt2pHdHP/atiJq3JO9PvFEJxoV3oxcRRfZIA0Az8bK1ksxOEP/+YmnT+AhALqAnNKSHgFx1HY9mH+o8vvQ6Oq7fobzv7BGT/v137jjQvVUnQNygRaYPMByoP4OYSvRg15LkA/l5C21zPnaZNia3RgemdsZtE/RvmtVdEXWiM05TXjjBXqeNIRm7y3I57xqJKq+7DIhNLea45kN4o14PqTFt4ns7Y1yzBxICJnwWV511pnrqzgOcpCDHOQgBznonydd4+35nHVZRGbFNVcHNxzEGbVF0wDFG3gbpJZv1G2hDo4MYRoWoDJDZh5nsxI7dHlu2ZZg4dkIB2eGGCWWXPTCzq/AHBwMM3evyTD/YIkWF0haLya2Cs6FO7g0RGjizvwqziYZcvLXIMjP6bj1GwtrUXfl17Ef7NBVcFwyiOAMh6Z4pQlvYJBaAe/3kfbC0yrVuceUp0Dpm8Oq1hAH514xEpZeXo+RwmD/001MhPRxZysopTaSszyNjbtw0CAnRe3VpSAIGE/PXULs+Bs1IPgJdSe46zKJMxd/Gufg6CCl9LJLa3H9AggEcM8inreI6fJDxbL3Q7RVDs4OMtqe3XAQ13D1V+BtsbYbP7/ZX21PDtWUBQvPOYzMwUALDpzS4mASDxmx/GiZ7EVco+v2r8Pe4SvqT3f3THRwfABS662Hw3m+Sgvs/g/bKDVXdv3wwnEvM8tvuK4OL8lycH8A0S5J0z7sPNogIb5StO5+nrZEbRSkf82FnvFN134Z0Zf/7BCdO2hs+36GoydeMr0dlNPCWZtFuB6vzOiP9rZojBFprV1B8H5jQrWXtuF6n1P8o41dfo4eeQmE9xRWComVxyok/dnupwKpaJlIza4aeD7SqqTSbz0cHB+AVNJxdyrHVUhsiKoQ9HfbstYu55kxQrQkTYZONnWFwXcp5d+G8FYKLPtVrX2q0lDTfX+8o5f+ARoToKz/UtoU1ttvZ6/fGRuoMkQtjE9Xz4gTGmfFpqGZMQI0Mz7NuDZDnpZZ3/qXcQzLU8UyKP8DmZ6uOhNbDOrjPYXt3HViwiO+KqXPglV3xVnfcnueo9f6mTYl1ETnnrveK2Pj6xp8INN9ZRJkrGaxKbuQzcoCPSRLkAvZyXMSUg3uYnVK77ZCXsTC+Cw2+/VTVR6Kb7vwGfxmbL2/nOsu+d3JR/1YmO7qyMos7rpsAlfCdcsixm3V1ubUXmerqR8zmANTS7rdHL34nFRJ3H/q8nBHrung/Nh0K8BFClXMZ2WKGKcwL2cz+7Op2uqpVCEwstPt8q/2F5bt/CQ2A58LJaKgrTWaAsj2Y8+bu/9UI9dXhvO6hvPVOJXpeB7B57hJqWQYvjVFChcN2yAihBUX1zP/X/FNkcThl3hBdO7Wd2Og9B10MoB8WEtZUYA/TEEjSkPQkFGQ7k0KxDxyCjloqtwB7ewzlu4CxAQGCAAECaYKN00RWqc/hfJv3sLYetKq694cdzGVQUl2NmRKTdptwAVTOAyAUICSzanDrx4ywiu2ggUsPV7QyV9/pEzg6L1+pprrt8bPjkvPxx1PCwEWCAYZSmAt6gTfMedRVfGyUKA6D/lINWhxYqZNhTxagEih2aAuROvzytD+s83HWJslWP09CALGGOAr2fpHVDaoxioIbCp+Dt4pjcrrCmQNYPMPU1/zk5odPfgX1HjpwShBKVWcMrOh7S8NPj+VPnpekpBCuCA7eYkwm63458xW+KJUP54maCQMZxvhscVPsQWQ2qArwuAQPsZSdqpwi60y2BbDsk8gtU8vZqYN7kYVmrXPZGdnmMx3pj8L+O2QpHFb9SzA5dKkDEXC6bNef/WfMGNx2MzodCM2HgEPJlVOCkYmXQiQnjIEEqtmwIgg0seKAz56AHoQIASQqwpUe/fex3AtYfmlIMj1YhLj2c7nq+g6W1bkAHwEkRrCXYo2JlTHPuagKiT8MJaFgx6nqZ/llkzeZShjPi9JEqlmx6TlHy6r6FPEkbi5zWWFUC4BbJzZcWloXkw6mpeQghalZuNRv0IgYStGUqsNe2FgXpcmkwZomsIKE6EtRtLua95wjearDyZyfGR/PpZXH2hjK/AVyMlPgcZ/Voi+yGz8qu3aA7sd04g8IljZ1rMU3n+yr1APz+3ofRuK1HcFwQgirXC2FulmXW7kJwmZGBGStPT1sdV1zxyPEFlRE+SckoEWpYhZewFPGY9A6jk/MrW40iVEAShhjb4YRTWZv2LaHL5F20PhCSntAJVYVBoGkBFWFB4iYmSgtmnKF3llI7fpmgCGjxdkQC6ZBIb/g/0PzppM4pDO7AjbZw2zbdp6ziYVGuajQnO+LmITsD0kmpRPEkQsXuoCsqNmxablz09K026UaWM/zzPtCysoDfsyvySMrzScIDtTRmqF/Llx6WhJSjY7RTg/YSroVSDI/0CZZQZEBmrKhlXXsyXQJu40XMHoes8AUGOdThRo9J4qqCxYV3/732OgvSlfFpSN5CubHFJAUs6564upJRvNMF8FeidQXc/8Xn3t5viFiRnqRQnZtENIwsIjWkHypCw4FnWIex39vaELPioMK9IVGEaJmSbcyBXFthIaa46kOV8VNmN/Q5+FQGmdPgI16P2oNjT6aDuKrb2PNd0X4savOKsESFJzfcWQF4YAwdkoXJiQHT0ajHXD85Ba5uzJt6ulsVVtDJ95MtW4MFlEOZZoY9B+9FM2AetnsDl6EwpbmwHshTU5+WyBZUYzsMCDJC382lT3NABCe7AijVUYNsnQqLByNC3eglEawwpvYrd6ctlFD86nmcg3sSZ2yAuDy9FSEcfTZs0OBaL5FG4gNso2yhFA4tiu1bFg6ArDwcBcnCzqBUFSjJxTRH3SDMuZ/5C/uaQrWSFgygu7kVoitPQM6yj6ZL/pHCUMfZsqsD+CXGG8tsWAcYimR5sxRuFu0w1shySWfevFdSWXxUfKVUNeGFZGVkowNA8NHGnFSsyx0xT4vaccvekvbwvXddqtMI4UlwfPiRMYFiSks9OEnVEosGoR26nlUUHx0J2iDUdKIACAboOhGO2qqmdR/iftzOuGqewxyLLepgg+heEEEOfjDtWjj+LMuCjvyCOtSFB/H/tT9ivbd3LWZqFFh0rUQ14YNiaR6hEnuDDQhjbzLOA74c8aO2cORhr1liEuaa0HCWrsCvml17esXRifqZ6fmEXj0EpoQF7rchJeGfBN/JmcHtYq85BXIQ2/SAPvgdNprc6EjjV27mPafytY3fMkYM7HKrsDViU5PYzdW4GmxlkwTNOk6A40LrK1lilUvOCgSQsAnu4J1SlDXhgyKrpdKTRVKwQ0w0h288dOMHJY+wL/DohokFntJrasiypPs237cHFVyOwYYT7sNcASk/FIUnsXMhq/X4RWS3Ts6gFeGTRGOD7NNSFl9yVc9qa6++dZXG/JY57HJx7kEvTtsDI0Le4CBVZ2EgShHXnKL+P9irZrD9+EYs5cdwmKK7ro41hOwNoddgH5Nhs8TzTI1Hj+ZV29TBF5ptoEjRYJKLEhqdbMqfLLNyYvTsxUgX3B7GO4wNKT1AirRRrkXVSJNcLGfBvNQAuDa27xqbYff8YBK1/nNB1l4Dmfpg0YTyQ2GGMtLHIlHGMiOuqb7v03jq30jquMh5XU63xNh0MKaPosu/EAz0NJ4SPy7Td7nvWADoElHN5M8pQTH+/KZWuxlF25NnleYqYeVg0wTbhK9dTUYCy3sxNAO8Dr+vwStK3sLKttpocVWGxRvO32ImgXNSMIY/edRlNgaoimUPxocOuGE1Xf86EtQ8ONOcM8pGawgzYL6yMdUmBDY0M1dQB7xggEazQ+62Fn2NFaw0NJLA8vYXModuUX73OWG2RehRXYNvC2waZlDEg4VpEGpf7ydbxhpmq45Q6xC49iWLN7EgE05kOgDr1/tJnUCF1YAMBgBIBvcjnZEJp77RBzD1CsFJxXo4KVDY7ef4Qarvw8iucjt2A3boDmmTTDkyx7arSSwuCvw3EHkIrPFCTJ7CBcXfVFGbZagZka4JXSCmdYrfD+jtxLHF8VixBvv0OpxPh9wz83YRRXMBIZaGt4HRXe2vCV6cZupq0RfjlNeDrzlhHVxI+OmMneqKjt3kd4Jw/bBZo+rOH/SlCU9NKO3k0kDbq3/JRtzPW2VzUddMstpqaGAnq6MFYwEN7o6r/+HAbnbUqokUO1TfvrKVlgeKcgLZoU2USuGLqwoQiagBGItw63tMk7HjpDO3WXfhwLAo+Fx0NG6BvvOGIkn0aNVx+MwmCwfDmlgqET+Tq6Y+maDYygPFKeiDIklTYlzBiBUVj9F+7ZKKnoIg59l3dfd16lKySFgELy9zKWYMEgjcYrll9+m4LtGfE56DjaHlDSTjF623pzDpoQXoemxHbhlQKUOphETw1jj7WiBSlmtiIQm+gTQNkytRfvObK6+0pTPtOVcTZKbKDYaSHwp4G4Hy3URc/dXLbMg4YSIqwZbNS6p5Jo++4h3lb++mzzjrW5IAiltLexGDQEa9m7nii/wvOSWTVLoBZBQeJhWzSUXRBvwUjHoAUo28CCJh5vQ+OOtxLlV/7AQbEwLY3yz2kALClSKyByunFkcv8dii8878NdLyOc+ApqZJLz7EdhhehzSQOaEKrDldoob6CKDUPjsMtNei7fDJpCCTi8xJQQa7yE5PylFa46UwZsU7sbytCnWlOx/soN7BWsv/TzIiiSyPNTW0PcyOlgzJ4yvDLABU/IqQC0AQgDaIRxka1o6okOVHXjT1yKs/uH/wA0UAdgAGNhcs8m4oyEw5fwvOSdcDoe4Hc4flpqpJMMnvVlfibzu6j2sotf8pnoeXuN+lGh+obhW7QtY7bp6+fsMxq2pzUehLhD5lzzg1+H+RZWxwKS/UpdEQoprrluM0V9PHFbbjcGcSYNWd5mDRqztxzbAdNiCTz6QQAmkQIxnXw//lgLGnO0ufpL4y02waaw5eY3IwNzELuZ5SYmfOJr4h292M8UKDgTBSHosDpgNAXHXUwEpNY+EW+948HD10Xnv3XZXVn/1a6qhn3plm/X2v5+vedfw32jKyC0HXGC89A7e6vRFKjvQE4BH2Ij0IyF4YMTZvTesTY08nDL1ZmJXfVJ5+7YBaS4Hi7fTY7+DsZQhRyKrcKzDgS5F03FnXenkyO4CtejYor4ATQ7+Xne/gJ9XuPNJ1YnKuy6P8M1pTljeFhlC2d3Ld5BfDeKQO8c7UBjI1rRqAjy85Fmct5vr52Z1Gn0ln8bH3vmju/5nv/h2U1hpgs7J2zXWHhu4rvYYIXaOqSBOClUW1HaeW+qo5deynL0zkcz9xQYuR7ZBGx3c+mK0FhNkyr6bX5OCyTnxhnNPk03fh3xrO2f7v5p4n5F0865+4sNpH2C9xE4/pRLmudNXsdNSszdX2gobr8z3dEbA4yMnd/P8I6piX87UNXCAQHxkiIoDssuU/2o+RxvOEF5L08Jwh3sRr3H54PNAG5nP2oVAZ5CvM1OTkkjNiubNsbVxIJ2cnB7kFLzlYcjxNXXXA7ktO30ja+O/TSiXDR/f5H2493GoulfnDLN2G00Lj5gUsH3vonVsV9rOnZIqy+vaLn6y3AH9xzkIAcNHfp/yYlzdX/Hce8AAAAASUVORK5CYII=" alt="JKPKA" width="30" height="30" style="margin-right: 20px;">JKPKA
			</a>

			<!-- Header Navbar -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
					<i class="fas fa-bars"></i>
					<span class="sr-only">Toggle navigation</span>
				</a>
				<!-- Navbar Right Menu -->
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">

						<!-- User Account Menu -->
						<li class="dropdown user user-menu">
							<!-- Menu Toggle Button -->
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<!-- The user image in the navbar-->
								<i class="fas fa-user-circle user-image"></i>
								<!-- hidden-xs hides the username on small devices so only the image appears. -->
								<span class="hidden-xs"><?php echo $name;?></span>
							</a>
							<ul class="dropdown-menu">
								<!-- The user image in the menu -->
								<li class="user-header">
									<i class="fas fa-user-circle img-circle"></i>

									<p>
										<?php echo $name;?>
									</p>
								</li>
								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-left">
										<a href="#" class="btn btn-default btn-flat">Profile</a>
									</div>
									<div class="pull-right">
										<a href="<?php echo $this->config->base_url();?>admin/dashboard/sign_out" class="btn btn-default btn-flat">Sign out</a>
									</div>
								</li>
							</ul>
						</li>
						<!-- Control Sidebar Toggle Button -->
						<li>
							<!-- <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a> -->
						</li>
					</ul>
				</div>
			</nav>
		</header>
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<!-- <div class="pull-left image">
						<img src="" class="img-circle" alt="User Image">
					</div> -->
					<!-- <div class="pull-left info"> -->
						<!-- <p><?php echo $name;?></p> -->
						<!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
					<!-- </div> -->
				</div>
				<!-- search form -->
				<!-- <form action="#" method="get" class="sidebar-form">
					<div class="input-group">
						<input type="text" name="q" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
							<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
							</button>
						</span>
					</div>
				</form> -->
				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu" data-widget="tree">
					<li class="header">MAIN NAVIGATION</li>
					<li class="<?php if($page == 'dashboard'){ echo 'active'; } ?>">
			          <a href="<?php echo $this->config->base_url();?>admin/Dashboard">
			            <i class="fas fa-home"></i> <span>Dashboard</span>
			          </a>
			        </li>
					<li class="<?php if($page == 'post_image' || $page == 'post_add' || $page == 'post'){ echo 'active';}?> treeview">
						<a href="#">
							<i class="fas fa-crow"></i> <span>Dari Lapangan</span>
							<span class="pull-right-container">
								<i class="fas fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li class="<?php if($page == 'post_add'){ echo 'active';}?>"><a href="<?php echo $this->config->base_url();?>admin/data/PostData/add"><i class="far fa-circle"></i>Tambah</a></li>
							<li class="<?php if($page == 'post'){ echo 'active';}?>"><a href="<?php echo $this->config->base_url();?>admin/data/PostData"><i class="far fa-circle"></i>Daftar</a></li>
							<li class="<?php if($page == 'post_image'){ echo 'active';}?>"><a href="<?php echo $this->config->base_url();?>admin/data/PostImage"><i class="far fa-circle"></i>Gambar</a></li>
						</ul>
					</li>
					<li class="<?php if($page == 'location' || $page == 'location_data'){ echo 'active'; } ?> treeview">
						<a href="#">
							<i class="fas fa-compass"></i> <span>Lokasi</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li class="<?php if($page == 'location'){ echo 'active';}?>"><a href="<?php echo $this->config->base_url();?>admin/Location/add"><i class="far fa-circle"></i>Tambah</a></li>
							<li class="<?php if($page == 'location_data'){ echo 'active';}?>"><a href="<?php echo $this->config->base_url();?>admin/Location/"><i class="far fa-circle"></i>Daftar</a></li>
						</ul>
					</li>
					<li class="<?php if($page == 'monitoring' || $page == 'monitoring_data'){ echo 'active'; } ?> treeview">
						<a href="#">
							<i class="fas fa-camera-retro"></i> <span>Pemantauan</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li class="<?php if($page == 'monitoring_data'){ echo 'active';}?>"><a href="<?php echo $this->config->base_url();?>admin/Monitoring/"><i class="far fa-circle"></i>Daftar</a></li>
						</ul>
					</li>
					<li class="<?php if($page == 'school' || $page == 'school_data'){ echo 'active'; } ?> treeview">
						<a href="#">
							<i class="fas fa-graduation-cap"></i> <span>Sekolah</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li class="<?php if($page == 'school'){ echo 'active';}?>"><a href="<?php echo $this->config->base_url();?>admin/School/add"><i class="far fa-circle"></i>Tambah</a></li>
							<li class="<?php if($page == 'school_data'){ echo 'active';}?>"><a href="<?php echo $this->config->base_url();?>admin/School/"><i class="far fa-circle"></i>Daftar</a></li>
						</ul>
					</li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>
		<div class="content-wrapper">
