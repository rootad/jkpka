<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div class="about_us-banner" style="background-image: url('<?php echo $this->config->base_url();?>media/images/header-siapakami.jpg')">
</div>
<div class="about_us-about_wraper">
	<div class="about_us-about_wraper-title">
		Partner Kami
	</div>
	<div class="about_us-about_wraper-line"></div>
	<div class="about_us-about_wraper-content">
		<div class="container">
			<div class="row">
			<div class="col-sm-12 col-md-3 col-lg-3 partner" class="">
				<img src="<?php echo $this->config->base_url();?>media/images/aml.jpg" alt="">
			</div>
			<div class="col-sm-4 col-md-3 col-lg-3 partner">
				<img src="<?php echo $this->config->base_url();?>media/images/bumn.jpg" alt="">
			</div>
			<div class="col-sm-4 col-md-3 col-lg-3 partner">
				<img src="<?php echo $this->config->base_url();?>media/images/esdm.jpg" alt="">
			</div>
			<div class="col-sm-4 col-md-3 col-lg-3 partner">
				<img src="<?php echo $this->config->base_url();?>media/images/kemen-lh.jpg" alt="">
			</div>
			<div class="col-sm-4 col-md-3 col-lg-3 partner">
				<img src="<?php echo $this->config->base_url();?>media/images/pjt1.jpg" alt="">
			</div>
		</div>
	</div>
</div>