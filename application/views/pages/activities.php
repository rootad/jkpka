<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div class="about_us-banner" style="background-image: url('<?php echo $this->config->base_url();?>media/images/header-siapakami.jpg')">
</div>
<div class="about_us-about_wraper">
	<div class="about_us-about_wraper-title">
		Kegiatan Kami
	</div>
	<div class="about_us-about_wraper-line"></div>
<div class="about_us-learning_model">
	<div class="about_us-learning_model-content">
		<div class="about_us-learning_model-content-line">
			<img class="about_us-learning_model-content-dot" src="data:image/gif;base64,R0lGODlhDQAPAIABAD/E5////yH5BAEAAAEALAAAAAANAA8AAAIPhI+py+0Po5y02hey3roAADs=" alt="">
			<div class="about_us-learning_model-content-side_text">
				<div class="about_us-learning_model-content-side_text-title">Pelatihan Guru</div>
				<p>Dalam rangka peningkatan kepedulian sekolah terhadap sumber daya air. Melalui pengintegrasian materi PKLH pada mata pelajaran Biologi, Kimia, Geografi dan ekonomi. Tercatat 200 orang guru dari SMA dan MA negeri maupun swasta pernah mendapat pelatihan.</p>
			</div>
		</div>
		<div class="about_us-learning_model-content-line">
			<img class="about_us-learning_model-content-dot" src="data:image/gif;base64,R0lGODlhDQAPAIABAD/E5////yH5BAEAAAEALAAAAAANAA8AAAIPhI+py+0Po5y02hey3roAADs=" alt="">
			<div class="about_us-learning_model-content-side_text">
				<div class="about_us-learning_model-content-side_text-title">Pelatihan Siswa</div>
				<p>Guru melatih siswa melalui kegiatan pembelajaran dengan media sungai dan lingkungan sekitar. Siswa melakukan praktik pengukuran parameter kualitas air Yaitu : suhu, pH, DO, CO2, dan Bioassesmen (menggunakan indikator biologi). Kegiatan sosial ekonomi sungai, tektur tanah, berbagai jenis batuan.</p>
			</div>
		</div>
		<div class="about_us-learning_model-content-line">
			<img class="about_us-learning_model-content-dot" src="data:image/gif;base64,R0lGODlhDQAPAIABAD/E5////yH5BAEAAAEALAAAAAANAA8AAAIPhI+py+0Po5y02hey3roAADs=" alt="">
			<div class="about_us-learning_model-content-side_text">
				<div class="about_us-learning_model-content-side_text-title">Olimpiade Air</div>
				<p>Olimpiade Air (Olimpia) diadakan pertama kali di Bendungan Selorejo, Kabupaten Malang. Diikuti oleh seluruh anggota JKPKA dan merupakan Yang pertama di Indonesia. Olimpia I diadakan Th 2003.</p>
			</div>
		</div>
		<div class="about_us-learning_model-content-line">
			<img class="about_us-learning_model-content-dot" src="data:image/gif;base64,R0lGODlhDQAPAIABAD/E5////yH5BAEAAAEALAAAAAANAA8AAAIPhI+py+0Po5y02hey3roAADs=" alt="">
			<div class="about_us-learning_model-content-side_text">
				<div class="about_us-learning_model-content-side_text-title">Pekerti ( PerkemahanTirta Bhakti)</div>
				<p>Pengembangan kegiatan JKPKA yang melibatkan Gerakan Pramuka. Dalam kegiatan perkemahan diadakan Bhakti sosial berupa penyuluhan Pelestarian Sungai, penyuluhan narkoba, pemutaran film hiburan, panggung gembira dengan pementasan Seni budaya lokal, seperti kuda lumping, tari. Peserta diwajibkan susur sungai dan penjelajahan, menggambar poster, membuat puisi dan laporan kegiatan penjelajah tentang pemanfaatan, segi sosial ekonomi, dan upaya pelestarian sungai.</p>
			</div>
		</div>
		<div class="about_us-learning_model-content-line">
			<img class="about_us-learning_model-content-dot" src="data:image/gif;base64,R0lGODlhDQAPAIABAD/E5////yH5BAEAAAEALAAAAAANAA8AAAIPhI+py+0Po5y02hey3roAADs=" alt="">
			<div class="about_us-learning_model-content-side_text">
				<div class="about_us-learning_model-content-side_text-title">Penghijauan Lingkungan Sekolah</div>
				<p>Kegiatan ini diikuti 52 sekolah anggota JKPKA se DAS Brantas, yang mencapai areal seluas 276.710 m2 dengan 2000 tanaman.</p>
				<p>Tujuannya untuk menambah koleksi keanekaragan Hayati, merindangkan sekolah dan mengganti tanaman yang ditebang secara liar.</p>
			</div>
		</div>
		<div class="about_us-learning_model-content-line">
			<img class="about_us-learning_model-content-dot" src="data:image/gif;base64,R0lGODlhDQAPAIABAD/E5////yH5BAEAAAEALAAAAAANAA8AAAIPhI+py+0Po5y02hey3roAADs=" alt="">
			<div class="about_us-learning_model-content-side_text">
				<div class="about_us-learning_model-content-side_text-title">Bersih-bersih sungai</div>
				<p>Dilaksanakan di kota Kediri oleh koordinator wilayah Kediri dengan peserta siswa lebih dari 1.500 orang.</p>
			</div>
		</div>
		<div class="about_us-learning_model-content-line">
			<img class="about_us-learning_model-content-dot" src="data:image/gif;base64,R0lGODlhDQAPAIABAD/E5////yH5BAEAAAEALAAAAAANAA8AAAIPhI+py+0Po5y02hey3roAADs=" alt="">
			<div class="about_us-learning_model-content-side_text">
				<div class="about_us-learning_model-content-side_text-title">Kampanye Pelestarian Sungai</div>
				<p>JKPKA ikut berpatisipasi dalam kampanye pelestarian sungai di wilayah SOR Mlirip Mojokerto.</p>
			</div>
		</div>
		<div class="about_us-learning_model-content-line">
			<img class="about_us-learning_model-content-dot" src="data:image/gif;base64,R0lGODlhDQAPAIABAD/E5////yH5BAEAAAEALAAAAAANAA8AAAIPhI+py+0Po5y02hey3roAADs=" alt="">
			<div class="about_us-learning_model-content-side_text">
				<div class="about_us-learning_model-content-side_text-title">Jelajah Kali Surabaya</div>
				<p>Jelajah Kali Surabaya dilakukan untuk melakukan pemantauan sungai di wilayah hilir yang mengalami pencemaran lebih parah.</p>
			</div>
		</div>
		<div class="about_us-learning_model-content-line">
			<img class="about_us-learning_model-content-dot" src="data:image/gif;base64,R0lGODlhDQAPAIABAD/E5////yH5BAEAAAEALAAAAAANAA8AAAIPhI+py+0Po5y02hey3roAADs=" alt="">
			<div class="about_us-learning_model-content-side_text">
				<div class="about_us-learning_model-content-side_text-title">Menelusuri Jejak limbah</div>
				<p>Limbah Pabrik Tapioka di alirkan ke sawah, hasil pengamatan menunjukkan ada peningkatan kesuburan tanaman padi.</p>
				<p>Kemudian air limbah dari sawah mengalir ke sungai, alam membersihkan limbah.</p>
			</div>
		</div>
		<div class="about_us-learning_model-content-line">
			<img class="about_us-learning_model-content-dot" src="data:image/gif;base64,R0lGODlhDQAPAIABAD/E5////yH5BAEAAAEALAAAAAANAA8AAAIPhI+py+0Po5y02hey3roAADs=" alt="">
			<div class="about_us-learning_model-content-side_text">
				<div class="about_us-learning_model-content-side_text-title">Lomba</div>
				<p>Lomba Penghijauan Lingkungan Sekolah Diadakan antar anggota JKPKA.</p>
				<p>Lomba Pelestarian Sumber daya air, meliputi lomba Karya Tulis Ilmiah, lomba laporan hasil pemantauan kualitas air,lomba puisi, poster dan lomba photo.</p>
			</div>
		</div>
		<div class="about_us-learning_model-content-line">
			<img class="about_us-learning_model-content-dot" src="data:image/gif;base64,R0lGODlhDQAPAIABAD/E5////yH5BAEAAAEALAAAAAANAA8AAAIPhI+py+0Po5y02hey3roAADs=" alt="">
			<div class="about_us-learning_model-content-side_text">
				<div class="about_us-learning_model-content-side_text-title">Temu Ilmiah</div>
				<p>Temu ilmiah diikuti oleh peserta lomba pelestarian sumber daya air, anggota JKPKA dari wilayah Hulu, tengah dan hilir.</p>
				<p>Kegiatan ini diadakan secara rutin pada akhir tahun kegiatan JKPKA.</p>
			</div>
		</div>
	</div>
</div>