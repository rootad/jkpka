<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div class="about_us-banner" style="background-image: url('<?php echo $this->config->base_url();?>media/images/header-siapakami.jpg')">
</div>
<div class="about_us-about_wraper">
	<div class="about_us-about_wraper-title">
		Siapa Kami
	</div>
	<div class="about_us-about_wraper-line"></div>
	<div class="about_us-about_wraper-content">
		<p>JKPKA adalah singkatan dari Jaring-jaring Komunikasi Pemantauan Kualitas Air 
(Water Quality Monitoring Communication Network) adalah organisasi non profit, beranggotakan guru dan siswa 
yang peduli terhadap pelestarian sumber daya air.
		</p>
		<p><div>Didirikan di  Malang  pada 24 Juni 1997.</div>
Oleh Guru-guru SMU, Perum Jasa Tirta I Malang, PKPKLH IKIP Malang (sekarang Universitas Negeri Malang). 
Beranggotakan sekolah SMA, MA, SMK Negeri dan swasta, berkembang ke SMP dan SD di wilayah DAS Kali Brantas 
dan Bengawan Solo . Dan terus berkembang sebagai perintis dan pelopor Sekolah Sungai Indonesia.
		</p>
	</div>
</div>
<div class="about_us-vm" style="background-image: url('<?php echo $this->config->base_url();?>media/images/visi-bg.jpg')">
	<div class="about_us-vm-vision_title">
		Visi
	</div>
	<div class="about_us-vm-vision_content">
		"Mewujudkan sekolah yang peduli terhadap sumber daya air, sungai dan lingkungan sekitar"
	</div>
	<div class="about_us-vm-mission_title">
		Misi
	</div>
	<div class="about_us-vm-mission_content">
		"JKPKA berperan sebagai organisasi yang mengkoordinasi sekolah-sekolah dan mencetak agen kepedulian dalam pelestarian sumber daya air di wilayah DAS Brantas Jawa Timur khususnya, dan Indonesia pada umumnya."
	</div>
</div>
<div class="about_us-learning_model">
	<div class="about_us-learning_model-title">
		Model Pembelajaran
	</div>
	<div class="about_us-learning_model-content">
		Sejak berdirinya, dalam berkegiatan JKPKA menggunakan model pembelajaran "Water Inquiry - GOADC" yang didukung oleh teori Konstruktivistik, Contextual Learning, Cooperative Learning, Active Learning, Scientific Approach. Model pembelajaran ini dikonsepkan oleh Soetarno, selaku Koordinator Pusat JKPKA. 
	</div>
	<img src="<?php echo $this->config->base_url();?>media/images/water-inquiry.jpg" alt="" class="about_us-learning_model-image">
	<div class="about_us-learning_model-title">
		Nilai -nilai
	</div>
	<div class="about_us-learning_model-content">
		Kegiatan JKPKA yang melibatkan 150 guru dan 4500 siswa tiap tahunnya, dilaksanakan dengan menjunjung tinggi nilai-nilai:
		<div class="about_us-learning_model-content-line">
			<img class="about_us-learning_model-content-dot" src="data:image/gif;base64,R0lGODlhDQAPAIABAD/E5////yH5BAEAAAEALAAAAAANAA8AAAIPhI+py+0Po5y02hey3roAADs=" alt="">
			<div class="about_us-learning_model-content-side_text">
				<div class="about_us-learning_model-content-side_text-title">Kebersamaan (Togetherness)</div>
				<p>Bekerja sama melakukan pemantauan</p>
			</div>
		</div>
		<div class="about_us-learning_model-content-line">
			<img class="about_us-learning_model-content-dot" src="data:image/gif;base64,R0lGODlhDQAPAIABAD/E5////yH5BAEAAAEALAAAAAANAA8AAAIPhI+py+0Po5y02hey3roAADs=" alt="">
			<div class="about_us-learning_model-content-side_text">
				<div class="about_us-learning_model-content-side_text-title">Peduli (Caring)</div>
				<p>Peduli terhadap sumber daya air, sungai dan lingkungan sekitar</p>
			</div>
		</div>
		<div class="about_us-learning_model-content-line">
			<img class="about_us-learning_model-content-dot" src="data:image/gif;base64,R0lGODlhDQAPAIABAD/E5////yH5BAEAAAEALAAAAAANAA8AAAIPhI+py+0Po5y02hey3roAADs=" alt="">
			<div class="about_us-learning_model-content-side_text">
				<div class="about_us-learning_model-content-side_text-title">Jaringan (Networking)</div>
				<p>Membentuk komunitas yang saling menjaga lingkungan sekitar</p>
			</div>
		</div>
		<div class="about_us-learning_model-content-line">
			<img class="about_us-learning_model-content-dot" src="data:image/gif;base64,R0lGODlhDQAPAIABAD/E5////yH5BAEAAAEALAAAAAANAA8AAAIPhI+py+0Po5y02hey3roAADs=" alt="">
			<div class="about_us-learning_model-content-side_text">
				<div class="about_us-learning_model-content-side_text-title">Komunikasi (Communication)</div>
				<p>Mengkomunikasikan hasil pemantauan kualitas sumber daya air kepada masyarakat</p>
			</div>
		</div>
		<div class="about_us-learning_model-content-line">
			<img class="about_us-learning_model-content-dot" src="data:image/gif;base64,R0lGODlhDQAPAIABAD/E5////yH5BAEAAAEALAAAAAANAA8AAAIPhI+py+0Po5y02hey3roAADs=" alt="">
			<div class="about_us-learning_model-content-side_text">
				<div class="about_us-learning_model-content-side_text-title">Keberlanjutan (Sustainability)</div>
				<p>Melakukan kegiatan pemantauan secara berkelanjutan</p>
			</div>
		</div>
	</div>
	<div class="about_us-learning_model-title more_space">
		Anggota
	</div>
	<div class="about_us-learning_model-wrap">
		<img src="<?php echo $this->config->base_url();?>media/images/grafik.jpg" alt="" class="about_us-learning_model-wrap-graph">
	</div>
	<div>
		<div class="about_us-learning_model-left">
			<div class="about_us-learning_model-sub_title">
				Daerah Hulu DAS Brantas Jawa Timur:
			</div>
			<ul class="about_us-learning_model-content">
				<li>SMAN 1 Malang</li>
				<li>SMAN 2 Malang</li>
				<li>SMAN 3 Malang</li>
				<li>SMAN 4 Malang</li>
				<li>SMAN 5 Malang</li>
				<li>SMAN 6 Malang</li>
				<li>SMAN 7 Malang</li>
				<li>SMAN 8 Malang</li>
				<li>SMAN 9 Malang</li>
				<li>SMAN 10 Malang</li>
				<li>SMAN 11 Malang</li>
				<li>SMAN 12 Malang</li>
				<li>MAN 3 Malang</li>
				<li>SMA Wahid Hasyim Malang</li>
				<li>SMA Dempo Malang</li>
				<li>SMA Salahudin Malang</li>
				<li>SMA Muhammadiyah 1 Malang</li>
				<li>SMA Arjuna Malang</li>
				<li>SMA Lab UM Malang</li>
				<li>SMA Islam Malang</li>
				<li>SMA St.Maria Malang</li>
				<li>SMA Frateran Malang</li>
				<li>SMA Islam Al Ma arif Singasari Malang</li>
				<li>SMAN Kepanjen Malang</li>
				<li>MAN 1 MALANG</li>
				<li>MAN 2 Batu</li>
				<li>SMAN 1 Batu</li>
				<li>SMAN 2 Batu</li>
			</ul>
			<div class="about_us-learning_model-sub_title">
				Daerah Tengah DAS Brantas Jawa Timur:
			</div>
			<ul class="about_us-learning_model-content">
				<li>SMAN 1 Kediri</li>
				<li>SMAN 2 Kediri</li>
				<li>SMAN 3 Kediri</li>
				<li>SMAN 4 Kediri</li>
				<li>SMAN 5 Kediri</li>
				<li>SMAN 6 Kediri</li>
				<li>SMAN 7 Kediri</li>
				<li>SMAN 8 Kediri</li>
				<li>SMAN 2 Blitar</li>
				<li>SMAN 1 Srengat Blitar</li>
				<li>SMAN 1 Garum Blitar</li>
				<li>SMAN 1 Sutojayan Blitar</li>
				<li>SMAN 1 Kademangan Blitar</li>
				<li>SMAN 1 Gondang Tulungagung</li>
				<li>SMAN 1 Ngunut Tulungagung</li>
				<li>SMAN 1 Tulungagung</li>
				<li>SMAN Tanjung Anom Nganjuk</li>
				<li>SMAN Gondang Nganjuk</li>
				<li>SMAN 1 Nganjuk</li>
				<li>SMAN 2 Nganjuk</li>
				<li>SMAN 3 Nganjuk</li>
				<li>SMAN 1 Kertosono</li>
				<li>SMAN 1 Grogol Kediri</li>
				<li>SMAN 2 Pare Kediri</li>
				<li>SMAN 1 Papar Kediri</li>
				<li>SMAN 1 Boyolangu Tulungagung</li>
				<li>SMAN 1 Kedungwaru Tulungagung</li>
				<li>SMAN 1 Purwoasri Kediri</li>
			</ul>
			<div class="about_us-learning_model-sub_title">
				Daerah Luar P.Jawa:
			</div>
			<ul class="about_us-learning_model-content">
				<li>SMAN 1 Tanimbar Selatan Maluku Tenggara Barat</li>
				<li>MAN 2 Pontianak Kalimantan Barat</li>
				<li>MAN Pagaralam Sumatera Selatan</li>
				<li>MA D.I. Putra Kediri Lobar NTB</li>
				<li>MAN Polewali Mandar Sulawesi Barat</li>
			</ul>
		</div>
		<div class="about_us-learning_model-right">
			<div class="about_us-learning_model-sub_title">
				Daerah Hilir DAS Brantas Jawa Timur:
			</div>
			<ul class="about_us-learning_model-content">
				<li>SMAN 2 Jombang</li>
				<li>SMAN 3 Jombang</li>
				<li>SMAN 1 Bandar Kedung Mulya</li>
				<li>SMAN Ploso Jombang</li>
				<li>SMAN Kesamben Jombang</li>
				<li>SMA DarulUlum 2 Unggulan BPPT Rejoso Jombang</li>
				<li>SMA Muhammadiyah 1 Jombang</li>
				<li>SMA A Wahid Hasyim Jombang</li>
				<li>SMA PGRI 1 Jombang</li>
				<li>MAN Tambak Beras Jombang</li>
				<li>SMAN 1 Gedeg Mojokerto</li>
				<li>SMAN 1 Sooko Mojokerto</li>
				<li>SMAN 1 Kota Mojokerto</li>
				<li>SMAN 1 Taman Sidoarjo</li>
				<li>SMAN 1 Pasuruan</li>
				<li>SMAN 5 Surabaya</li>
				<li>SMA Kristen Petra 3 Surabaya</li>
				<li>SMAN 21 Surabaya</li>
				<li>SMAN 4 Sidoarjo</li>
				<li>SMAN 3 Sidoarjo</li>
				<li>SMAN 1 Taman Sidoarjo</li>
				<li>SMAN 1 Driyorejo Gresik</li>
				<li>SMAN 1 Wringinanom Gresik</li>
				<li>SMAN Plandaan Jombang</li>
				<li>SMA Kristen Petra 1 Surabaya</li>
			</ul>
			<div class="about_us-learning_model-sub_title">
				Sekolah Adiwiyata:
			</div>
			<ul class="about_us-learning_model-content">
				<li>SMPN 16 Surabaya</li>
				<li>SMPN 5 Surabaya</li>
				<li>SMPN 21 Surabaya</li>
				<li>SMPN 39 Surabaya</li>
				<li>SMAN 11 Surabaya</li>
				<li>SMKN 1 Surabaya</li>
				<li>SMA Trimurti Surabaya</li>
				<li>SMAN 4 Surabaya</li>
				<li>SMAN Gondang Mojokerto</li>
				<li>SMAN 1 Mojosari Kab. Mojokerto</li>
				<li>SMKN 1 Pungging Kab. Mojokerto</li>
				<li>SMAN 1 Puri Kab. Mojokerto</li>
				<li>SMAN 2 Probolinggo</li>
				<li>SMKN 1 Probolinggo</li>
				<li>SMKN 3 Probolinggo</li>
				<li>SMAN 4 Probolinggo</li>
				<li>SMKN 6 Malang</li>
			</ul>
			<div class="about_us-learning_model-sub_title">
				Daerah DAS Bengawan Solo:
			</div>
			<ul class="about_us-learning_model-content">
				<li>SMAN 1 Dagangan Madiun</li>
				<li>SMAN 1 Dolopo Madiun</li>
				<li>SMAN 1 Saradan Madiun</li>
				<li>SMAN 1 Nglames Madiun</li>
				<li>SMAN 1 Mejayan Madiun</li>
				<li>SMAN 2 Mejayan Madiun</li>
				<li>SMAN 1 Pilangkenceng Madiun</li>
				<li>SMAN 1 Geger Madiun</li>
			</ul>
		</div>
	</div>
</div>