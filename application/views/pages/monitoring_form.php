<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$year = date("Y");
$month = date("m");
$day = date("d");
$monthList = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember');
$monthMin = $monthList[ $month - 1 ];

?><!DOCTYPE html>
<div class="join-banner" style="background-image: url('<?php echo $this->config->base_url();?>media/images/header-kontakkami.jpg')">
</div>
<div class="join-join_wraper">
	<div class="join-contact_wraper-title">
		Formulir Pemantauan Biologi
	</div>
	<div class="join-join_wraper-line"></div>
	
	<div class="join-join_wraper-content">
		<?php if (!$result) { ?>
		<p>Silahkan mengisikan hasil pemantauan Anda bersama tim pada kolom di bawah ini.</p>
		<p><strong>Administrator berhak menghapus data-data, apabila ditemui hal-hal yang tidak berkenan.</strong></p>
		<?php
		}
		if (strlen(validation_errors()) > 0) { ?>
		<div class="alert alert-danger alert-dismissible fade show" role="alert">
			<?php echo validation_errors(); ?>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<?php } ?>

		<?php
		if ($result) { 
			$ex = explode('<<', $value['hasil_pantau']); 
		?>
		<div class="col-sm-12 col-md-6 col-lg-6 monitoring-item">
			<div class="alert alert-success" role="alert">
				<h4 class="alert-heading">Hasil penelitan yang anda daftarkan</h4>
				<table>
					<tr>
						<td>Bidang</td>
						<td>:</td>
						<td><?php echo $value['bid_pantau']; ?></td>
					</tr>
					<tr>
						<td>Tgl. Pemantauan</td>
						<td>:</td>
						<td><?php echo $ex[1] . '-' . $ex[2] . '-' . $ex[3]; ?></td>
					</tr>
					<tr>
						<td>Tgl. Posting</td>
						<td>:</td>
						<td><?php echo $value['tgl_posting']; ?></td>
					</tr>
					<tr>
						<td>Nama Sekolah</td>
						<td>:</td>
						<td><?php echo $value['nama_anggota']; ?></td>
					</tr>
					<tr>
						<td>Lokasi</td>
						<td>:</td>
						<td><?php echo $value['lokasi']; ?></td>
					</tr>
					<tr>
						<td>Pukul</td>
						<td>:</td>
						<td><?php echo $ex[4]; ?></td>
					</tr>
					<tr>
						<td>Jumlah Total Skor</td>
						<td>:</td>
						<td><?php echo $ex[5]; ?></td>
					</tr>
					<tr>
						<td>Jumlah tipe binatang</td>
						<td>:</td>
						<td><?php echo $ex[6]; ?></td>
					</tr>
					<tr>
						<td>Indeks Kualitas Air</td>
						<td>:</td>
						<td><?php echo $ex[7]; ?> ppm</td>
					</tr>
					<tr>
						<td>Kesimpulan</td>
						<td>:</td>
						<td><?php echo $ex[0]; ?> ppm</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td> </td>
						<td> </td>
					</tr>
				</table>
				<div><a href="<?php echo $this->config->base_url();?>Monitoring">> Lihat daftar pemantauan</a></div>
				<div><a href="<?php echo $this->config->base_url();?>MonitoringForm">> Daftarkan hasil pemantauan Biologi Anda yang lain</a></div>
			</div>
		</div>
		<?php } else { ?>
		<form action="<?php echo $this->config->base_url();?>MonitoringForm" method="post">
			<div class="form-group">
				<label for="nameSchool">Nama Sekolah</label>
				<select class="form-control" id="nameSchool" name="nameSchool">
					<?php foreach ($school as $key => $value) { ?>
					<option value="<?php echo $value->id_anggota; ?>"><?php echo $value->nama_anggota; ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="form-group">
				<label for="location">Lokasi Pemantauan</label>
				<input type="text" class="form-control" id="location" name="location" placeholder="Masukan Lokasi Pemantauan">
			</div>
			<div class="form-group dateMonitoring">
				<label for="date">Tanggal Pemantauan</label><br/>
				<select name="dateDay" id="dateDay" class="form-control custom-select">
					<?php for ($i = 1; $i <= 31; $i++) { ?>
					<option value="<?php echo $i;?>" <?php if ($i == $day) { echo "selected"; } ?>><?php echo $i; ?></option>
					<?php } ?>
				</select> /
				<select name="dateMonth" id="dateMonth" class="form-control custom-select">
					<?php foreach ($monthList as $key => $value) { ?>
					<option value="<?php echo ($key + 1);?>" <?php if (($key + 1) == $month) { echo "selected"; } ?>><?php echo $value; ?></option>
					<?php } ?>
				</select> /
				<select name="dateYear" id="dateYear" class="form-control custom-select">
					<?php for ($i = 2005; $i <= $year; $i++) { ?>
					<option value="<?php echo $i;?>" <?php if ($i == $year) { echo "selected"; } ?>><?php echo $i; ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="form-group">
				<label for="time">Pukul</label>
				<input type="text" class="form-control" id="time" name="time" placeholder="Masukan Pukul">
				<small id="timeHelp" class="form-text text-muted">Contoh: 8:00-13.00</small>
			</div>
			<hr/>
			<p>Parameter Biologi Kualitas Air</p>
			<table>
				<tr>
					<td><strong>Nama Binatang</strong></td>
					<td width="50px"><strong>Skor</strong></td>
					<td><strong>Pilih</strong></td>
				</tr>
				<tr>
					<td>Cacing Larva</td>
					<td><div align="center">1</div></td>
					<td><input type="checkbox" name="check[]" value="1" class="form"></td>
				</tr>
				<tr>
					<td>Larva Mrutu Biasa</td>
					<td><div align="center">2</div></td>
					<td><input type="checkbox" name="check[]" value="2" class="form"></td>
				</tr>
				<tr>
					<td>Belatung Ekor Tikus</td>
					<td><div align="center">3</div></td>
					<td><input type="checkbox" name="check[]" value="3" class="form"></td>
				</tr>
				<tr>
					<td>Lintah</td>
					<td><div align="center">3</div></td>
					<td><input type="checkbox" name="check[]" value="3" class="form"></td>
				</tr>
				<tr>
					<td>Kepiting Sungai</td>
					<td><div align="center">3</div></td>
					<td><input type="checkbox" name="check[]" value="3" class="form"></td>
				</tr>
				<tr>
					<td>Kerang</td>
					<td><div align="center">3</div></td>
					<td><input type="checkbox" name="check[]" value="3" class="form"></td>
				</tr>
				<tr>
					<td>Siput Tanpa 'Pintu'</td>
					<td><div align="center">3</div></td>
					<td><input type="checkbox" name="check[]" value="3" class="form"></td>
				</tr>
				<tr>
					<td>Nimfa Capung Jarum Ekor Tebal</td>
					<td><div align="center">3</div></td>
					<td><input type="checkbox" name="check[]" value="3" class="form"></td>
				</tr>
				<tr>
					<td>Nimfa Capung Dobson</td>
					<td><div align="center">4</div></td>
					<td><input type="checkbox" name="check[]" value="4" class="form"></td>
				</tr>
				<tr>
					<td>Nimfa Capung Sialid</td>
					<td><div align="center">4</div></td>
					<td><input type="checkbox" name="check[]" value="4" class="form"></td>
				</tr>
				<tr>
					<td>Nimfa Lalat Sehari Perenang</td>
					<td><div align="center">4</div></td>
					<td><input type="checkbox" name="check[]" value="4" class="form"></td>
				</tr>
				<tr>
					<td>Larva Lalat atau Nyamuk lainnya</td>
					<td><div align="center">5</div></td>
					<td><input type="checkbox" name="check[]" value="5" class="form"></td>
				</tr>
				<tr>
					<td>Cacing Pipih</td>
					<td><div align="center">5</div></td>
					<td><input type="checkbox" name="check[]" value="5" class="form"></td>
				</tr>
				<tr>
					<td>Larva Kumbang</td>
					<td><div align="center">5</div></td>
					<td><input type="checkbox" name="check[]" value="5" class="form"></td>
				</tr>
				<tr>
					<td>Kumbang Dewasa</td>
					<td><div align="center">5</div></td>
					<td><input type="checkbox" name="check[]" value="5" class="form"></td>
				</tr>
				<tr>
					<td>Kepik - Pejalan Kaki</td>
					<td><div align="center">5</div></td>
					<td><input type="checkbox" name="check[]" value="5" class="form"></td>
				</tr>
				<tr>
					<td>Anggang - anggang</td>
					<td><div align="center">5</div></td>
					<td><input type="checkbox" name="check[]" value="5" class="form"></td>
				</tr>
				<tr>
					<td>Kepik Perenang Punggung</td>
					<td><div align="center">5</div></td>
					<td><input type="checkbox" name="check[]" value="5" class="form"></td>
				</tr>
				<tr>
					<td>Kepik Pendayung</td>
					<td><div align="center">5</div></td>
					<td><input type="checkbox" name="check[]" value="5" class="form"></td>
				</tr>
				<tr>
					<td>Kepik Air lainnya</td>
					<td><div align="center">5</div></td>
					<td><input type="checkbox" name="check[]" value="5" class="form"></td>
				</tr>
				<tr>
					<td>Siput Ber'pintu', > 15 mm</td>
					<td><div align="center">6</div></td>
					<td><input type="checkbox" name="check[]" value="6" class="form"></td>
				</tr>
				<tr>
					<td>Kijing</td>
					<td><div align="center">6</div></td>
					<td><input type="checkbox" name="check[]" value="6" class="form"></td>
				</tr>
				<tr>
					<td>Limpet Air Tawar</td>
					<td><div align="center">6</div></td>
					<td><input type="checkbox" name="check[]" value="6" class="form"></td>
				</tr>
				<tr>
					<td>Nimfa Capung Biasa</td>
					<td><div align="center">6</div></td>
					<td><input type="checkbox" name="check[]" value="6" class="form"></td>
				</tr>
				<tr>
					<td>Nimfa Capung Jarum lainnya</td>
					<td><div align="center">6</div></td>
					<td><input type="checkbox" name="check[]" value="6" class="form"></td>
				</tr>
				<tr>
					<td>Larva Ulat Air (Tanpa Kantung)</td>
					<td><div align="center">7</div></td>
					<td><input type="checkbox" name="check[]" value="7" class="form"></td>
				</tr>
				<tr>
					<td>Larva Ulat Kantung Air (Kantung terbuat dari dedaunan)</td>
					<td><div align="center">7</div></td>
					<td><input type="checkbox" name="check[]" value="7" class="form"></td>
				</tr>
				<tr>
					<td>Nimfa Lalat Sehari Insang Segi Empat</td>
					<td><div align="center">7</div></td>
					<td><input type="checkbox" name="check[]" value="7" class="form"></td>
				</tr>
				<tr>
					<td>Udang Air Tawar dan Udang Biasa</td>
					<td><div align="center">8</div></td>
					<td><input type="checkbox" name="check[]" value="8" class="form"></td>
				</tr>
				<tr>
					<td>Kepik Pinggan Bermoncong Panjang</td>
					<td><div align="center">10</div></td>
					<td><input type="checkbox" name="check[]" value="10" class="form"></td>
				</tr>
				<tr>
					<td>Larva Ulat Kantung Air (kantung dari pasir atau kerikil)</td>
					<td><div align="center">10</div></td>
					<td><input type="checkbox" name="check[]" value="10" class="form"></td>
				</tr>
				<tr>
					<td>Nimfa Lalat Sehari Pipih</td>
					<td><div align="center">10</div></td>
					<td><input type="checkbox" name="check[]" value="10" class="form"></td>
				</tr>
				<tr>
					<td>Nimfa Lalat Sehari Insang Bercabang</td>
					<td><div align="center">10</div></td>
					<td><input type="checkbox" name="check[]" value="10" class="form"></td>
				</tr>
				<tr>
					<td>Nimfa Lalat Sehari Penggali</td>
					<td><div align="center">10</div></td>
					<td><input type="checkbox" name="check[]" value="10" class="form"></td>
				</tr>
				<tr>
					<td>Nimfa Plekoptera</td>
					<td><div align="center">10</div></td>
					<td><input type="checkbox" name="check[]" value="10" class="form"></td>
				</tr>

			</table>
			<div class="g-recaptcha" data-sitekey="6LcWG-4SAAAAALBU46vz1B68SlGU1W0YBXv_Zv9X"></div>
			<div style="text-align: right;">
				<button type="submit" class="btn--green">Kirim</button>
			</div>
		</form>
		<?php } ?>
	</div>
</div>
