<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div class="news-banner" style="background-image: url('<?php echo $this->config->base_url();?>media/images/header-kontakkami.jpg')">
</div>
<div class="news-contact_wraper">
	<div class="news-contact_wraper-title">
		Dari Lapangan
	</div>
	<div class="news-contact_wraper-line"></div>
	
	<div class="news-contact_wraper-content">
		<div class="container">
			<div class="row">
				<?php foreach ($result as $key => $value) { ?>
				<div class="col-sm-12 col-md-6 col-lg-4 news-contact_wraper-content-item">
					<a href="<?php echo $this->config->base_url() . 'news/content/' . $value['ID']; ?>">
						<div class="image" style="background-image: url('<?php if (strlen($value['image']) > 0) { if (substr( $value['image'], 0, 4 ) != 'http') { echo $this->config->base_url(); } echo $value['image']; } else { echo 'http://via.placeholder.com/350x150?text=JKPKA'; } ?>')"></div>
						<div class="title">
							<?php echo $value['post_title']; ?>
						</div>
					</a>
					<div class="date">
						<?php echo $value['post_date']; ?>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
		<nav aria-label="page news" >
			<ul class="pagination justify-content-center">
				<?php for ($x = 1; $x <= $pages; $x++) { ?>
				<li class="page-item <?php if ($x == $page_current) { echo 'active'; } ?>"><a class="page-link" href="<?php if ($x == $page_current) { echo ''; } else { echo $this->config->base_url() . $page . '/index/' . $x; } ?>"><?php echo $x; if ($x == $page_current) { echo '<span class="sr-only">(current)</span>'; } ?></a></li>
				<?php } ?>
			</ul>
		</nav>
		
	</div>
</div>
