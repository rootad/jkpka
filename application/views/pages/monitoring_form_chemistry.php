<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$year = date("Y");
$month = date("m");
$day = date("d");
$monthList = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember');
$monthMin = $monthList[ $month - 1 ];

?><!DOCTYPE html>
<div class="join-banner" style="background-image: url('<?php echo $this->config->base_url();?>media/images/header-kontakkami.jpg')">
</div>
<div class="join-join_wraper">
	<div class="join-contact_wraper-title">
		Formulir Pemantauan Kimia
	</div>
	<div class="join-join_wraper-line"></div>
	
	<div class="join-join_wraper-content">
		<?php if (!$result) { ?>
		<p>Silahkan mengisikan hasil pemantauan Anda bersama tim pada kolom di bawah ini.</p>
		<p><strong>Administrator berhak menghapus data-data, apabila ditemui hal-hal yang tidak berkenan.</strong></p>
		<?php
		}
		if (strlen(validation_errors()) > 0) { ?>
		<div class="alert alert-danger alert-dismissible fade show" role="alert">
			<?php echo validation_errors(); ?>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<?php } ?>

		<?php
		if ($result) { 
			$ex = explode('<<', $value['hasil_pantau']); 
		?>
		<div class="col-sm-12 col-md-6 col-lg-6 monitoring-item">
			<div class="alert alert-success" role="alert">
				<h4 class="alert-heading">Hasil penelitan yang anda daftarkan</h4>
				<table>
					<tr>
						<td>Bidang</td>
						<td>:</td>
						<td><?php echo $value['bid_pantau']; ?></td>
					</tr>
					<tr>
						<td>Tgl. Pemantauan</td>
						<td>:</td>
						<td><?php echo $ex[0] . '-' . $ex[1] . '-' . $ex[2]; ?></td>
					</tr>
					<tr>
						<td>Tgl. Posting</td>
						<td>:</td>
						<td><?php echo $value['tgl_posting']; ?></td>
					</tr>
					<tr>
						<td>Nama Sekolah</td>
						<td>:</td>
						<td><?php echo $value['nama_anggota']; ?></td>
					</tr>
					<tr>
						<td>Lokasi</td>
						<td>:</td>
						<td><?php echo $value['lokasi']; ?></td>
					</tr>
					<tr>
						<td>Pukul</td>
						<td>:</td>
						<td><?php echo $ex[3]; ?></td>
					</tr>
					<tr>
						<td>Temperatur</td>
						<td>:</td>
						<td><?php echo $ex[4]; ?> derajat Celcius</td>
					</tr>
					<tr>
						<td>pH</td>
						<td>:</td>
						<td><?php echo $ex[5]; ?></td>
					</tr>
					<tr>
						<td>DO (Disolve Oxygen)</td>
						<td>:</td>
						<td><?php echo $ex[6]; ?> ppm</td>
					</tr>
					<tr>
						<td>COD (Chemical Oxygen Demand)</td>
						<td>:</td>
						<td><?php echo $ex[7]; ?> ppm</td>
					</tr>
					<tr>
						<td>Transparansi</td>
						<td>:</td>
						<td><?php echo $ex[8]; ?> cm</td>
					</tr>
				</table>
				<div><a href="<?php echo $this->config->base_url();?>Monitoring">> Lihat daftar pemantauan</a></div>
				<div><a href="<?php echo $this->config->base_url();?>MonitoringForm">> Daftarkan hasil pemantauan Biologi Anda yang lain</a></div>
			</div>
		</div>
		<?php } else { ?>
		<form action="<?php echo $this->config->base_url();?>MonitoringForm/chemistry" method="post">
			<div class="form-group">
				<label for="nameSchool">Nama Sekolah</label>
				<select class="form-control" id="nameSchool" name="nameSchool">
					<?php foreach ($school as $key => $value) { ?>
					<option value="<?php echo $value->id_anggota; ?>"><?php echo $value->nama_anggota; ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="form-group">
				<label for="location">Lokasi Pemantauan</label>
				<input type="text" class="form-control" id="location" name="location" placeholder="Masukan Lokasi Pemantauan">
			</div>
			<div class="form-group dateMonitoring">
				<label for="date">Tanggal Pemantauan</label><br/>
				<select name="dateDay" id="dateDay" class="form-control custom-select">
					<?php for ($i = 1; $i <= 31; $i++) { ?>
					<option value="<?php echo $i;?>" <?php if ($i == $day) { echo "selected"; } ?>><?php echo $i; ?></option>
					<?php } ?>
				</select> /
				<select name="dateMonth" id="dateMonth" class="form-control custom-select">
					<?php foreach ($monthList as $key => $value) { ?>
					<option value="<?php echo ($key + 1);?>" <?php if (($key + 1) == $month) { echo "selected"; } ?>><?php echo $value; ?></option>
					<?php } ?>
				</select> /
				<select name="dateYear" id="dateYear" class="form-control custom-select">
					<?php for ($i = 2005; $i <= $year; $i++) { ?>
					<option value="<?php echo $i;?>" <?php if ($i == $year) { echo "selected"; } ?>><?php echo $i; ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="form-group">
				<label for="time">Pukul</label>
				<input type="text" class="form-control" id="time" name="time" placeholder="Masukan Pukul">
				<small id="timeHelp" class="form-text text-muted">Contoh: 8:00-13.00</small>
			</div>
			<hr/>
			<p>Parameter Kimia Kualitas Air</p>
			<div class="form-group">
				<label for="temprature">Temperatur</label>
				<input type="number" class="form-control" id="temprature" name="temprature" placeholder="Masukan Temperatur">
				<small class="form-text text-muted">Satuan: derajat Celcius</small>
			</div>
			<div class="form-group">
				<label for="ph">pH</label>
				<input type="number" class="form-control" id="ph" name="ph" placeholder="Masukan pH">
			</div>
			<div class="form-group">
				<label for="do">DO (Disolve Oxygen)</label>
				<input type="number" class="form-control" id="do" name="do" placeholder="Masukan DO (Disolve Oxygen)">
				<small class="form-text text-muted">Satuan: ppm</small>
			</div>
			<div class="form-group">
				<label for="cod">COD (Chemical Oxygen Demand)</label>
				<input type="number" class="form-control" id="cod" name="cod" placeholder="Masukan COD (Chemical Oxygen Demand)">
			</div>
			<div class="form-group">
				<label for="tr">Transparansi</label>
				<input type="number" class="form-control" id="tr" name="tr" placeholder="Masukan Transparansi">
				<small class="form-text text-muted">Satuan: cm</small>
			</div>
			<div class="g-recaptcha" data-sitekey="6LcWG-4SAAAAALBU46vz1B68SlGU1W0YBXv_Zv9X"></div>
			<div style="text-align: right;">
				<button type="submit" class="btn--green">Kirim</button>
			</div>
		</form>
		<?php } ?>
	</div>
</div>
