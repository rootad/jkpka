<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Profile</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form role="form"  action="<?php echo $this->config->base_url();?>member/Profile/" method="post" enctype="multipart/form-data">
        <div class="box-body">
            <?php
            if (strlen(validation_errors()) > 0) { ?>
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                Gagal ubah informasi profile. <?php echo validation_errors(); ?>
            </div>
            <?php } ?>

            <?php
            if ($result == "1") { ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                Berhasil ubah informasi profile
            </div>
            <?php } ?>
            <div class="form-group">
                <label for="title">Nama Lengkap</label>
                <input type="text" class="form-control" name="fullName" placeholder="Nama Lengkap" value="<?php echo $fullName; ?>">
            </div>
            <div class="form-group">
                <label for="title">Jenis Kelamin</label>
                <select class="form-control select" id="gender" name="gender">
        			<option value="man" <?php if($gender == "man") { echo "selected"; } ?>>Pria</option>
        			<option value="woman" <?php if($gender == "woman") { echo "selected"; } ?>>Wanita</option>
        		</select>
            </div>
            <div class="form-group">
                <label for="title">Tanggal Lahir</label>
                <input type="text" class="form-control date" name="birthday" id="birthday" placeholder="Tanggal Lahir" value="<?php echo $birthday; ?>">
            </div>
            <div class="form-group">
                <label for="title">Sekolah / Instansi</label>
                <select class="form-control select" id="nameSchool" name="nameSchool">
					<?php foreach ($school as $key => $value) { ?>
					<option value="<?php echo $value->id_anggota; ?>" <?php if($nameSchool == $value->id_anggota) { echo "selected"; } ?>><?php echo $value->nama_anggota; ?></option>
					<?php } ?>
				</select>
            </div>
            <div class="form-group">
                <label for="title">Pekerjaan</label>
                <select class="form-control select" id="job" name="job">
					<option value="student" <?php if($job == "student") { echo "selected"; } ?>>Pelajar</option>
					<option value="teachers" <?php if($job == "teachers") { echo "selected"; } ?>>Pengajar</option>
					<option value="workers" <?php if($job == "workers") { echo "selected"; } ?>>Pekerja</option>
				</select>
            </div>
            <p class="lead">Ubah password:</p>
            <div class="form-group">
                <label for="title">Password</label>
                <input type="password" class="form-control" name="password" placeholder="Password" value="<?php echo $password; ?>">
            </div>
            <div class="form-group">
                <label for="title">Ulangi Password</label>
                <input type="password" class="form-control" name="re_password" placeholder="Ulangi password" value="<?php echo $re_password; ?>">
            </div>
            <p>*Kosongi password jika tidak ingin menganti password.</p>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
        <!-- /.box-body -->

    </form>
</div>
