<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?php if ($edit) { echo "Ubah"; } else { echo "Tambah"; } ?> Pemantauan</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form role="form"  action="<?php echo $this->config->base_url();?>member/Monitoring/<?php if ($edit) { echo "edit?edit=" . $monitoringId; } else { echo "add"; } ?>" method="post" enctype="multipart/form-data">
        <div class="box-body"  id="part1">
            <?php
            if (strlen(validation_errors()) > 0 || strlen($error_message) > 0) { ?>
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                Gagal <?php if ($edit) { echo "ubah"; } else { echo "tambah"; } ?> Pemantauan. <?php echo validation_errors(); ?> <?php echo $error_message; ?>
            </div>
            <?php } ?>

            <?php
            if ($result == "1") { ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                Berhasil <?php if ($edit) { echo "ubah"; } else { echo "tambah"; } ?> Pemantauan
            </div>
            <?php } ?>
            <div class="form-group">
                <label for="locationId">Lokasi</label>
                <select class="form-control select" id="location" name="locationId">
                    <?php foreach ($location as $key => $value) { ?>
                    <option value="<?php echo $value->location_id; ?>" <?php if($locationId == $value->location_id) { echo "selected"; } ?>><?php echo $value->name; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="date">Tanggal / Waktu</label>
                <p><input size="16" type="text" name="date" readonly class="form-control datetime" value="<?php echo $date; ?>"></p>
            </div>
            <div class="form-group">
                <label for="participants">Jumlah Perserta</label>
                <input type="number" class="form-control numberonly" id="participants" name="participants" placeholder="Jumlah Perserta" value="<?php echo $participants; ?>">
            </div>
            <div class="form-group">
                <label>Rata-rata Usia Peserta</label>
                <p><input class="radio" id="" type="radio" name="age" value="1" <?php if($age == "1") { echo "checked"; } ?>>0 - 10</p>
                <p><input class="radio" id="" type="radio" name="age" value="2" <?php if($age == "2") { echo "checked"; } ?>>11 - 14</p>
                <p><input class="radio" id="" type="radio" name="age" value="3" <?php if($age == "3") { echo "checked"; } ?>>15 - 17</p>
                <p><input class="radio" id="" type="radio" name="age" value="4" <?php if($age == "4") { echo "checked"; } ?>>18 - Keatas</p>
            </div>
            <div class="box-footer">
                <div id="part1Button" class="btn btn-info <?php if ($showAll) { echo 'hide'; } ?>" style="float: right;">Lanjut</div>
            </div>
        </div>
        <div class="box-body <?php if (!$showAll) { echo 'hide'; } ?>" id="part2">
            <p class="lead">Informasi Tentang Lokasi:</p>
            <div class="form-group">
                <label>Cuaca</label>
                <p><input class="radio" id="" type="radio" name="weather" value="Cerah" <?php if($weather == "Cerah") { echo "checked"; } ?>>Cerah</p>
                <p><input class="radio" id="" type="radio" name="weather" value="Mendung" <?php if($weather == "Mendung") { echo "checked"; } ?>>Mendung</p>
                <p><input class="radio" id="" type="radio" name="weather" value="Hujan" <?php if($weather == "Hujan") { echo "checked"; } ?>>Hujan</p>
                <p><input class="radio" id="" type="radio" name="weather" value="Berkabut" <?php if($weather == "Berkabut") { echo "checked"; } ?>>Berkabut</p>
            </div>
            <div class="form-group">
                <label>Lingkungan Sekitar</label>
                <p><input class="radio" id="" type="radio" name="surroundingEnvironment" value="Pemukiman" <?php if($surroundingEnvironment == "Pemukiman") { echo "checked"; } ?>>Pemukiman</p>
                <p><input class="radio" id="" type="radio" name="surroundingEnvironment" value="Pertanian" <?php if($surroundingEnvironment == "Pertanian") { echo "checked"; } ?>>Pertanian</p>
                <p><input class="radio" id="" type="radio" name="surroundingEnvironment" value="Industri" <?php if($surroundingEnvironment == "Industri") { echo "checked"; } ?>>Industri</p>
                <p><input class="radio" id="" type="radio" name="surroundingEnvironment" value="Pasar" <?php if($surroundingEnvironment == "Pasar") { echo "checked"; } ?>>Pasar</p>
                <p><input class="radio" id="" type="radio" name="surroundingEnvironment" value="Pertambangan" <?php if($surroundingEnvironment == "Pertambangan") { echo "checked"; } ?>>Pertambangan</p>
                <p><input class="radio" id="" type="radio" name="surroundingEnvironment" value="Taman" <?php if($surroundingEnvironment == "Taman") { echo "checked"; } ?>>Taman</p>
                <p><input class="radio" id="" type="radio" name="surroundingEnvironment" value="Cagar Budaya" <?php if($surroundingEnvironment == "Cagar Budaya") { echo "checked"; } ?>>Cagar Budaya</p>
                <p><input class="radio" id="otherSurroundingEnvironmentRadio" type="radio" name="surroundingEnvironment" value="<?php if($surroundingEnvironment != "Pemukiman" && $surroundingEnvironment != "Pertanian" && $surroundingEnvironment != "Industri" && $surroundingEnvironment != "Pasar" && $surroundingEnvironment != "Pertambangan" && $surroundingEnvironment != "Taman" && $surroundingEnvironment != "Cagar Budaya") { if (strlen($surroundingEnvironment) > 0) { echo $surroundingEnvironment; } } ?>" <?php if($surroundingEnvironment != "Pemukiman" && $surroundingEnvironment != "Pertanian" && $surroundingEnvironment != "Industri" && $surroundingEnvironment != "Pasar" && $surroundingEnvironment != "Pertambangan" && $surroundingEnvironment != "Taman" && $surroundingEnvironment != "Cagar Budaya") { if (strlen($surroundingEnvironment) > 0) {echo "checked"; } } ?>>Lain-Lain</p>
                <p><input type="text" class="form-control" id="otherSurroundingEnvironment" name="otherSurroundingEnvironment" placeholder="Isi ini Jika anda memilih Lain-Lain" value="<?php if($surroundingEnvironment != "Pemukiman" && $surroundingEnvironment != "Pertanian" && $surroundingEnvironment != "Industri" && $surroundingEnvironment != "Pasar" && $surroundingEnvironment != "Pertambangan" && $surroundingEnvironment != "Taman" && $surroundingEnvironment != "Cagar Budaya") { if (strlen($surroundingEnvironment) > 0) { echo $surroundingEnvironment; } } ?>"></p>
            </div>
            <div class="form-group">
                <label for="addOnInfo">Informasi Tambahan</label>
                <input type="text" class="form-control" name="addOnInfo" placeholder="Informasi Tambahan" value="<?php echo $addOnInfo; ?>">
            </div>
            <div class="form-group">
                <label>Foto Kegiatan</label>
                <input id="exampleInputFile" type="file" name="image">
            </div>
            <div class="box-footer">
                <div id="part2Button" class="btn btn-info <?php if ($showAll) { echo 'hide'; } ?>" style="float: right;">Lanjut</div>
            </div>
        </div>
        <div class="box-body <?php if (!$showAll) { echo 'hide'; } ?>" id="part3">
            <p class="lead">Parameter Kimia:</p>
            <div class="form-group">
                <label for="temprature">Suhu Lokasi</label>
                <div class="input-group">
                    <input type="number" class="form-control numberonly" id="temprature" name="temprature" placeholder="Suhu Lokasi" value="<?php echo $temprature; ?>" aaa="<?php echo $temprature; ?>">
                    <div class="input-group-addon">&#8451;</div>
                </div>
            </div>
            <div class="form-group">
                <label for="ph">pH</label>
                <div class="input-group">
                    <input type="number" class="form-control numberonly" id="ph" name="ph" placeholder="pH" value="<?php echo $ph; ?>">
                    <div class="input-group-addon">pH</div>
                </div>
            </div>
            <div class="form-group">
                <label for="do">DO (Dissolved Oxygen)</label>
                <div class="input-group">
                    <input type="number" class="form-control numberonly" id="do" name="do" placeholder="DO (Dissolved Oxygen)" value="<?php echo $do; ?>">
                    <div class="input-group-addon">‰</div>
                </div>
            </div>
            <div class="form-group">
                <label for="cod">COD (Chemical Oxygen Demand)</label>
                <div class="input-group">
                    <input type="number" class="form-control numberonly" id="cod" name="cod" placeholder="COD (Chemical Oxygen Demand)" value="<?php echo $cod; ?>">
                    <div class="input-group-addon">mg/L</div>
                </div>
            </div>
            <div class="form-group">
                <label for="transparency">Transparansi</label>
                <div class="input-group">
                    <input type="number" class="form-control numberonly" id="transparency" name="transparency" placeholder="Transparansi" value="<?php echo $cod; ?>">
                    <div class="input-group-addon">cm</div>
                </div>
            </div>
            <div class="box-footer">
                <div id="part3Button" class="btn btn-info <?php if ($showAll) { echo 'hide'; } ?>" style="float: right;">Lanjut</div>
            </div>
        </div>
        <div class="box-body <?php if (!$showAll) { echo 'hide'; } ?>" id="part4">
            <p class="lead">Parameter Biologi:</p>
            <div class="form-group biology">
                <table class="table table-striped table-hover">
                    <tr>
                        <th><strong>Nama Binatang</strong></td>
                        <th width="50px"><strong>Skor</strong></td>
                        <th><strong>Pilih</strong></td>
                    </tr>
                    <tr>
                        <td>Cacing Larva</td>
                        <td><div align="center">1</div></td>
                        <td><input class="checkbox form" type="checkbox" name="cacing_larva" value="1" <?php if ($cacing_larva != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Larva Mrutu Biasa</td>
                        <td><div align="center">2</div></td>
                        <td><input class="checkbox form" type="checkbox" name="larva_mrutu_biasa" value="2" <?php if ($larva_mrutu_biasa != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Belatung Ekor Tikus</td>
                        <td><div align="center">3</div></td>
                        <td><input class="checkbox form" type="checkbox" name="belatung_ekor_tikus" value="3" <?php if ($belatung_ekor_tikus != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Lintah</td>
                        <td><div align="center">3</div></td>
                        <td><input class="checkbox form" type="checkbox" name="lintah" value="3" <?php if ($lintah != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Kepiting Sungai</td>
                        <td><div align="center">3</div></td>
                        <td><input class="checkbox form" type="checkbox" name="kepiting_sungai" value="3" <?php if ($kepiting_sungai != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Kerang</td>
                        <td><div align="center">3</div></td>
                        <td><input class="checkbox form" type="checkbox" name="kerang" value="3" <?php if ($kerang != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Siput Tanpa 'Pintu'</td>
                        <td><div align="center">3</div></td>
                        <td><input class="checkbox form" type="checkbox" name="siput_tanpa_pintu" value="3" <?php if ($siput_tanpa_pintu != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Nimfa Capung Jarum Ekor Tebal</td>
                        <td><div align="center">3</div></td>
                        <td><input class="checkbox form" type="checkbox" name="nimfa_capung_jarum_ekor_tebal" value="3" <?php if ($nimfa_capung_jarum_ekor_tebal != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Nimfa Capung Dobson</td>
                        <td><div align="center">4</div></td>
                        <td><input class="checkbox form" type="checkbox" name="nimfa_capung_dobson" value="4" <?php if ($nimfa_capung_dobson != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Nimfa Capung Sialid</td>
                        <td><div align="center">4</div></td>
                        <td><input class="checkbox form" type="checkbox" name="nimfa_capung_sialid" value="4" <?php if ($nimfa_capung_sialid != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Nimfa Lalat Sehari Perenang</td>
                        <td><div align="center">4</div></td>
                        <td><input class="checkbox form" type="checkbox" name="nimfa_lalat_sehari_perenang" value="4" <?php if ($nimfa_lalat_sehari_perenang != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Larva Lalat atau Nyamuk Lainnya</td>
                        <td><div align="center">5</div></td>
                        <td><input class="checkbox form" type="checkbox" name="larva_lalat_atau_nyamuk_lainnya" value="5" <?php if ($larva_lalat_atau_nyamuk_lainnya != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Cacing Pipih</td>
                        <td><div align="center">5</div></td>
                        <td><input class="checkbox form" type="checkbox" name="cacing_pipih" value="5" <?php if ($cacing_pipih != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Larva Kumbang</td>
                        <td><div align="center">5</div></td>
                        <td><input class="checkbox form" type="checkbox" name="larva_kumbang" value="5" <?php if ($larva_kumbang != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Kumbang Dewasa</td>
                        <td><div align="center">5</div></td>
                        <td><input class="checkbox form" type="checkbox" name="kumbang_dewasa" value="5" <?php if ($kumbang_dewasa != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Kepik - Pejalan Kaki</td>
                        <td><div align="center">5</div></td>
                        <td><input class="checkbox form" type="checkbox" name="kepik_pejalan_kaki" value="5" <?php if ($kepik_pejalan_kaki != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Anggang - Anggang</td>
                        <td><div align="center">5</div></td>
                        <td><input class="checkbox form" type="checkbox" name="anggang_anggang" value="5" <?php if ($anggang_anggang != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Kepik Perenang Punggung</td>
                        <td><div align="center">5</div></td>
                        <td><input class="checkbox form" type="checkbox" name="kepik_perenang_punggung" value="5" <?php if ($kepik_perenang_punggung != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Kepik Pendayung</td>
                        <td><div align="center">5</div></td>
                        <td><input class="checkbox form" type="checkbox" name="kepik_pendayung" value="5" <?php if ($kepik_pendayung != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Kepik Air Lainnya</td>
                        <td><div align="center">5</div></td>
                        <td><input class="checkbox form" type="checkbox" name="kepik_air_lainnya" value="5" <?php if ($kepik_air_lainnya != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Siput Berpintu, lebih besar dari 15 mm</td>
                        <td><div align="center">6</div></td>
                        <td><input class="checkbox form" type="checkbox" name="siput_berpintu" value="6" <?php if ($siput_berpintu != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Kijing</td>
                        <td><div align="center">6</div></td>
                        <td><input class="checkbox form" type="checkbox" name="kijing" value="6" <?php if ($kijing != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Limpet Air Tawar</td>
                        <td><div align="center">6</div></td>
                        <td><input class="checkbox form" type="checkbox" name="limpet_air_tawar" value="6" <?php if ($limpet_air_tawar != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Nimfa Capung Biasa</td>
                        <td><div align="center">6</div></td>
                        <td><input class="checkbox form" type="checkbox" name="nimfa_capung_biasa" value="6" <?php if ($nimfa_capung_biasa != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Nimfa Capung Jarum lainnya</td>
                        <td><div align="center">6</div></td>
                        <td><input class="checkbox form" type="checkbox" name="nimfa_capung_jarum_lainnya" value="6" <?php if ($nimfa_capung_jarum_lainnya != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Larva Ulat Air (Tanpa Kantung)</td>
                        <td><div align="center">7</div></td>
                        <td><input class="checkbox form" type="checkbox" name="larva_ulat_air_tanpa_kantung" value="7" <?php if ($larva_ulat_air_tanpa_kantung != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Larva Ulat Kantung Air (Kantung terbuat dari dedaunan)</td>
                        <td><div align="center">7</div></td>
                        <td><input class="checkbox form" type="checkbox" name="larva_ulat_kantung_air_kantung_terbuat_dari_dedaunan" value="7" <?php if ($larva_ulat_kantung_air_kantung_terbuat_dari_dedaunan != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Nimfa Lalat Sehari Insang Segi Empat</td>
                        <td><div align="center">7</div></td>
                        <td><input class="checkbox form" type="checkbox" name="nimfa_lalat_sehari_insang_segi_empat" value="7" <?php if ($nimfa_lalat_sehari_insang_segi_empat != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Udang Air Tawar dan Udang Biasa</td>
                        <td><div align="center">8</div></td>
                        <td><input class="checkbox form" type="checkbox" name="udang_air_tawar_dan_udang_biasa" value="8" <?php if ($udang_air_tawar_dan_udang_biasa != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Kepik Pinggan Bermoncong Panjang</td>
                        <td><div align="center">10</div></td>
                        <td><input class="checkbox form" type="checkbox" name="kepik_pinggan_bermoncong_panjang" value="10" <?php if ($kepik_pinggan_bermoncong_panjang != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Larva Ulat Kantung Air (kantung dari pasir atau kerikil)</td>
                        <td><div align="center">10</div></td>
                        <td><input class="checkbox form" type="checkbox" name="larva_ulat_kantung_air_kantung_dari_pasir_atau_kerikil" value="10" <?php if ($larva_ulat_kantung_air_kantung_dari_pasir_atau_kerikil != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Nimfa Lalat Sehari Pipih</td>
                        <td><div align="center">10</div></td>
                        <td><input class="checkbox form" type="checkbox" name="nimfa_lalat_sehari_pipih" value="10" <?php if ($nimfa_lalat_sehari_pipih != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Nimfa Lalat Sehari Insang Bercabang</td>
                        <td><div align="center">10</div></td>
                        <td><input class="checkbox form" type="checkbox" name="nimfa_lalat_sehari_insang_bercabang" value="10" <?php if ($nimfa_lalat_sehari_insang_bercabang != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Nimfa Lalat Sehari Penggali</td>
                        <td><div align="center">10</div></td>
                        <td><input class="checkbox form" type="checkbox" name="nimfa_lalat_sehari_penggali" value="10" <?php if ($nimfa_lalat_sehari_penggali != "0") { echo 'checked'; } ?>></td>
                    </tr>
                    <tr>
                        <td>Nimfa Plekoptera</td>
                        <td><div align="center">10</div></td>
                        <td><input class="checkbox form" type="checkbox" name="nimfa_plekoptera" value="10" <?php if ($nimfa_plekoptera != "0") { echo 'checked'; } ?>></td>
                    </tr>
                </table>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>
