<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div class="news-banner" style="background-image: url('<?php if (strlen($image) > 0) { if (substr( $image, 0, 4 ) != 'http') { echo $this->config->base_url(); } echo $image; } else { echo 'http://via.placeholder.com/1366x758?text=JKPKA'; } ?>')">
</div>
<div class="news-contact_wraper">
	<div class="news-contact_wraper-title">
		<?php echo $result->post_title; ?>
	</div>
	<div class="news-contact_wraper-line"></div>
	<div class="news-contact_wraper-date">
		<?php echo date ("d F Y", strtotime($result->post_date)); ?>
	</div>
	<div class="news-contact_wraper-content">
		<?php 
			$content = str_replace('src="wp-content', 'src="' . $this->config->base_url() . "wp-content", $result->post_content);
			echo $content; 
		?>
	</div>
</div>
