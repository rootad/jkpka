<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div class="join-banner" style="background-image: url('<?php echo $this->config->base_url();?>media/images/header-kontakkami.jpg')">
</div>
<div class="join-join_wraper">
	<div class="join-contact_wraper-title">
		Ayo Bergabung!
	</div>
	<div class="join-join_wraper-line"></div>
	
	<div class="join-join_wraper-content">
		<?php
		if (strlen(validation_errors()) > 0) { ?>
		<div class="alert alert-danger alert-dismissible fade show" role="alert">
			<?php echo validation_errors(); ?>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<?php } ?>

		<?php
		if ($result) { ?>
		<div class="alert alert-success alert-dismissible fade show" role="alert">
			Permintaan berhasil, tim kami segera menghubungi anda.
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<?php } ?>
		<form action="<?php echo $this->config->base_url();?>join" method="post">
			<div class="form-group">
				<label for="name">Nama Anda</label>
				<input type="text" class="form-control" id="name" name="name" placeholder="Masukan Nama Anda">
			</div>
			<div class="form-group">
				<label for="nameSchool">Nama Sekolah</label>
				<input type="text" class="form-control" id="nameSchool" name="nameSchool" placeholder="Masukan Nama Sekolah">
			</div>
			<div class="form-group">
				<label for="email">Email</label>
				<input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Masukan Email">
				<small id="emailHelp" class="form-text text-muted">Contoh: joko@example.com</small>
			</div>
			<div class="form-group">
				<label for="message">Pesan</label>
				<input type="text" class="form-control" id="message" name="message" placeholder="Masukan Pesan">
			</div>
			<div class="g-recaptcha" data-sitekey="6LcWG-4SAAAAALBU46vz1B68SlGU1W0YBXv_Zv9X"></div>
			<div style="text-align: right;">
				<button type="submit" class="btn--green">Kirim</button>
			</div>
		</form>
	</div>
</div>
