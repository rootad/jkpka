<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div class="monitoring-banner" style="background-image: url('<?php echo $this->config->base_url();?>media/images/header-kontakkami.jpg')">
</div>
<div class="monitoring-contact_wraper">
	<div class="monitoring-contact_wraper-title">
		Pemantauan
	</div>
	<div class="monitoring-contact_wraper-line"></div>
	
	<div class="monitoring-contact_wraper-content">
		Situs ini meyediakan kesempatan untuk berbagi informasi hasil pemantaun kualitas air dari berbagai sekolah. Sekolah Anda juga dapat bergabung di sini. Untuk membagikan hasil pemantauan kualitas air di daerah Anda.
		<div><a href="<?php echo $this->config->base_url();?>MonitoringForm">> Daftarkan hasil pemantauan Biologi Anda</a></div>
		<div><a href="<?php echo $this->config->base_url();?>MonitoringForm/chemistry">> Daftarkan hasil pemantauan Kimia Anda</a></div>
		<div class="container">
			<div class="row">
				<?php 
				foreach ($result as $key => $value) {
				$ex = explode('<<', $value['hasil_pantau']); 
				?>
				<div class="col-sm-12 col-md-6 col-lg-6 monitoring-item">
					<div class="alert alert-success" role="alert">
						<?php if ($value['bid_pantau'] == "Kimia") { ?>
						<h4 class="alert-heading"><?php echo $value['lokasi']; ?></h4>
						<table>
							<tr>
								<td>Bidang</td>
								<td>:</td>
								<td><?php echo $value['bid_pantau']; ?></td>
							</tr>
							<tr>
								<td>Tgl. Pemantauan</td>
								<td>:</td>
								<td><?php echo $ex[0] . '-' . $ex[1] . '-' . $ex[2]; ?></td>
							</tr>
							<tr>
								<td>Tgl. Posting</td>
								<td>:</td>
								<td><?php echo $value['tgl_posting']; ?></td>
							</tr>
							<tr>
								<td>Nama Sekolah</td>
								<td>:</td>
								<td><?php echo $value['nama_anggota']; ?></td>
							</tr>
							<tr>
								<td>Lokasi</td>
								<td>:</td>
								<td><?php echo $value['lokasi']; ?></td>
							</tr>
							<tr>
								<td>Pukul</td>
								<td>:</td>
								<td><?php echo $ex[3]; ?></td>
							</tr>
							<tr>
								<td>Temperatur</td>
								<td>:</td>
								<td><?php echo $ex[4]; ?> derajat Celcius</td>
							</tr>
							<tr>
								<td>pH</td>
								<td>:</td>
								<td><?php echo $ex[5]; ?></td>
							</tr>
							<tr>
								<td>DO (Disolve Oxygen)</td>
								<td>:</td>
								<td><?php echo $ex[6]; ?> ppm</td>
							</tr>
							<tr>
								<td>COD (Chemical Oxygen Demand)</td>
								<td>:</td>
								<td><?php echo $ex[7]; ?> ppm</td>
							</tr>
							<tr>
								<td>Transparansi</td>
								<td>:</td>
								<td><?php echo $ex[8]; ?> cm</td>
							</tr>
						</table>
						<?php } else { ?>
						<h4 class="alert-heading"><?php echo $value['lokasi']; ?></h4>
						<table>
							<tr>
								<td>Bidang</td>
								<td>:</td>
								<td><?php echo $value['bid_pantau']; ?></td>
							</tr>
							<tr>
								<td>Tgl. Pemantauan</td>
								<td>:</td>
								<td><?php echo $ex[1] . '-' . $ex[2] . '-' . $ex[3]; ?></td>
							</tr>
							<tr>
								<td>Tgl. Posting</td>
								<td>:</td>
								<td><?php echo $value['tgl_posting']; ?></td>
							</tr>
							<tr>
								<td>Nama Sekolah</td>
								<td>:</td>
								<td><?php echo $value['nama_anggota']; ?></td>
							</tr>
							<tr>
								<td>Lokasi</td>
								<td>:</td>
								<td><?php echo $value['lokasi']; ?></td>
							</tr>
							<tr>
								<td>Pukul</td>
								<td>:</td>
								<td><?php echo $ex[4]; ?></td>
							</tr>
							<tr>
								<td>Jumlah Total Skor</td>
								<td>:</td>
								<td><?php echo $ex[5]; ?></td>
							</tr>
							<tr>
								<td>Jumlah tipe binatang</td>
								<td>:</td>
								<td><?php echo $ex[6]; ?></td>
							</tr>
							<tr>
								<td>Indeks Kualitas Air</td>
								<td>:</td>
								<td><?php echo $ex[7]; ?> ppm</td>
							</tr>
							<tr>
								<td>Kesimpulan</td>
								<td>:</td>
								<td><?php echo $ex[0]; ?> ppm</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td> </td>
								<td> </td>
							</tr>
						</table>
						<?php } ?>
						
					</div>
				</div>
				
				<?php } ?>
			</div>
		</div>
		<nav aria-label="page news" >
			<ul class="pagination justify-content-center">
				<?php for ($x = 1; $x <= $pages; $x++) { ?>
				<li class="page-item <?php if ($x == $page_current) { echo 'active'; } ?>"><a class="page-link" href="<?php if ($x == $page_current) { echo ''; } else { echo $this->config->base_url() . $page . '/index/' . $x; } ?>"><?php echo $x; if ($x == $page_current) { echo '<span class="sr-only">(current)</span>'; } ?></a></li>
				<?php } ?>
			</ul>
		</nav>
		
	</div>
</div>
