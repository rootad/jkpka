<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div class="contact_us-banner" style="background-image: url('<?php echo $this->config->base_url();?>media/images/header-kontakkami.jpg')">
</div>
<div class="contact_us-contact_wraper">
	<div class="contact_us-contact_wraper-title">
		Kontak Kami
	</div>
	<div class="contact_us-contact_wraper-line"></div>
	
	<div class="contact_us-contact_wraper-content">
		<div class="contact_us-contact_wraper-content-sub_title">
			Koodinator Pusat:
		</div>
		<div class="clearfix">
			<img class="contact_us-contact_wraper-content-dot" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADASURBVEhL7cyxCQJBEIXhNTEztgE7sAeLsA3bsA2LsAIR7MAGjM1M1PfDHcjwvLnVDe+HD469mSlTLZvJvsN30+ZykFeHb96atJCj9Md7vPHvr5ZykXi8xz9mfmolV3GHPzHDbFVruYk76DDLzqg2chd3aAg77A62lYe4A2Owyw3bTp7iFmtwg1tfc0s10txSjTS3hJibQZpbQszNIM0tIeZmkOaWEHMzSHNLiLkZpLklxNwM0s4yZtHNnGSqplLeiy/MxWEhz5QAAAAASUVORK5CYII=" alt="">
			<div class="contact_us-contact_wraper-content-side_text">
				<div>Jl. Dewi Sartika No. 27</div>
				<div>Jombang 61418 - JAWA TIMUR</div>
			</div>
		</div>
		<div class="clearfix">
			<img class="contact_us-contact_wraper-content-dot" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAC7SURBVEhL7ZTLCcJAFEUH3FqCXViE4sZSbMVSLMQqNB24FfQe8MGQ+4Qxg5BFDhxI5t6XPykLs+IgB/nqlGPspXGX2cAUb9KI8CinnIwZZmPfqIO1PMunjPVv0qHLDMS6EcFFblgQW3mVkY0lowPMMBuZUQ8+5EmuPrLNWmuORh2G2RW23qGRlXD8jKHlHRlZqTa+ktavzMhKPRpZqUcjK/VoZKUejazUo/H3nx2/WIJs4Bc5xk4uzIJS3vADGzOIzf2yAAAAAElFTkSuQmCC" alt="">
			<div class="contact_us-contact_wraper-content-side_text">
				<div>tr_jkpka@yahoo.co.id</div>
			</div>
		</div>
		<div class="clearfix">
			<img class="contact_us-contact_wraper-content-dot" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAESSURBVEhL5ZJNSgNBEEbHjZtgTqFr416ykETBI0lwp4fQI0iuEEkOkbhWskgkIURduNL3DdUwNHR6arLTBw+mi6+qf5jiX3CJD/iCn+bMan1szAmO8SfjMx6jiy6uUQOWOMAzbJn6Vu0dlVmhemqh02xQjU94hCnaOMSwSa2bTDAMP1AhgzJhk5EKu+ihggvcdfIY3SQ814UKKR5RIb2vl1tUr/6uJPoVFTotVz46qN5puUqwRYU8zxNQj3o1I4kCsinZ/hComsPV4wobrh5X2HD1uMKGq8cVNlw9b1g7bMT5V0xyjfEmOapZDb/CRtzhN37hjQrGOWr4vFztwQdWTxurA+xFuEE8WCe/x0P8cxTFLw/SjgcnVcn3AAAAAElFTkSuQmCC" alt="">
			<div class="contact_us-contact_wraper-content-side_text">
				<div>0321-867-412</div>
				<div>081-555-700-699</div>
			</div>
		</div>
	</div>
</div>
