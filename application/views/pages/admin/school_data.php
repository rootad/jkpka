<div class="box box-primary">
    <div class="box-body">
        <?php
            if ($status_delete == "1") { ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                Berhasil hapus sekolah
            </div>
            <?php } ?>
        <table id="post_list" class="table table-bordered table-hover display" style="width:100%">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>Wilayah</th>
                    <th>Alamat</th>
                    <th>Telepon</th>
                    <th>Jabatan</th>
                    <th>Menu</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Nama</th>
                    <th>Wilayah</th>
                    <th>Alamat</th>
                    <th>Telepon</th>
                    <th>Jabatan</th>
                    <th>Menu</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>