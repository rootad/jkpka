<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?php if ($edit) { echo "Ubah"; } else { echo "Tambah"; } ?> Lokasi</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form role="form"  action="<?php echo $this->config->base_url();?>admin/School/<?php if ($edit) { echo "edit?edit=" . $id_anggota; } else { echo "add"; } ?>" method="post" enctype="multipart/form-data">
        <div class="box-body">
            <?php
            if (strlen(validation_errors()) > 0 || strlen($error_message) > 0) { ?>
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                Gagal <?php if ($edit) { echo "ubah"; } else { echo "tambah"; } ?> sekolah. <?php echo validation_errors(); ?> <?php echo $error_message; ?>
            </div>
            <?php } ?>

            <?php
            if ($result == "1") { ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                Berhasil <?php if ($edit) { echo "ubah"; } else { echo "tambah"; } ?> sekolah
            </div>
            <?php } ?>
            <div class="form-group">
                <label for="name">Nama</label>
                <input type="text" class="form-control" name="name" placeholder="Nama" value="<?php echo $nameSchool; ?>">
            </div>
            <div class="form-group">
                <label for="name">Wilayah</label>
                <input type="text" class="form-control" name="area" placeholder="Wilayah" value="<?php echo $area; ?>">
            </div>
            <div class="form-group">
                <label for="name">Alamat</label>
                <input type="text" class="form-control" name="address" placeholder="Alamat" value="<?php echo $address; ?>">
            </div>
            <div class="form-group">
                <label for="name">Telepon</label>
                <input type="text" class="form-control" name="phone" placeholder="Telepon" value="<?php echo $phone; ?>">
            </div>
            <div class="form-group">
                <label for="name">Jabatan</label>
                <input type="text" class="form-control" name="position" placeholder="Jabatan" value="<?php echo $position; ?>">
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
        <!-- /.box-body -->

    </form>
</div>
