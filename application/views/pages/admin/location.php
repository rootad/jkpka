<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?php if ($edit) { echo "Ubah"; } else { echo "Tambah"; } ?> Lokasi</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form role="form"  action="<?php echo $this->config->base_url();?>admin/Location/<?php if ($edit) { echo "edit?edit=" . $location_id; } else { echo "add"; } ?>" method="post" enctype="multipart/form-data">
        <div class="box-body">
            <?php
            if (strlen(validation_errors()) > 0 || strlen($error_message) > 0) { ?>
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                Gagal <?php if ($edit) { echo "ubah"; } else { echo "tambah"; } ?> lokasi. <?php echo validation_errors(); ?> <?php echo $error_message; ?>
            </div>
            <?php } ?>

            <?php
            if ($result == "1") { ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                Berhasil <?php if ($edit) { echo "ubah"; } else { echo "tambah"; } ?> lokasi
            </div>
            <?php } ?>
            <div class="form-group">
                <label for="name">Nama Lokasi</label>
                <input type="text" class="form-control" name="name" placeholder="Nama Lokasi" value="<?php echo $nameLocation; ?>">
            </div>
            <div class="form-group">
                <label for="information">Informasi Lokasi</label>
                <input type="text" class="form-control" name="information" placeholder="Informasi Lokasi" value="<?php echo $information; ?>">
            </div>
            <p class="lead">Informasi tentang lokasi:</p>
            <div class="form-group">
                <label>Jenis Permukaan air</label>
                <p><input class="radio" id="" type="radio" name="surface" value="Sungai" <?php if($surface == "Sungai") { echo "checked"; } ?>>Sungai</p>
                <p><input class="radio" id="" type="radio" name="surface" value="Danau" <?php if($surface == "Danau") { echo "checked"; } ?>>Danau</p>
                <p><input class="radio" id="" type="radio" name="surface" value="Waduk" <?php if($surface == "Waduk") { echo "checked"; } ?>>Waduk</p>
                <p><input class="radio" id="otherSurfaceRadio" type="radio" name="surface" value="<?php if($surface != "Waduk" && $surface != "Sungai" && $surface != "Danau") { if (strlen($surface) > 0) { echo $surface; } } ?>" <?php if($surface != "Waduk" && $surface != "Sungai" && $surface != "Danau") { if (strlen($surface) > 0) {echo "checked"; } } ?>>Lain-Lain</p>
                <p><input type="text" class="form-control" id="otherSurface" name="otherSurface" placeholder="Isi ini Jika anda memilih Lain-Lain" value="<?php if($surface != "Waduk" && $surface != "Sungai" && $surface != "Danau") { if (strlen($surface) > 0) { echo $surface; } } ?>"></p>
            </div>
            <div class="form-group">
                <label>Koordinat</label>
                <input type="hidden" id="locationLon" name="locationLon" value="<?php echo $locationLon; ?>">
                <input type="hidden" id="locationLat" name="locationLat" value="<?php echo $locationLat; ?>">
                <p><input class="radio" id="location_radio" type="radio" name="location" value="1">Lokasi Sekarang <span id="location_info" style="font-weight: normal;"></span></p>                
                <p><input class="radio" id="map_radio" type="radio" name="location" value="2">Gunakan Peta</p>
                <div id="map" style="width: 100%; height:500px;"></div>
            </div>
            <div class="form-group">
                <label>Gambar</label>
                <input id="exampleInputFile" type="file" name="image">
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
        <!-- /.box-body -->

    </form>
</div>
