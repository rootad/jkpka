<div class="box box-primary">
    <div class="box-body">
        <?php
            if ($status_delete == "1") { ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                Berhasil hapus monitoring
            </div>
            <?php } ?>
        <table id="post_list" class="table table-bordered table-hover display" style="width:100%">
            <thead>
                <tr>
                    <th rowspan="2">Tanggal</th>
                    <th rowspan="2">Lokasi</th>
                    <th colspan="2">Hasil Pemantauan</th>
                    <th rowspan="2">Menu</th>
                </tr>
                <tr>
                    <th>Biologi</th>
                    <th>Kimia</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Tanggal</th>
                    <th>Lokasi</th>
                    <th>Biologi</th>
                    <th>Kimia</th>
                    <th>Menu</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>