<form action="<?php echo $this->config->base_url();?>admin/data/PostImage/index" method="post" enctype="multipart/form-data">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Upload Gambar</h3>
        </div>
        <div class="box-body" style="min-height: 150px;">
            <?php
            if (strlen($error) > 0 && $error != "0") { ?>
            <div class="alert alert-danger alert-dismissible">
                Gagal upload gambar. <?php echo $error; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } 
            if ($error == "0") { ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                Berhasil upload gambar
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } ?>
            <div class="form-group">
                <label for="exampleInputFile">Pilih Gambar</label>
                <input id="exampleInputFile" type="file" name="image">

                <!-- <p class="help-block">Example block-level help text here.</p> -->
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Kirim</button>
            </div>
        </div>
    </div>
</form>

<div class="box box-primary">
    <div class="box-header with-border">
            <h3 class="box-title">Daftar Gambar</h3>
        </div>
    <div class="box-body">
        <?php $temp = $this->session->flashdata('delete_image') ;
        if ($temp != null) { ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <?php echo $temp; ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php } ?>
        <table id="image_list" class="dataTables_wrapper form-inline dt-bootstrap display" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nama</th>
                    <th>Gambar</th>
                    <th>Url</th>
                    <th>Menu</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Nama</th>
                    <th>Gambar</th>
                    <th>Url</th>
                    <th>Menu</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>