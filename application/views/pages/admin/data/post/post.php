<div class="box box-primary">
    <div class="box-body">
        <?php
            if ($status_delete == "1") { ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                Berhasil hapus dari lapangan
            </div>
            <?php } ?>
        <table id="post_list" class="dataTables_wrapper form-inline dt-bootstrap display" style="width:100%">
            <thead>
                <tr>
                    <th>Judul</th>
                    <th>Tanggal</th>
                    <th>Menu</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Judul</th>
                    <th>Tanggal</th>
                    <th>Menu</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>