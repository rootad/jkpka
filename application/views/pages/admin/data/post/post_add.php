<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Tambah Dari Lapangan</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form role="form"  action="<?php echo $this->config->base_url();?>admin/data/PostData/<?php if (strlen($edit) > 0) { echo 'edit?edit=' . $edit; } else { echo "add"; }?>" method="post" enctype="multipart/form-data">
        <div class="box-body">
            <?php
            if (strlen(validation_errors()) > 0) { ?>
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?php if (strlen($edit) > 0) { echo 'Gagal ubah dari lapangan'; } else { echo 'Gagal tambah dari lapangan'; }?>. <?php echo validation_errors(); ?>
            </div>
            <?php } ?>

            <?php
            if ($result) { ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?php if (strlen($edit) > 0) { echo 'Berhasil ubah dari lapangan'; } else { echo 'Berhasil tambah dari lapangan'; }?>
            </div>
            <?php } ?>
            <div class="form-group">
                <label for="title">Judul</label>
                <input class="form-control" id="title" name="post_title" placeholder="Masukan judul" type="text" value="<?php echo $post_title; ?>">
            </div>
            <div class="form-group">
                <label for="image_cover">Gambar Cover</label>
                <input class="form-control" id="image_cover" name="image_cover" placeholder="Masukan url gambar" type="text" value="<?php echo $image_cover; ?>">
            </div>
            <textarea name="post_content" id="post_content">
                <?php echo $post_content; ?>
            </textarea>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
        <!-- /.box-body -->

    </form>
</div>

<div class="box box-primary">
    <div class="box-header with-border">
            <h3 class="box-title">Daftar Gambar</h3>
        </div>
    <div class="box-body">
        <?php $temp = $this->session->flashdata('delete_image') ;
        if ($temp != null) { ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <?php echo $temp; ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php } ?>
        <table id="image_list" class="dataTables_wrapper form-inline dt-bootstrap display" style="width:100%">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>Gambar</th>
                    <th>Url</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Nama</th>
                    <th>Gambar</th>
                    <th>Url</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>