<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Daftar 10 pemantauan terbaru</h3>
    </div>
    <div class="box-body">
        <div class="form-group">
            <label for="date">Tanggal</label>
            <input size="16" type="text" name="date" id="date" readonly class="form-control" value="">
            <input size="16" type="text" name="date" id="xx" readonly class="form-control" value="">
        </div>
        <div id="line-example"></div>
        <table id="post_list" class="table table-bordered table-hover display" style="width:100%">
            <thead>
                <tr>
                    <th rowspan="2">Tanggal</th>
                    <th rowspan="2">Lokasi</th>
                    <th colspan="2">Hasil Pemantauan</th>
                    <th rowspan="2">Menu</th>
                </tr>
                <tr>
                    <th>Biologi</th>
                    <th>Kimia</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Tanggal</th>
                    <th>Lokasi</th>
                    <th>Biologi</th>
                    <th>Kimia</th>
                    <th>Menu</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>