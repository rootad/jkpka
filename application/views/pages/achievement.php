<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div class="about_us-banner" style="background-image: url('<?php echo $this->config->base_url();?>media/images/header-siapakami.jpg')">
</div>
<div class="about_us-about_wraper">
	<div class="about_us-about_wraper-title">
		Pencapaian Kami
	</div>
	<div class="about_us-about_wraper-line"></div>
	<div class="about_us-about_wraper-content">
		<div class="container">
			<div class="row">
			<div class="col-sm-4 col-md-3 col-lg-3" style="margin-bottom: 16px;">
				<img src="<?php echo $this->config->base_url();?>media/images/achievement/dai-jombang-2-150x150.jpg" alt="" width="150" height="150">
			</div>
			<div class="col-sm-4 col-md-3 col-lg-3" style="margin-bottom: 16px;">
				<img src="<?php echo $this->config->base_url();?>media/images/achievement/Dari-Jombang-150x150.jpg" alt="" width="150" height="150">
			</div>
			<div class="col-sm-4 col-md-3 col-lg-3" style="margin-bottom: 16px;">
				<img src="<?php echo $this->config->base_url();?>media/images/achievement/Kalpataru2-150x150.jpg" alt="" width="150" height="150">
			</div>
			<div class="col-sm-4 col-md-3 col-lg-3" style="margin-bottom: 16px;">
				<img src="<?php echo $this->config->base_url();?>media/images/achievement/Kalpataru-150x150.jpg" alt="" width="150" height="150">
			</div>
			<div class="col-sm-4 col-md-3 col-lg-3" style="margin-bottom: 16px;">
				<img src="<?php echo $this->config->base_url();?>media/images/achievement/LH-jatim-150x150.jpg" alt="" width="150" height="150">
			</div>
			<div class="col-sm-4 col-md-3 col-lg-3" style="margin-bottom: 16px;">
				<img src="<?php echo $this->config->base_url();?>media/images/achievement/pengharhaan-dari-DIRUT-Tirtayasa-150x150.jpg" alt="" width="150" height="150">
			</div>
			<div class="col-sm-4 col-md-3 col-lg-3" style="margin-bottom: 16px;">
				<img src="<?php echo $this->config->base_url();?>media/images/achievement/temu-ilmiah-150x150.jpgS" alt="" width="150" height="150">
			</div>
		</div>
	</div>
</div>