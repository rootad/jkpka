<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MonitoringModel extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('MyIpAddress');
	}

	public function create_biology($id, $location, $result) {
		$data = array(
			'tgl_posting' => date('d-m-Y H:i:s'),
			'id_anggota' => $id,
			'lokasi' => $location,
			'bid_pantau' => "B",
			'hasil_pantau' => $result
		);
		
		return $this->db->insert('pantau', $data);
	}


	public function create_monitoring($memberId, $arr, $imageName, $imageUrl) {
		$data = array(
			'member_id' => $memberId,
			"location_id" => $arr["locationId"],
			"date" => $arr["date"],
			"participants" => $arr["participants"],
			"age" => $arr["age"],
			"weather" => $arr["weather"],
			"add_on_info" => $arr["addOnInfo"],
			"surrounding_environment" => $arr["surroundingEnvironment"],
			"ph" => $arr["ph"],
			"do" => $arr["do"],
			"cod" => $arr["cod"],
			"transparency" => $arr["transparency"],
			"cacing_larva" => $arr["cacing_larva"],
			"larva_mrutu_biasa" => $arr["larva_mrutu_biasa"],
			"belatung_ekor_tikus" => $arr["belatung_ekor_tikus"],
			"lintah" => $arr["lintah"],
			"kepiting_sungai" => $arr["kepiting_sungai"],
			"kerang" => $arr["kerang"],
			"siput_tanpa_pintu" => $arr["siput_tanpa_pintu"],
			"nimfa_capung_jarum_ekor_tebal" => $arr["nimfa_capung_jarum_ekor_tebal"],
			"nimfa_capung_dobson" => $arr["nimfa_capung_dobson"],
			"nimfa_capung_sialid" => $arr["nimfa_capung_sialid"],
			"nimfa_lalat_sehari_perenang" => $arr["nimfa_lalat_sehari_perenang"],
			"larva_lalat_atau_nyamuk_lainnya" => $arr["larva_lalat_atau_nyamuk_lainnya"],
			"cacing_pipih" => $arr["cacing_pipih"],
			"larva_kumbang" => $arr["larva_kumbang"],
			"kumbang_dewasa" => $arr["kumbang_dewasa"],
			"kepik_pejalan_kaki" => $arr["kepik_pejalan_kaki"],
			"anggang_anggang" => $arr["anggang_anggang"],
			"kepik_perenang_punggung" => $arr["kepik_perenang_punggung"],
			"kepik_pendayung" => $arr["kepik_pendayung"],
			"kepik_air_lainnya" => $arr["kepik_air_lainnya"],
			"siput_berpintu" => $arr["siput_berpintu"],
			"kijing" => $arr["kijing"],
			"limpet_air_tawar" => $arr["limpet_air_tawar"],
			"nimfa_capung_biasa" => $arr["nimfa_capung_biasa"],
			"nimfa_capung_jarum_lainnya" => $arr["nimfa_capung_jarum_lainnya"],
			"larva_ulat_air_tanpa_kantung" => $arr["larva_ulat_air_tanpa_kantung"],
			"larva_ulat_kantung_air_kantung_terbuat_dari_dedaunan" => $arr["larva_ulat_kantung_air_kantung_terbuat_dari_dedaunan"],
			"nimfa_lalat_sehari_insang_segi_empat" => $arr["nimfa_lalat_sehari_insang_segi_empat"],
			"udang_air_tawar_dan_udang_biasa" => $arr["udang_air_tawar_dan_udang_biasa"],
			"kepik_pinggan_bermoncong_panjang" => $arr["kepik_pinggan_bermoncong_panjang"],
			"larva_ulat_kantung_air_kantung_dari_pasir_atau_kerikil" => $arr["larva_ulat_kantung_air_kantung_dari_pasir_atau_kerikil"],
			"nimfa_lalat_sehari_pipih" => $arr["nimfa_lalat_sehari_pipih"],
			"nimfa_lalat_sehari_insang_bercabang" => $arr["nimfa_lalat_sehari_insang_bercabang"],
			"nimfa_lalat_sehari_penggali" => $arr["nimfa_lalat_sehari_penggali"],
			"nimfa_plekoptera" => $arr["nimfa_plekoptera"],
			"temprature" => $arr["temprature"],
			'created' => date('d-m-Y H:i:s'),
			'created_by' => $memberId,
			'updated' => date('d-m-Y H:i:s'),
			'updated_by' => $memberId
		);

		if (strlen($imageUrl) > 0 && strlen($imageName) > 0) {
			$data = array_merge(
				$data, 
				array(
					'image_name' => $imageName,
					'image_url' => $imageUrl
				)
			);
		}
		
		return $this->db->insert('monitoring', $data);
	}

	public function create_location($memberId, $name, $information, $surface, $lat, $lng, $imageName, $imageUrl) {
		$data = array(
			'member_id' => $memberId,
			'name' => $name,
			'information' => $information,
			'water_surface' => $surface,
			'lat' => $lat,
			'lng' => $lng,
			'created' => date('d-m-Y H:i:s'),
			'created_by' => $memberId,
			'updated' => date('d-m-Y H:i:s'),
			'updated_by' => $memberId
		);

		if (strlen($imageUrl) > 0 && strlen($imageName) > 0) {
			$data = array_merge(
				$data, 
				array(
					'image_name' => $imageName,
					'image_url' => $imageUrl
				)
			);
		}
		
		return $this->db->insert('location', $data);
	}

	public function create_school($name, $area, $address, $phone, $position) {
		$data = array(
			'nama_anggota' => $name,
			'area' => $area,
			'almt_sekolah' => $address,
			'telp_anggota' => $phone,
			'kp_anggota' => $position
		);
		
		return $this->db->insert('anggota', $data);
	}

	public function update_monitoring($id, $arr, $imageName, $imageUrl, $memberId) {
		$data = array(
			'member_id' => $memberId,
			"location_id" => $arr["locationId"],
			"date" => $arr["date"],
			"id_anggota" => $arr["nameSchool"],
			"participants" => $arr["participants"],
			"age" => $arr["age"],
			"weather" => $arr["weather"],
			"add_on_info" => $arr["addOnInfo"],
			"surrounding_environment" => $arr["surroundingEnvironment"],
			"ph" => $arr["ph"],
			"do" => $arr["do"],
			"cod" => $arr["cod"],
			"transparency" => $arr["transparency"],
			"cacing_larva" => $arr["cacing_larva"],
			"larva_mrutu_biasa" => $arr["larva_mrutu_biasa"],
			"belatung_ekor_tikus" => $arr["belatung_ekor_tikus"],
			"lintah" => $arr["lintah"],
			"kepiting_sungai" => $arr["kepiting_sungai"],
			"kerang" => $arr["kerang"],
			"siput_tanpa_pintu" => $arr["siput_tanpa_pintu"],
			"nimfa_capung_jarum_ekor_tebal" => $arr["nimfa_capung_jarum_ekor_tebal"],
			"nimfa_capung_dobson" => $arr["nimfa_capung_dobson"],
			"nimfa_capung_sialid" => $arr["nimfa_capung_sialid"],
			"nimfa_lalat_sehari_perenang" => $arr["nimfa_lalat_sehari_perenang"],
			"larva_lalat_atau_nyamuk_lainnya" => $arr["larva_lalat_atau_nyamuk_lainnya"],
			"cacing_pipih" => $arr["cacing_pipih"],
			"larva_kumbang" => $arr["larva_kumbang"],
			"kumbang_dewasa" => $arr["kumbang_dewasa"],
			"kepik_pejalan_kaki" => $arr["kepik_pejalan_kaki"],
			"anggang_anggang" => $arr["anggang_anggang"],
			"kepik_perenang_punggung" => $arr["kepik_perenang_punggung"],
			"kepik_pendayung" => $arr["kepik_pendayung"],
			"kepik_air_lainnya" => $arr["kepik_air_lainnya"],
			"siput_berpintu" => $arr["siput_berpintu"],
			"kijing" => $arr["kijing"],
			"limpet_air_tawar" => $arr["limpet_air_tawar"],
			"nimfa_capung_biasa" => $arr["nimfa_capung_biasa"],
			"nimfa_capung_jarum_lainnya" => $arr["nimfa_capung_jarum_lainnya"],
			"larva_ulat_air_tanpa_kantung" => $arr["larva_ulat_air_tanpa_kantung"],
			"larva_ulat_kantung_air_kantung_terbuat_dari_dedaunan" => $arr["larva_ulat_kantung_air_kantung_terbuat_dari_dedaunan"],
			"nimfa_lalat_sehari_insang_segi_empat" => $arr["nimfa_lalat_sehari_insang_segi_empat"],
			"udang_air_tawar_dan_udang_biasa" => $arr["udang_air_tawar_dan_udang_biasa"],
			"kepik_pinggan_bermoncong_panjang" => $arr["kepik_pinggan_bermoncong_panjang"],
			"larva_ulat_kantung_air_kantung_dari_pasir_atau_kerikil" => $arr["larva_ulat_kantung_air_kantung_dari_pasir_atau_kerikil"],
			"nimfa_lalat_sehari_pipih" => $arr["nimfa_lalat_sehari_pipih"],
			"nimfa_lalat_sehari_insang_bercabang" => $arr["nimfa_lalat_sehari_insang_bercabang"],
			"nimfa_lalat_sehari_penggali" => $arr["nimfa_lalat_sehari_penggali"],
			"nimfa_plekoptera" => $arr["nimfa_plekoptera"],
			"temprature" => $arr["temprature"],
			'updated' => date('d-m-Y H:i:s'),
			'updated_by' => $memberId
		);

		if (strlen($imageUrl) > 0 && strlen($imageName) > 0) {
			$data = array_merge(
				$data, 
				array(
					'image_name' => $imageName,
					'image_url' => $imageUrl
				)
			);
		}
		$this->db->where('monitoring_id', $id);
		return $this->db->update('monitoring', $data);
	}

	public function update_location($locationId, $name, $information, $surface, $lat, $lng, $imageName, $imageUrl, $memberId) {
		$data = array(
			'name' => $name,
			'information' => $information,
			'water_surface' => $surface,
			'lat' => $lat,
			'lng' => $lng,
			'updated' => date('d-m-Y H:i:s'),
			'updated_by' => $memberId
		);

		if (strlen($imageUrl) > 0 && strlen($imageName) > 0) {
			$data = array_merge(
				$data, 
				array(
					'image_name' => $imageName,
					'image_url' => $imageUrl
				)
			);
		}
		$this->db->where('location_id', $locationId);
		return $this->db->update('location', $data);
	}

	public function update_school($id, $name, $area, $address, $phone, $position) {
		$data = array(
			'name' => $name,
			'area' => $area,
			'address' => $address,
			'phone' => $phone,
			'position' => $position
		);

		$this->db->where('id_anggota', $locationId);
		return $this->db->update('anggota', $data);
	}

	public function get_location_by_id($id) {
		$this->db->from('location');
		$this->db->where('location_id', $id);
		$this->db->where('status', "1");
		return $this->db->get()->row();
	}

	public function get_school_by_id($id) {
		$this->db->from('anggota');
		$this->db->where('id_anggota', $id);
		$this->db->where('status', "1");
		return $this->db->get()->row();
	}

	public function get_monitoring_by_id($id) {
		$this->db->from('monitoring');
		$this->db->join('location', 'location.location_id = monitoring.location_id', 'left');
		$this->db->join('anggota', 'anggota.id_anggota = monitoring.id_anggota', 'left');
		$this->db->where('monitoring.status', "1");
		$this->db->where('monitoring.monitoring_id', $id);
		return $this->db->get()->row();
	}

	public function get_last_monitoring($member_id) {
		$this->db->from('monitoring');
		$this->db->order_by('created', 'DESC');
		$this->db->where('member_id', $member_id);
		$this->db->limit(1);
		return $this->db->get()->row();
	}

	public function delete_location($id) {
		$data = array(
			'status' => "0"
		);
		$this->db->where('location_id', $id);
		return $this->db->update('location', $data);
	}

	public function delete_monitoring($id) {
		$data = array(
			'status' => "0"
		);
		$this->db->where('monitoring_id', $id);
		return $this->db->update('monitoring', $data);
	}

	public function get($start = 0, $limit = 12) {
		$this->db->from('pantau');
		$this->db->join('anggota', 'anggota.id_anggota = pantau.id_pantau', 'left');
		$this->db->order_by('tgl_posting', 'DESC');
		$this->db->limit($limit, $start);
		
		return $this->db->get()->result();
	}

	public function get_school($start = 0, $limit = 12) {
		$this->db->from('anggota');
		$this->db->order_by('nama_anggota', 'ASC');
		// $this->db->limit($limit, $start);
		$this->db->where('status', "1");
		return $this->db->get()->result();
	}

	public function count() {
		return $this->db->count_all('pantau');
	}
	public function location_count($memberId = "") {
		if (strlen($memberId) > 0) {
			$this->db->where('member_id', $memberId);
		}
		$this->db->from('location');
		$this->db->where('status', "1");
		return $this->db->count_all_results();
	}

	public function monitoring_count($memberId = "") {
		if (strlen($memberId) > 0) {
			$this->db->where('member_id', $memberId);
		}
		$this->db->from('monitoring');
		$this->db->where('status', "1");
		return $this->db->count_all_results();
	}

	public function school_count($schoolId = "") {
		if (strlen($schoolId) > 0) {
			$this->db->where('id_anggota', $schoolId);
		}
		$this->db->from('anggota');
		$this->db->where('status', "1");
		return $this->db->count_all_results();
	}

	public function get_location_list($start = 0, $length = 10, $column, $dir, $search, $columns, $memberId = "") {
		$this->db->from('location');
		// $draw = $draw - 1 * $length;
		if ($length != 999) {
			$this->db->limit($length, $start);
		}
		if ($columns[$column]["orderable"] == "true") {
			$this->db->order_by($columns[$column]["data"], $dir);
		}
		if (strlen($search) > 0) {
			foreach ($columns as $key => $value) {
				if ($value["searchable"] == "true") {
					$this->db->or_like($value["data"], $search, "both");
				}
			}
		}
		if (strlen($memberId) > 0) {
			$this->db->where('member_id', $memberId);
		}
		$this->db->where('status', "1");
		return $this->db->get()->result();
	}

	public function get_monitoring_list($start = 0, $length = 10, $column, $dir, $search, $columns, $memberId = "") {
		$this->db->select('*, location.name AS location_name');
		$this->db->from('monitoring');
		// $draw = $draw - 1 * $length;
		if ($length != 999) {
			$this->db->limit($length, $start);
		}
		if ($columns[$column]["orderable"] == "true") {
			$this->db->order_by($columns[$column]["data"], $dir);
		}
		if (strlen($search) > 0) {
			foreach ($columns as $key => $value) {
				if ($value["searchable"] == "true") {
					$this->db->or_like($value["data"], $search, "both");
				}
			}
		}
		if (strlen($memberId) > 0) {
			$this->db->where('monitoring.member_id', $memberId);
		}
		$this->db->join('location', 'location.location_id = monitoring.location_id', 'left');
		$this->db->join('anggota', 'anggota.id_anggota = monitoring.id_anggota', 'left');
		$this->db->join('member', 'monitoring.member_id = member.member_id', 'left');
		$this->db->where('monitoring.status', "1");
		$this->db->order_by('monitoring.created', "DESC");
		return $this->db->get()->result();
	}

	public function get_school_list($start = 0, $length = 10, $column, $dir, $search, $columns, $memberId = "") {
		$this->db->from('anggota');
		// $draw = $draw - 1 * $length;
		if ($length != 999) {
			$this->db->limit($length, $start);
		}
		if ($columns[$column]["orderable"] == "true") {
			$this->db->order_by($columns[$column]["data"], $dir);
		}
		if (strlen($search) > 0) {
			foreach ($columns as $key => $value) {
				if ($value["searchable"] == "true") {
					$this->db->or_like($value["data"], $search, "both");
				}
			}
		}
		$this->db->where('status', "1");
		return $this->db->get()->result();
	}

	public function get_location_list_simple($memberId = "") {
		$this->db->from('location');
		if (strlen($memberId) > 0) {
			$this->db->where('member_id', $memberId);
		}
		$this->db->where('status', "1");
		return $this->db->get()->result();
	}

	public function get_monitoring_list_simple($memberId = "", $new = false) {
		$this->db->from('monitoring');
		if (strlen($memberId) > 0) {
			$this->db->where('member_id', $memberId);
		}
		if ($new) {
			$this->db->limit(10);
			$this->db->order_by('date', 'DESC');
		}
		$this->db->where('status', "1");
		return $this->db->get()->result();
	}

	public function biology_column() {
		return array_slice($this->db->list_fields('monitoring'), 15, 35);
	}

	public function biology_animal($monitoringId = "") {
		$this->db->select(self::biology_column());

		$this->db->from('monitoring');

		if (strlen($monitoringId) > 0) {
			$this->db->where('monitoring_id', $monitoringId);
		}
		$this->db->where('status', "1");
		$this->db->limit(1);
		// print_r($this->db->get()->result_array());
		return $this->db->get()->result_array();
	}

	public function biology_result($monitoringId = "") {
		$arr = self::biology_animal($monitoringId);
        $total = array_sum($arr[0]);

		$number = 0;
        foreach ($arr[0] as $key => $value) {
        	if ($value > 0) {
    			$number++;
        	}
        }
        $indeks = 0;
        if ($number > 0) {
        	$indeks = number_format($total / $number, 1);
        }

        $result_index = "";
        switch ($indeks){
			case 0:
				$result_index = 'Luar biasa kotor (tidak ada kehidupan sama sekali)';
				break;
			case (1.0 <= $indeks and $indeks <= 2.9):
				$result_index = 'Sangat Kotor';
				break;
			case (3.0 <= $indeks and $indeks <=4.9):
				$result_index = 'Kotor';
				break;
			case (5.0 <= $indeks and $indeks <=5.9):
				$result_index = 'Sedang (rata-rata)';
				break;
			case (6.0 <= $indeks and $indeks <=7.9):
				$result_index = 'Agak bersih sampai bersih';
				break;
			case (8.0 <= $indeks and $indeks <=10):
				$result_index = 'Sangat Bersih';
				break;
		}
		return array(
			"number" => $number,
			"total" => $total,
			"index" => $indeks,
			"indexText" => $result_index
		);

	}
}