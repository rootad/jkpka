<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function create_user($username, $name, $email, $password) {
		$data = array(
			'email'      => $email,
			'name'      => $name,
			'password'   => $this->hash_password($password),
			'created' => date('d-m-Y H:i:s'),
		);

		return $this->db->insert('users', $data);
	}

	public function create_member($fullName, $gender, $birthday, $nameSchool, $institute, $job, $email, $password) {
		$data = array(
			'name' => $fullName,
			'gender' => $gender,
			'birthday' => $birthday,
			'id_anggota' => $nameSchool,
			'institute' => $institute,
			'job' => $job,
			'email' => $email,
			'password' => password_hash($password, PASSWORD_DEFAULT),
			'created' => date('d-m-Y H:i:s'),
		);

		return $this->db->insert('member', $data);
	}

	public function member_set_verified($verified, $email) {
		$data = array(
			'verified' => $verified
		);
		$this->db->where('email', $email);
		$this->db->update('member', $data);
	}

	public function member_last_login($member_id) {
		$data = array(
			'last_login_date' => date('d-m-Y H:i:s')
		);
		$this->db->where('member_id', $member_id);
		$this->db->update('member', $data);
	}

	public function member_set_password($password, $email) {
		$data = array(
			'password' => password_hash($password, PASSWORD_DEFAULT)
		);
		$this->db->where('email', $email);
		$this->db->update('member', $data);
	}

	public function member_update_profile($fullName, $gender, $birthday, $nameSchool, $job, $email, $password) {
		$data = array(
			'name' => $fullName,
			'gender' => $gender,
			'birthday' => $birthday,
			'id_anggota' => $nameSchool,
			'job' => $job,
		);
		if (strlen($password) > 0) {
			$data = array_merge($data, array('password' => password_hash($password, PASSWORD_DEFAULT)));
		}
		$this->db->where('email', $email);
		$this->db->update('member', $data);
	}

	public function resolve_user_login($email, $password) {
		$this->db->select('password');
		$this->db->from('users');
		$this->db->where('email', $email);
		$hash = $this->db->get()->row('password');

		return password_verify($password, $hash);
	}

	public function resolve_member_login($email, $password) {
		$this->db->select('password');
		$this->db->from('member');
		$this->db->where('email', $email);
		$hash = $this->db->get()->row('password');

		return password_verify($password, $hash);
	}

	public function get_user_by_id($user_id) {
		$this->db->from('users');
		$this->db->where('id', $user_id);

		return $this->db->get()->row();
	}

	public function get_user_by_email($email) {
		$this->db->from('users');
		$this->db->where('email', $email);

		return $this->db->get()->row();
	}

	public function get_member_by_email($email) {
		$this->db->from('member');
		$this->db->where('email', $email);

		return $this->db->get()->row();
	}

	public function get_users() {
		$this->db->from('users');

		return $this->db->get()->row();
	}
}
