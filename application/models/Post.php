<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('MyIpAddress');
	}

	public function create($name, $content, $image) {
		$data = array(
			'post_title' => $name,
			'post_content' => $content,
			'post_date' => date('Y-m-d H:i:s'),
			'post_date_gmt' => date('Y-m-d H:i:s')
		);

		$this->db->insert('wp_posts', $data);

		$id = $this->db->insert_id();

		$data = array(
			'post_title' => $name,
			'post_name' => $name,
			'post_parent' => $id,
			'guid' => $image
		);

		return $this->db->insert('wp_images', $data);
	}

	public function update($name, $content, $image , $id) {
		$temp = self::get_image($id);

		if (count($temp) > 0) {
			$data = array(
				'post_title' => $name,
				'post_content' => $content,
				'post_date' => date('Y-m-d H:i:s'),
				'post_date_gmt' => date('Y-m-d H:i:s')
			);
			$this->db->where('ID', $id);
			$this->db->update('wp_posts', $data);

			$data = array(
				'post_title' => $name,
				'post_name' => $name,
				'post_parent' => $id,
				'guid' => $image
			);

			$this->db->where('post_parent', $id);
			return $this->db->update('wp_images', $data);
		} else {
			$data = array(
				'post_title' => $name,
				'post_name' => $name,
				'post_parent' => $id,
				'guid' => $image
			);
			return $this->db->insert('wp_images', $data);

			$data = array(
				'post_title' => $name,
				'post_content' => $content,
				'post_date' => date('Y-m-d H:i:s'),
				'post_date_gmt' => date('Y-m-d H:i:s')
			);

			$this->db->where('id', $id);
			return $this->db->update('wp_posts', $data);
		}
	}

	public function delete($id) {
		return $this->db->delete('wp_posts', array('ID' => $id));
	}

	public function get($start = 0, $limit = 12) {
		$this->db->from('wp_posts');
		$this->db->limit($limit, $start);
		$this->db->order_by('id', 'DESC');
		
		return $this->db->get()->result();
	}

	public function get_list($start = 0, $length = 10, $column, $dir, $search, $columns) {
		$this->db->from('wp_posts');
		// $draw = $draw - 1 * $length;
		if ($length != 999) {
			$this->db->limit($length, $start);
		}
		if ($columns[$column]["orderable"] == "true") {
			$this->db->order_by($columns[$column]["data"], $dir);
		}
		if (strlen($search) > 0) {
			foreach ($columns as $key => $value) {
				if ($value["searchable"] == "true") {
					$this->db->or_like($value["data"], $search, "both");
				}
			}
		}
		
		return $this->db->get()->result();
	}

	public function get_last() {
		$this->db->select('wp_posts.ID, wp_posts.post_date, wp_posts.post_title, wp_images.guid');
		$this->db->distinct();
		$this->db->from('wp_posts');
		$this->db->join('wp_images', 'wp_images.post_parent = wp_posts.ID', 'left');
		$this->db->where('wp_images.guid <>', "");
		$this->db->limit(5);
		$this->db->order_by('post_date', 'DESC');
		
		return $this->db->get()->result();
	}

	public function get_last_min() {
		$this->db->select('wp_posts.ID, wp_posts.post_date, wp_posts.post_title, wp_images.guid');
		$this->db->distinct();
		$this->db->from('wp_posts');
		$this->db->join('wp_images', 'wp_images.post_parent = wp_posts.ID', 'left');
		$this->db->where('wp_images.guid <>', "");
		$this->db->limit(2);
		$this->db->order_by('post_date', 'DESC');
		
		return $this->db->get()->result();
	}

	public function get_by_id($id = 0) {
		$this->db->from('wp_posts');
		$this->db->where('ID', $id);
		
		return $this->db->get()->row();
	}

	public function get_images() {
		$this->db->from('wp_posts');
		
		return $this->db->get()->result();
	}

	public function get_image($parent_id) {
		$this->db->from('wp_images');
		$this->db->where('post_parent', $parent_id);
		$this->db->limit(1);
		
		return $this->db->get()->row();
	}

	public function count() {
		return $this->db->count_all('wp_posts');
	}

	public function add_image($title, $status, $name, $url) {
		$data = array(
			'post_title' => $title,
			'post_status' => $status,
			'post_name' => $name,
			'guid' => $url
		);
		
		return $this->db->insert('wp_images', $data);
	}

	public function get_image_list($start = 0, $length = 10, $column, $dir, $search, $columns) {
		$this->db->from('wp_images');
		// $draw = $draw - 1 * $length;
		if ($length != 999) {
			$this->db->limit($length, $start);
		}
		if ($columns[$column]["orderable"] == "true") {
			$this->db->order_by($columns[$column]["data"], $dir);
		}
		if (strlen($search) > 0) {
			foreach ($columns as $key => $value) {
				if ($value["searchable"] == "true") {
					$this->db->or_like($value["data"], $search, "both");
				}
			}
		}
		
		return $this->db->get()->result();
	}

	public function get_image_list_count() {
		return $this->db->count_all('wp_images');
	}

	public function delete_image($id) {
		return $this->db->delete('wp_images', array('ID' => $id));
	}

	public function get_image_by_id($id) {
		$this->db->from('wp_images');
		$this->db->where('ID', $id);
		$this->db->limit(1);
		
		return $this->db->get()->row();
	}
}