<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('MyIpAddress');
	}

	public function request($name, $nameSchool, $email, $message) {
		$data = array(
			'name' => $name,
			'email' => $email,
			'name_school' => $nameSchool,
			'message' => $message,
			'created' => date('d-m-Y H:i:s'),
			'created_by' => $this->myipaddress->get_client_ip(),
			'updated' => date('d-m-Y H:i:s'),
			'updated_by' => $this->myipaddress->get_client_ip(),
		);
		
		return $this->db->insert('request', $data);
	}

	public function get_request($start, $end) {
		$this->db->from('request');
		$this->db->limit($start, $end);
		
		return $this->db->get();
	}
}