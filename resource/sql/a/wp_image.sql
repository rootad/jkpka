-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 25, 2018 at 05:51 PM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jkpkaco1_jkpka2013`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_images`
--

CREATE TABLE `wp_images` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_images`
--

INSERT INTO `wp_images` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(12, 1, '2013-11-06 07:00:03', '2013-11-06 07:00:03', '', 'image4OK', '', 'inherit', 'open', 'open', '', 'image4ok', '', '', '2013-11-06 07:00:03', '2013-11-06 07:00:03', '', 11, 'wp-content/uploads/2013/11/image4OK.jpg', 0, 'attachment', 'image/jpeg', 0),
(15, 1, '2013-11-06 07:01:25', '2013-11-06 07:01:25', '', 'image3OK', '', 'inherit', 'open', 'open', '', 'image3ok', '', '', '2013-11-06 07:01:25', '2013-11-06 07:01:25', '', 14, 'wp-content/uploads/2013/11/image3OK.jpg', 0, 'attachment', 'image/jpeg', 0),
(18, 1, '2013-11-06 07:04:36', '2013-11-06 07:04:36', '', 'IMG_0005', '', 'inherit', 'open', 'open', '', 'img_0005', '', '', '2013-11-06 07:04:36', '2013-11-06 07:04:36', '', 17, 'wp-content/uploads/2013/11/IMG_0005.jpg', 0, 'attachment', 'image/jpeg', 0),
(30, 1, '2013-11-06 07:23:48', '2013-11-06 07:23:48', '', 'SAM_7033', '', 'inherit', 'open', 'open', '', 'sam_7033', '', '', '2013-11-06 07:23:48', '2013-11-06 07:23:48', '', 29, 'wp-content/uploads/2013/11/SAM_7033.jpg', 0, 'attachment', 'image/jpeg', 0),
(121, 1, '2013-11-07 01:17:30', '2013-11-07 01:17:30', '', 'seminar', '', 'inherit', 'open', 'open', '', 'seminar', '', '', '2013-11-07 01:17:30', '2013-11-07 01:17:30', '', 23, 'wp-content/uploads/2013/11/seminar.jpg', 0, 'attachment', 'image/jpeg', 0),
(122, 1, '2013-11-07 01:18:21', '2013-11-07 01:18:21', '', '7', '', 'inherit', 'open', 'open', '', '7', '', '', '2013-11-07 01:18:21', '2013-11-07 01:18:21', '', 32, 'wp-content/uploads/2013/11/7.jpg', 0, 'attachment', 'image/jpeg', 0),
(123, 1, '2013-11-07 01:19:14', '2013-11-07 01:19:14', '', '6', '', 'inherit', 'open', 'open', '', '6', '', '', '2013-11-07 01:19:14', '2013-11-07 01:19:14', '', 118, 'wp-content/uploads/2013/11/6.jpg', 0, 'attachment', 'image/jpeg', 0),
(124, 1, '2013-11-07 01:21:03', '2013-11-07 01:21:03', '', 'kemah', '', 'inherit', 'open', 'open', '', 'kemah', '', '', '2013-11-07 01:21:03', '2013-11-07 01:21:03', '', 20, 'wp-content/uploads/2013/11/kemah.jpg', 0, 'attachment', 'image/jpeg', 0),
(139, 1, '2013-11-07 02:24:44', '2013-11-07 02:24:44', '', 'air minum', '', 'inherit', 'open', 'open', '', 'air-minum', '', '', '2013-11-07 02:24:44', '2013-11-07 02:24:44', '', 134, 'wp-content/uploads/2013/11/air-minum.jpg', 0, 'attachment', 'image/jpeg', 0),
(143, 1, '2013-11-07 02:56:18', '2013-11-07 02:56:18', '', 'lomba', '', 'inherit', 'open', 'open', '', 'lomba', '', '', '2013-11-07 02:56:18', '2013-11-07 02:56:18', '', 142, 'wp-content/uploads/2013/11/lomba.jpg', 0, 'attachment', 'image/jpeg', 0),
(149, 1, '2013-11-07 06:43:05', '2013-11-07 06:43:05', '', 'waduk', '', 'inherit', 'open', 'open', '', 'waduk', '', '', '2013-11-07 06:43:05', '2013-11-07 06:43:05', '', 148, 'wp-content/uploads/2013/11/waduk.jpg', 0, 'attachment', 'image/jpeg', 0),
(151, 1, '2013-11-07 06:45:16', '2013-11-07 06:45:16', '', '10', '', 'inherit', 'open', 'open', '', '10', '', '', '2013-11-07 06:45:16', '2013-11-07 06:45:16', '', 115, 'wp-content/uploads/2013/11/10.jpg', 0, 'attachment', 'image/jpeg', 0),
(169, 1, '2013-11-07 07:28:53', '2013-11-07 07:28:53', '', '1', '', 'inherit', 'open', 'open', '', '1', '', '', '2013-11-07 07:28:53', '2013-11-07 07:28:53', '', 168, 'wp-content/uploads/2013/11/1.jpg', 0, 'attachment', 'image/jpeg', 0),
(180, 1, '2013-11-07 07:42:24', '2013-11-07 07:42:24', '', 'Bioassessment di sungai sentul Ploso Jombang3', '', 'inherit', 'open', 'open', '', 'bioassessment-di-sungai-sentul-ploso-jombang3', '', '', '2013-11-07 07:42:24', '2013-11-07 07:42:24', '', 0, 'wp-content/uploads/2013/11/Bioassessment-di-sungai-sentul-Ploso-Jombang3.jpg', 0, 'attachment', 'image/jpeg', 0),
(185, 1, '2013-11-07 07:52:04', '2013-11-07 07:52:04', '', '75501_4873731820862_1648176081_n', '', 'inherit', 'open', 'open', '', '75501_4873731820862_1648176081_n', '', '', '2013-11-07 07:52:04', '2013-11-07 07:52:04', '', 184, 'wp-content/uploads/2013/11/75501_4873731820862_1648176081_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(198, 1, '2013-11-07 08:03:39', '2013-11-07 08:03:39', '', 'diskusi', '', 'inherit', 'open', 'open', '', 'diskusi', '', '', '2013-11-07 08:03:39', '2013-11-07 08:03:39', '', 0, 'wp-content/uploads/2013/11/diskusi.jpg', 0, 'attachment', 'image/jpeg', 0),
(230, 1, '2013-11-07 13:16:23', '2013-11-07 13:16:23', '', '1044990_4339757271832_1532597711_n', '', 'inherit', 'open', 'open', '', '1044990_4339757271832_1532597711_n', '', '', '2013-11-07 13:16:23', '2013-11-07 13:16:23', '', 226, 'wp-content/uploads/2013/11/1044990_4339757271832_1532597711_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(234, 1, '2013-11-07 13:26:09', '2013-11-07 13:26:09', '', '944725_4246505620599_54087626_n', '', 'inherit', 'open', 'open', '', '944725_4246505620599_54087626_n', '', '', '2013-11-07 13:26:09', '2013-11-07 13:26:09', '', 26, 'wp-content/uploads/2013/11/944725_4246505620599_54087626_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(241, 1, '2013-11-07 13:42:48', '2013-11-07 13:42:48', '', 'launcing buku', '', 'inherit', 'open', 'open', '', 'launcing-buku', '', '', '2013-11-07 13:42:48', '2013-11-07 13:42:48', '', 239, 'wp-content/uploads/2013/11/launcing-buku.jpg', 0, 'attachment', 'image/jpeg', 0),
(245, 1, '2013-11-07 13:52:47', '2013-11-07 13:52:47', '', '413_356122821163433_344945413_n', '', 'inherit', 'open', 'open', '', '413_356122821163433_344945413_n', '', '', '2013-11-07 13:52:47', '2013-11-07 13:52:47', '', 243, 'wp-content/uploads/2013/03/413_356122821163433_344945413_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(253, 1, '2013-11-07 14:03:16', '2013-11-07 14:03:16', '', '262738_3492931021705_1761280306_n', '', 'inherit', 'open', 'open', '', '262738_3492931021705_1761280306_n', '', '', '2013-11-07 14:03:16', '2013-11-07 14:03:16', '', 248, 'wp-content/uploads/2013/11/262738_3492931021705_1761280306_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(257, 1, '2013-11-07 14:12:02', '2013-11-07 14:12:02', '', 'lomba 2011 1-2', '', 'inherit', 'open', 'open', '', 'lomba-2011-1-2', '', '', '2013-11-07 14:12:02', '2013-11-07 14:12:02', '', 255, 'wp-content/uploads/2013/11/lomba-2011-1-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(274, 1, '2013-11-07 14:30:41', '2013-11-07 14:30:41', '', 'IMG_0016', '', 'inherit', 'open', 'open', '', 'img_0016', '', '', '2013-11-07 14:30:41', '2013-11-07 14:30:41', '', 260, 'wp-content/uploads/2013/11/IMG_0016.jpg', 0, 'attachment', 'image/jpeg', 0),
(301, 1, '2013-11-07 14:40:45', '2013-11-07 14:40:45', '', 'dsda', '', 'inherit', 'open', 'open', '', 'dsda', '', '', '2013-11-07 14:40:45', '2013-11-07 14:40:45', '', 300, 'wp-content/uploads/2013/11/dsda.jpg', 0, 'attachment', 'image/jpeg', 0),
(355, 1, '2014-03-24 02:29:37', '2014-03-24 02:29:37', '', 'hari air2', '', 'inherit', 'open', 'open', '', 'hari-air2', '', '', '2014-03-24 02:29:37', '2014-03-24 02:29:37', '', 354, 'wp-content/uploads/2014/03/hari-air2.jpg', 0, 'attachment', 'image/jpeg', 0),
(358, 1, '2014-03-24 02:38:30', '2014-03-24 02:38:30', '', 'banjir', '', 'inherit', 'open', 'open', '', 'banjir', '', '', '2014-03-24 02:38:30', '2014-03-24 02:38:30', '', 357, 'wp-content/uploads/2014/03/banjir.jpg', 0, 'attachment', 'image/jpeg', 0),
(361, 1, '2014-03-24 02:42:28', '2014-03-24 02:42:28', '', 'wonogiri', '', 'inherit', 'open', 'open', '', 'wonogiri', '', '', '2014-03-24 02:42:28', '2014-03-24 02:42:28', '', 360, 'wp-content/uploads/2014/03/wonogiri.jpg', 0, 'attachment', 'image/jpeg', 0),
(365, 1, '2014-03-24 02:49:03', '2014-03-24 02:49:03', '', 'longsor', '', 'inherit', 'open', 'open', '', 'longsor', '', '', '2014-03-24 02:49:03', '2014-03-24 02:49:03', '', 364, 'wp-content/uploads/2014/03/longsor.jpg', 0, 'attachment', 'image/jpeg', 0),
(369, 1, '2014-06-28 21:07:18', '2014-06-28 21:07:18', '', '0g147', '', 'inherit', 'open', 'open', '', '0g147', '', '', '2014-06-28 21:07:18', '2014-06-28 21:07:18', '', 368, 'wp-content/uploads/2014/06/0g147.jpg', 0, 'attachment', 'image/jpeg', 0),
(373, 1, '2014-06-28 21:14:08', '2014-06-28 21:14:08', '', 'sungai brantas', '', 'inherit', 'open', 'open', '', 'sungai-brantas', '', '', '2014-06-28 21:14:08', '2014-06-28 21:14:08', '', 372, 'wp-content/uploads/2014/06/sungai-brantas.jpg', 0, 'attachment', 'image/jpeg', 0),
(377, 1, '2014-10-29 02:37:33', '2014-10-29 02:37:33', '', 'worksohp 2014', '', 'inherit', 'open', 'open', '', 'worksohp-2014', '', '', '2014-10-29 02:37:33', '2014-10-29 02:37:33', '', 376, 'wp-content/uploads/2014/10/worksohp-2014.jpg', 0, 'attachment', 'image/jpeg', 0),
(380, 1, '2014-10-29 02:53:23', '2014-10-29 02:53:23', '', 'pemantauan 1', '', 'inherit', 'open', 'open', '', 'pemantauan-1', '', '', '2014-10-29 02:53:23', '2014-10-29 02:53:23', '', 379, 'wp-content/uploads/2014/10/pemantauan-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(386, 1, '2014-10-29 03:08:18', '2014-10-29 03:08:18', '', 'berita 2', '', 'inherit', 'open', 'open', '', 'berita-2', '', '', '2014-10-29 03:08:18', '2014-10-29 03:08:18', '', 385, 'wp-content/uploads/2014/10/berita-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(388, 1, '2014-10-29 03:12:47', '2014-10-29 03:12:47', '', 'kali brantas', '', 'inherit', 'open', 'open', '', 'kali-brantas', '', '', '2014-10-29 03:12:47', '2014-10-29 03:12:47', '', 382, 'wp-content/uploads/2014/10/kali-brantas.jpg', 0, 'attachment', 'image/jpeg', 0),
(390, 1, '2014-10-29 03:20:06', '2014-10-29 03:20:06', '', 'air_bersih', '', 'inherit', 'open', 'open', '', 'air_bersih', '', '', '2014-10-29 03:20:06', '2014-10-29 03:20:06', '', 389, 'wp-content/uploads/2014/10/air_bersih.jpg', 0, 'attachment', 'image/jpeg', 0),
(394, 1, '2015-10-07 22:52:31', '2015-10-07 22:52:31', '', 'sma malaang', '', 'inherit', 'open', 'closed', '', 'sma-malaang', '', '', '2015-10-07 22:52:31', '2015-10-07 22:52:31', '', 393, 'wp-content/uploads/2015/10/sma-malaang.jpg', 0, 'attachment', 'image/jpeg', 0),
(397, 1, '2015-10-07 22:56:13', '2015-10-07 22:56:13', '', 'nyebrang', '', 'inherit', 'open', 'closed', '', 'nyebrang', '', '', '2015-10-07 22:56:13', '2015-10-07 22:56:13', '', 396, 'wp-content/uploads/2015/10/nyebrang.jpg', 0, 'attachment', 'image/jpeg', 0),
(400, 1, '2015-10-07 23:00:46', '2015-10-07 23:00:46', '', 'diklat', '', 'inherit', 'open', 'closed', '', 'diklat', '', '', '2015-10-07 23:00:46', '2015-10-07 23:00:46', '', 399, 'wp-content/uploads/2015/10/diklat.jpg', 0, 'attachment', 'image/jpeg', 0),
(405, 1, '2015-10-08 00:25:38', '2015-10-08 00:25:38', '', 'lomba 2015 oke', '', 'inherit', 'open', 'closed', '', 'lomba-2015-oke', '', '', '2015-10-08 00:25:38', '2015-10-08 00:25:38', '', 403, 'wp-content/uploads/2015/10/lomba-2015-oke.jpg', 0, 'attachment', 'image/jpeg', 0),
(411, 1, '2015-10-08 00:47:16', '2015-10-08 00:47:16', '', 'jt1', '', 'inherit', 'open', 'closed', '', 'jt1', '', '', '2015-10-08 00:47:16', '2015-10-08 00:47:16', '', 408, 'wp-content/uploads/2015/10/jt1-.jpg', 0, 'attachment', 'image/jpeg', 0),
(414, 1, '2016-04-05 03:45:10', '2016-04-05 03:45:10', '', '14', '', 'inherit', 'open', 'closed', '', '14', '', '', '2016-04-05 03:45:10', '2016-04-05 03:45:10', '', 413, 'wp-content/uploads/2016/04/14.jpg', 0, 'attachment', 'image/jpeg', 0),
(417, 1, '2016-04-05 04:56:18', '2016-04-05 04:56:18', '', 'air', '', 'inherit', 'open', 'closed', '', 'air', '', '', '2016-04-05 04:56:18', '2016-04-05 04:56:18', '', 416, 'wp-content/uploads/2016/04/air.jpg', 0, 'attachment', 'image/jpeg', 0),
(421, 1, '2016-04-05 05:03:55', '2016-04-05 05:03:55', '', '2', '', 'inherit', 'open', 'closed', '', '2-2', '', '', '2016-04-05 05:03:55', '2016-04-05 05:03:55', '', 0, 'wp-content/uploads/2016/04/2.jpg', 0, 'attachment', 'image/jpeg', 0),
(427, 1, '2016-08-19 12:22:24', '2016-08-19 12:22:24', '', '13909221_10204954806469605_4751784922724320323_o', '', 'inherit', 'open', 'closed', '', '13909221_10204954806469605_4751784922724320323_o', '', '', '2016-08-19 12:22:24', '2016-08-19 12:22:24', '', 426, 'wp-content/uploads/2016/08/13909221_10204954806469605_4751784922724320323_o.jpg', 0, 'attachment', 'image/jpeg', 0),
(437, 1, '2016-08-19 12:32:38', '2016-08-19 12:32:38', '', '13909444_10204971850215688_3454593280559788152_o', '', 'inherit', 'open', 'closed', '', '13909444_10204971850215688_3454593280559788152_o', '', '', '2016-08-19 12:32:38', '2016-08-19 12:32:38', '', 436, 'wp-content/uploads/2016/08/13909444_10204971850215688_3454593280559788152_o.jpg', 0, 'attachment', 'image/jpeg', 0),
(444, 1, '2016-09-01 12:06:32', '2016-09-01 12:06:32', '', '14224801_10205081611439650_1786508112173288053_n', '', 'inherit', 'open', 'closed', '', '14224801_10205081611439650_1786508112173288053_n', '', '', '2016-09-01 12:06:32', '2016-09-01 12:06:32', '', 443, 'wp-content/uploads/2016/09/14224801_10205081611439650_1786508112173288053_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(449, 1, '2016-09-01 12:14:41', '2016-09-01 12:14:41', '', '14021577_10205029432415207_4071832354263892813_n', '', 'inherit', 'open', 'closed', '', '14021577_10205029432415207_4071832354263892813_n-2', '', '', '2016-09-01 12:14:41', '2016-09-01 12:14:41', '', 446, 'wp-content/uploads/2016/09/14021577_10205029432415207_4071832354263892813_n-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(454, 1, '2016-12-01 11:04:49', '2016-12-01 11:04:49', '', 'dscf4580', '', 'inherit', 'open', 'closed', '', 'dscf4580', '', '', '2016-12-01 11:04:49', '2016-12-01 11:04:49', '', 453, 'wp-content/uploads/2016/12/DSCF4580.jpg', 0, 'attachment', 'image/jpeg', 0),
(461, 1, '2017-04-28 00:22:47', '2017-04-28 00:22:47', '', 'muri 1', '', 'inherit', 'open', 'closed', '', 'muri-1', '', '', '2017-04-28 00:22:47', '2017-04-28 00:22:47', '', 457, 'wp-content/uploads/2017/04/muri-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(471, 1, '2017-07-25 14:07:13', '2017-07-25 14:07:13', '', 'Screenshot_5', '', 'inherit', 'open', 'closed', '', 'screenshot_5', '', '', '2017-07-25 14:07:13', '2017-07-25 14:07:13', '', 470, 'wp-content/uploads/2017/07/Screenshot_5.png', 0, 'attachment', 'image/png', 0),
(483, 1, '2017-07-26 06:44:50', '2017-07-26 06:44:50', '', 'Untitled-1--R4--DUTA SUNGAI w', '', 'inherit', 'open', 'closed', '', 'untitled-1-r4-duta-sungai-w', '', '', '2017-07-26 06:44:50', '2017-07-26 06:44:50', '', 479, 'wp-content/uploads/2017/07/Untitled-1-R4-DUTA-SUNGAI-w.jpg', 0, 'attachment', 'image/jpeg', 0),
(493, 1, '2017-08-27 01:46:41', '2017-08-27 01:46:41', '', 'IMG-20170827-WA0002', '', 'inherit', 'open', 'closed', '', 'img-20170827-wa0002', '', '', '2017-08-27 01:46:41', '2017-08-27 01:46:41', '', 492, 'wp-content/uploads/2017/08/IMG-20170827-WA0002.jpg', 0, 'attachment', 'image/jpeg', 0),
(500, 1, '2017-09-20 07:27:36', '2017-09-20 07:27:36', '', '21105889_10207367452104238_8481858566494294175_n', '', 'inherit', 'open', 'closed', '', '21105889_10207367452104238_8481858566494294175_n', '', '', '2017-09-20 07:27:36', '2017-09-20 07:27:36', '', 497, 'wp-content/uploads/2017/09/21105889_10207367452104238_8481858566494294175_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(502, 1, '2017-09-20 07:30:15', '2017-09-20 07:30:15', '', '21192671_1534829429871193_5258996612103682629_n', '', 'inherit', 'open', 'closed', '', '21192671_1534829429871193_5258996612103682629_n', '', '', '2017-09-20 07:30:15', '2017-09-20 07:30:15', '', 501, 'wp-content/uploads/2017/09/21192671_1534829429871193_5258996612103682629_n.jpg', 0, 'attachment', 'image/jpeg', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_images`
--
ALTER TABLE `wp_images`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`),
  ADD KEY `post_name` (`post_name`(191));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_images`
--
ALTER TABLE `wp_images`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=512;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
