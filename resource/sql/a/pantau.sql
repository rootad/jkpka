-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 26, 2018 at 08:33 AM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jkpkaco1_pantau`
--

-- --------------------------------------------------------

--
-- Table structure for table `anggota`
--

CREATE TABLE `anggota` (
  `id_anggota` int(10) UNSIGNED NOT NULL,
  `nama_anggota` char(60) DEFAULT NULL,
  `wilayah` char(6) DEFAULT NULL,
  `almt_sekolah` char(100) DEFAULT NULL,
  `telp_anggota` char(20) DEFAULT NULL,
  `kp_anggota` varchar(45) DEFAULT NULL,
  `pict_anggota` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anggota`
--

INSERT INTO `anggota` (`id_anggota`, `nama_anggota`, `wilayah`, `almt_sekolah`, `telp_anggota`, `kp_anggota`, `pict_anggota`) VALUES
(50, ' SMAN 1 Malang', 'Hulu', '', '', '', ''),
(51, ' SMAN 2 Malang', 'Hulu', '', '', '', ''),
(52, ' SMAN 3 Malang', 'Hulu', '', '', '', ''),
(53, ' SMAN 4 Malang', 'Hulu', '', '', '', ''),
(54, ' SMAN 5 Malang', 'Hulu', '', '', '', ''),
(55, ' SMAN 6 Malang', 'Hulu', '', '', '', ''),
(56, ' SMAN 6 Malang', 'Hulu', '', '', '', ''),
(57, ' SMAN 7 Malang', 'Hulu', '', '', '', ''),
(58, ' SMAN 8 Malang', 'Hulu', '', '', '', ''),
(59, ' SMAN 9 Malang', 'Hulu', '', '', '', ''),
(60, ' SMAN 10 Malang', 'Hulu', '', '', '', ''),
(61, ' SMAN 11 Malang', 'Hulu', '', '', '', ''),
(62, ' SMAN 12 Malang', 'Hulu', '', '', '', ''),
(63, ' MAN 3 Malang', 'Hulu', '', '', '', ''),
(64, ' SMA Wahid Hasyim Malang', 'Hulu', '', '', '', ''),
(65, ' SMA Dempo Malang', 'Hulu', '', '', '', ''),
(66, ' SMA Salahudin Malang', 'Hulu', '', '', '', ''),
(67, ' SMA Muhammadiyah 1 Malang', 'Hulu', '', '', '', ''),
(68, ' SMA Arjuna Malang', 'Hulu', '', '', '', ''),
(69, ' SMA Lab UM Malang', 'Hulu', '', '', '', ''),
(70, ' SMA Islam Malang', 'Hulu', '', '', '', ''),
(71, ' SMA St.Maria Malang', 'Hulu', '', '', '', ''),
(72, ' SMA Frateran Malang', 'Hulu', '', '', '', ''),
(73, ' SMA Islam Al Ma arif Singasari Malang', 'Hulu', '', '', '', ''),
(74, ' SMAN Kepanjen Malang', 'Hulu', '', '', '', ''),
(75, ' MAN 1 MALANG', 'Hulu', '', '', '', ''),
(76, ' MAN 2 Batu', 'Hulu', '', '', '', ''),
(77, ' SMAN 1 Batu', 'Hulu', '', '', '', ''),
(78, ' SMAN 2 Batu', 'Hulu', '', '', '', ''),
(79, ' SMAN 1 Kediri', 'Tengah', '', '', '', ''),
(80, ' SMAN 2 Kediri', 'Tengah', '', '', '', ''),
(81, ' SMAN 3 Kediri', 'Tengah', '', '', '', ''),
(82, ' SMAN 4 Kediri', 'Tengah', '', '', '', ''),
(83, ' SMAN 5 Kediri', 'Tengah', '', '', '', ''),
(84, ' SMAN 6 Kediri', 'Tengah', '', '', '', ''),
(85, ' SMAN 7 Kediri', 'Tengah', '', '', '', ''),
(86, ' SMAN 8 Kediri', 'Tengah', '', '', '', ''),
(87, ' SMAN 2 Blitar', 'Tengah', '', '', '', ''),
(88, ' SMAN 1 Srengat Blitar', 'Tengah', '', '', '', ''),
(89, ' SMAN 1 Garum Blitar', 'Tengah', '', '', '', ''),
(90, ' SMAN 1 Sutojayan Blitar', 'Tengah', '', '', '', ''),
(91, ' SMAN 1 Kademangan Blitar', 'Tengah', '', '', '', ''),
(92, ' SMAN 1 Gondang Tulungagung', 'Tengah', '', '', '', ''),
(93, ' SMAN 1 Ngunut Tulungagung', 'Tengah', '', '', '', ''),
(94, ' SMAN 1 Tulungagung', 'Tengah', '', '', '', ''),
(95, ' SMAN Tanjung Anom Nganjuk', 'Tengah', '', '', '', ''),
(96, ' SMAN Gondang Nganjuk', 'Tengah', '', '', '', ''),
(97, ' SMAN 1 Nganjuk', 'Tengah', '', '', '', ''),
(98, ' SMAN 2 Nganjuk', 'Tengah', '', '', '', ''),
(99, ' SMAN 3 Nganjuk', 'Tengah', '', '', '', ''),
(100, ' SMAN 1 Kertosono', 'Tengah', '', '', '', ''),
(101, ' SMAN 1 Grogol Kediri', 'Tengah', '', '', '', ''),
(102, ' SMAN 2 Pare Kediri', 'Tengah', '', '', '', ''),
(103, ' SMAN 1 Papar Kediri', 'Tengah', '', '', '', ''),
(104, ' SMAN 2 Jombang', 'Hilir', 'Jl. Dr. W. Sudirohusodo No.1 Jombang', '0321861777', 'Drs. Soetarno', '../foto_skul/foto1.gif'),
(105, ' SMAN 3 Jombang', 'Hilir', '', '', '', ''),
(106, ' SMAN 1 Bandar Kedung Mulya', 'Hilir', '', '', '', ''),
(107, ' SMAN Ploso Jombang', 'Hilir', '', '', '', ''),
(108, ' SMAN Kesamben Jombang', 'Hilir', '', '', '', ''),
(109, ' SMA DarulUlum 2 Unggulan BPPT Rejoso Jombang', 'Hilir', '', '', '', ''),
(110, ' SMA Muhammadiyah 1 Jombang', 'Hilir', '', '', '', ''),
(111, ' SMA A Wahid Hasyim Jombang', 'Hilir', '', '', '', ''),
(112, ' SMA PGRI 1 Jombang', 'Hilir', '', '', '', ''),
(113, ' MAN Tambak Beras Jombang', 'Hilir', '', '', '', ''),
(114, ' SMAN 1 Gedeg Mojokerto', 'Hilir', '', '', '', ''),
(115, ' SMAN 1 Sooko Mojokerto', 'Hilir', '', '', '', ''),
(116, ' SMAN 1 Kota Mojokerto', 'Hilir', '', '', '', ''),
(117, ' SMAN 1 Taman Sidoarjo', 'Hilir', '', '', '', ''),
(118, ' SMAN 1 Pasuruan', 'Hilir', '', '', '', ''),
(119, ' SMAN 5 Surabaya', 'Hilir', '', '', '', ''),
(120, ' SMA Kristen Petra 3 Surabaya', 'Hilir', '', '', '', ''),
(144, 'SMAN 21 Surabaya', 'Hilir', NULL, NULL, NULL, NULL),
(143, 'SMAN 4 Sidoarjo', 'Hilir', NULL, NULL, NULL, NULL),
(126, 'SMAN 1 Boyolangu Tulungagung', 'Tengah', NULL, NULL, NULL, NULL),
(127, 'SMAN 1 Kedungwaru Tulungagung', 'Tengah', NULL, NULL, NULL, NULL),
(142, 'SMAN 3 Sidoarjo', 'Hilir', NULL, NULL, NULL, NULL),
(141, 'SMAN 1 Taman Sidoarjo', 'Hilir', NULL, NULL, NULL, NULL),
(140, 'SMAN 1 Driyorejo Gresik', 'Hilir', NULL, NULL, NULL, NULL),
(134, 'SMAN 1 Purwoasri Kediri', 'Tengah', NULL, NULL, NULL, NULL),
(139, 'SMAN 1 Wringinanom Gresik', 'Hilir', NULL, NULL, NULL, NULL),
(138, 'SMAN Plandaan Jombang', 'Hilir', NULL, NULL, NULL, NULL),
(145, 'SMA Kristen Petra 1 Surabaya', 'Hilir', NULL, NULL, NULL, NULL),
(146, 'SMAN 1 Dagangan Madiun', 'BS', NULL, NULL, NULL, NULL),
(147, 'SMAN 1 Dolopo Madiun', 'BS', NULL, NULL, NULL, NULL),
(148, 'SMAN 1 Saradan Madiun', 'BS', NULL, NULL, NULL, NULL),
(149, 'SMAN 1 Nglames Madiun', 'BS', NULL, NULL, NULL, NULL),
(150, 'SMAN 1 Mejayan Madiun', 'BS', NULL, NULL, NULL, NULL),
(151, 'SMAN 2 Mejayan Madiun', 'BS', NULL, NULL, NULL, NULL),
(152, 'SMAN 1 Pilangkenceng Madiun', 'BS', NULL, NULL, NULL, NULL),
(153, 'SMAN 1 Geger Madiun', 'BS', NULL, NULL, NULL, NULL),
(154, 'SMAN 1 Tanimbar Selatan Maluku Tenggara Barat', 'L', NULL, NULL, NULL, NULL),
(155, 'MAN 2 Pontianak Kalimantan Barat', 'L', NULL, NULL, NULL, NULL),
(156, 'MAN Pagaralam Sumatera Selatan', 'L', NULL, NULL, NULL, NULL),
(157, 'MA D.I. Putra Kediri Lobar NTB', 'L', NULL, NULL, NULL, NULL),
(158, 'MAN Polewali Mandar Sulawesi Barat', 'L', NULL, NULL, NULL, NULL),
(159, 'SMPN 16 Surabaya', 'AD', NULL, NULL, NULL, NULL),
(160, 'SMPN 5 Surabaya', 'AD', NULL, NULL, NULL, NULL),
(161, 'SMPN 21 Surabaya', 'AD', NULL, NULL, NULL, NULL),
(162, 'SMPN 39 Surabaya', 'AD', NULL, NULL, NULL, NULL),
(163, 'SMAN 11 Surabaya', 'AD', NULL, NULL, NULL, NULL),
(164, 'SMKN 1 Surabaya', 'AD', NULL, NULL, NULL, NULL),
(165, 'SMA Trimurti Surabaya', 'AD', NULL, NULL, NULL, NULL),
(166, 'SMAN 4 Surabaya', 'AD', NULL, NULL, NULL, NULL),
(167, 'SMAN Gondang Mojokerto', 'AD', NULL, NULL, NULL, NULL),
(168, 'SMAN 1 Mojosari Kab. Mojokerto', 'AD', NULL, NULL, NULL, NULL),
(169, 'SMKN 1 Pungging Kab. Mojokerto', 'AD', NULL, NULL, NULL, NULL),
(170, 'SMAN 1 Puri Kab. Mojokerto', 'AD', NULL, NULL, NULL, NULL),
(171, 'SMAN 2 Kota Probolinggo', 'AD', NULL, NULL, NULL, NULL),
(172, 'SMKN 1 Kota Probolinggo', 'AD', NULL, NULL, NULL, NULL),
(173, 'SMKN 3 Kota Probolinggo', 'AD', NULL, NULL, NULL, NULL),
(174, 'SMAN 4 Kota Probolinggo', 'AD', NULL, NULL, NULL, NULL),
(179, 'SMKN 6 Malang', 'AD', NULL, NULL, NULL, NULL),
(184, 'SMPN 1 Jombang', 'Hilir', NULL, NULL, NULL, NULL),
(189, 'SMPN 3 Jombang', 'Hilir', NULL, NULL, NULL, NULL),
(194, 'SMPN 4 Jombang', 'Hilir', NULL, NULL, NULL, NULL),
(199, 'SMPN 5 Jombang', 'Hilir', NULL, NULL, NULL, NULL),
(204, 'SMPN 1 Tembelang', 'Hilir', NULL, NULL, NULL, NULL),
(209, 'SMPN 1 Diwek', 'Hilir', NULL, NULL, NULL, NULL),
(214, 'MAN 1 Jombang', 'Hilir', NULL, NULL, NULL, NULL),
(219, 'SMKN 3 Jombang', 'Hilir', NULL, NULL, NULL, NULL),
(220, 'SMPN 1 Pilang Kenceng', 'AD', NULL, NULL, NULL, NULL),
(221, 'SMPN 2 Pilang Kenceng', 'AD', NULL, NULL, NULL, NULL),
(222, 'SMAN 1 Pilang Kenceng', 'AD', NULL, NULL, NULL, NULL),
(223, 'MAN Dalopo', 'AD', NULL, NULL, NULL, NULL),
(224, 'MAN Kembang Sawit', 'AD', NULL, NULL, NULL, NULL),
(225, 'MAN Rejosari', 'AD', NULL, NULL, NULL, NULL),
(226, 'MA Darul Falah', 'AD', NULL, NULL, NULL, NULL),
(227, 'SMAN 1 Nglames', 'AD', NULL, NULL, NULL, NULL),
(228, 'SMAN 1 Wungu', 'AD', NULL, NULL, NULL, NULL),
(229, 'SMAN 1 Jiwan', 'AD', NULL, NULL, NULL, NULL),
(230, 'SMAN 1 Dalopo', 'AD', NULL, NULL, NULL, NULL),
(231, 'SMAN 1 Dagangan', 'AD', NULL, NULL, NULL, NULL),
(232, 'SMAN 1 Kebonsari', 'AD', NULL, NULL, NULL, NULL),
(233, 'SMKN 2 Jiwan', 'AD', NULL, NULL, NULL, NULL),
(234, 'SMKN Kare', 'AD', NULL, NULL, NULL, NULL),
(235, 'SMKN 1 Geger', 'AD', NULL, NULL, NULL, NULL),
(236, 'SMK Al Hikmah Geger', 'AD', NULL, NULL, NULL, NULL),
(237, 'SMK Wisma Wisnu', 'AD', NULL, NULL, NULL, NULL),
(238, 'nama_anggota', 'wilaya', 'almt_sekolah', 'telp_anggota', 'kp_anggota', 'pict_anggota'),
(239, 'SMP Negeri 1 Wonosalam', 'hulu', '', '', 'Koordinator Pak. Kukuh', ''),
(240, 'SMP Sapta Jarak Wonosalam', 'hulu', '', '', '', ''),
(241, 'SMK Negeri Wonosalam', 'hulu', '', '', '', ''),
(242, 'SMP Negeri 1 Bareng', 'hulu', '', '', '', ''),
(243, 'SMP Negeri 2 Bareng', 'hulu', '', '', '', ''),
(244, 'SMA Negeri Bareng', 'hulu', '', '', '', ''),
(245, 'SMP Negeri 1 Ngoro', 'hulu', '', '', '', ''),
(246, 'SMP Negeri 2 Ngoro', 'hulu', '', '', '', ''),
(247, 'SMA Negeri Ngoro', 'hulu', '', '', '', ''),
(248, 'MA Faster Wonosalam', 'hulu', '', '', '', ''),
(249, 'SMPN 2 Wonosalam', 'hulu', '', '', '', ''),
(250, 'MTsN Panglungan Wonosalam', 'hulu', '', '', '', ''),
(251, 'nama_anggota', 'wilaya', 'almt_sekolah', 'telp_anggota', 'kp_anggota', 'pict_anggota'),
(252, 'SMP Negeri 1 Mojoagung', 'Tengah', '', '', '', ''),
(253, 'SMP Negeri 2 Mojoagung', 'Tengah', '', '', '', ''),
(254, 'SMP Negeri 3 Mojoagung', 'Tengah', '', '', '', ''),
(255, 'SMA Negeri 1 Mojoagung', 'Tengah', '', '', '', ''),
(256, 'SMP Negeri 1  Peterongan', 'Tengah', '', '', '', ''),
(257, 'SMP Negeri 2  Peterongan', 'Tengah', '', '', '', ''),
(258, 'SMP Negeri 3 Peterongan', 'Tengah', '', '', '', ''),
(259, 'SMP Negeri 1 Jogoroto', 'Tengah', '', '', '', ''),
(260, 'SMP Negeri 2 Jogoroto', 'Tengah', '', '', '', ''),
(261, 'SMP Negeri 1 Mojowarno', 'Tengah', '', '', '', ''),
(262, 'SMP Negeri 2 Mojowarno', 'Tengah', '', '', '', ''),
(263, 'SMP Negeri 1 Diwek', 'Tengah', '', '', 'koordinator Pokja  Tengah  SMP  Ibu. Widiastu', ''),
(264, 'SMP Negeri 2 Diwek', 'Tengah', '', '', '', ''),
(265, 'SMA Negeri Jogoroto', 'Tengah', '', '', '', ''),
(266, 'SMP Negeri 1 Gudo', 'Tengah', '', '', '', ''),
(267, 'SMP Negeri 2 Gudo', 'Tengah', '', '', '', ''),
(268, 'SMP Negeri 1 Perak', 'Tengah', '', '', '', ''),
(269, 'SMA Negeri Bandar Kedung Mulyo', 'Tengah', '', '', '', ''),
(270, 'SMP Negeri 1 Jombang', 'Tengah', '', '', '', ''),
(271, 'SMP Negeri 2 Jombang', 'Tengah', '', '', '', ''),
(272, 'SMP Negeri 3 Jombang', 'Tengah', '', '', '', ''),
(273, 'SMA Negeri 1 Jombang', 'Tengah', '', '', '', ''),
(274, 'SMA Negeri 2 Jombang', 'Tengah', '', '', '', ''),
(275, 'SMA Negeri 3 Jombang', 'Tengah', '', '', '', ''),
(276, 'SMP Negeri  2 Megaluh', 'Hilir', '', '', '', ''),
(277, 'SMP Negeri 1 Tembelang', 'Hilir', '', '', '', ''),
(278, 'SMP Negeri 1 Ploso', 'Hilir', '', '', '', ''),
(279, 'SMA Negeri  Ploso', 'Hilir', '', '', '', ''),
(280, 'SMP Negeri 2 Kabuh', 'Hilir', '', '', '', ''),
(281, 'SMA Negeri 1 Kabuh', 'Hilir', '', '', '', ''),
(282, 'SMK Negeri  Kabuh', 'Hilir', '', '', '', ''),
(283, 'SMP Negeri 1 Kudu', 'Hilir', '', '', '', ''),
(284, 'SMK Negeri Kudu', 'Hilir', '', '', 'Koordinator Pokja Hilir Pak Sapto Tuhu   Fatk', ''),
(285, 'SMP Negeri 1 Kesamben', 'Hilir', '', '', '', ''),
(286, 'SMA Negeri Kesamben', 'Hilir', '', '', '', ''),
(287, 'SMK Global Sumobito', 'Hilir', '', '', '', ''),
(288, 'SMPN 1 Sumobito', 'Hilir', '', '', '', ''),
(289, 'SMPN 2 Sumobito', 'Hilir', '', '', '', ''),
(290, 'SDN Kepanjen 2 Jombang', 'SD', '', '', 'Koordinator pokja SD ', ''),
(291, 'SDN Ceweng Jombang', 'SD', '', '', '', ''),
(292, 'SDN Sentul Jombang', 'SD', '', '', '', ''),
(293, 'SDIT Al Ummah', 'SD', '', '', '', ''),
(294, 'SD Plus Darul ulum', 'SD', '', '', '', ''),
(295, 'SDK Wijana', 'SD', '', '', '', ''),
(296, 'SMKN 2 Boyolangu', 'Hulu', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `kodeberita` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `penulis` varchar(10) DEFAULT NULL,
  `judul` varchar(80) DEFAULT NULL,
  `fullnews` text,
  `pict_news` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`kodeberita`, `tanggal`, `penulis`, `judul`, `fullnews`, `pict_news`) VALUES
(16, '2007-07-19 06:34:49', 'ADMIN', 'Lomba Pelestarian Sumber Daya Air DAS Kali Brantas Tahun 2007', 'Dalam rangka meningkatkan kepedulian terhadap pelestarian lingkungan di tingkatan siswa Sekolah Menengah Atas, Koordinator Pusat JKPKA (Jaring-jaring Komunikasi Pemantauan Kualitas Air) mengadakan Lomba Pelestarian Sumber Daya Air DAS Kali Brantas.\r\n\r\n<strong>1. Lomba Karya Ilmiah</strong>\r\nTema: Pelestarian Sumber Daya Air\r\nBahan: Kertas A4/Kuarto 70gr (dapat diketik manual atau komputer)\r\nPanjang: min. 10 halaman\r\n\r\n<strong>2. Lomba Laporan Hasil Pemantauan Kualitas Air</strong>\r\nTema: Kualitas Air di DAS Kali Brantas\r\nBahan: Kertas A4/Kuarto 70gr (dapat diketik manual atau komputer)\r\nPanjang: tidak terbatas\r\nBidang: Kimia/Biologi\r\n\r\n<strong>3. Lomba Poster</strong>\r\nTema: permasalahan Sumber Daya Air\r\nBahan: Kertas Manila (40cm x 30cm) memakai cat air/cat poster/pastel/pensil warna\r\nTeknik: bebas\r\n\r\n<strong>4. Lomba Photo</strong>\r\nTema: pelestarian Sumber Daya Air (kegiatan penelitian kualitas air, kondisi pencemaran dan pelestarian)\r\nBahan: photo ukuran 10R\r\nTeknik: bebas\r\n\r\n<strong>5. Lomba Essai</strong>\r\nTema: pelestarian Sumber Daya Air (menyangkut issue-issue, budaya, kearifan lokal)\r\nBahan: Kertas A4/Kuarto 70gr (dapat diketik manual atau komputer)\r\nPanjang: maksimal 2 halaman\r\n<ol>\r\n<strong>Ketentuan Umum:</strong>\r\n<li class=\"unblock\">Peserta adalah murid SMU anggota JKPKA.</li>\r\n<li class=\"unblock\">Setiap SMU minimal mengikutsertakan 1 (satu) orang atau 1 (satu) kelompok untuk setiap mata lomba.</li>\r\n<li class=\"unblock\">Khusus untuk Lomba Karya Ilmiah dan Lomba Laporan Hasil Pemantauan Kualitas Air peserta berbentuk kelompok dengan anggota maksimal 3 orang.</li>\r\n</ol>\r\n\r\n<h3>Materi lomba harap dikirim langsung atau via pos ke:\r\nBIRO MANAJEMEN MUTU\r\nD/A. PERUM JASA TIRTA I\r\nJLN. SURABAYA No.24 - MALANG\r\n\r\nCONTACT PERSON:\r\n-MURTOMO (081334846159)</h3>\r\n\r\n<h3 style=\"color:red\">Materi lomba paling lambat diterima (cap pos) 31 Agustus 2007</h3>', ''),
(17, '2009-10-10 13:52:09', 'ADMIN', 'Lomba Pelestarian Sumber Daya Air 2009', 'Dalam rangka meningkatkan kepedulian terhadap pelestarian lingkungan di tingkat siswa sekolah menengah atas, koordinator pusat jaring-jaring komunikasi pemantauan kualitas air (jkpka) mengadakan lomba bertema pelestarian sumber daya air. Total hadiah Rp. 10juta. Lomba yang diadakan meliputi:<br><ol><li>Lomba Karya Tulis</li><li>Lomba Pemantauan Kualitas air</li><li>Lomba Lukis Kaos Lingkungan</li><li>Lomba Kreasi Lingkungan.</li></ol><br>Materi lomba paling lambat diterima (cap pos) 30 Oktober 2009.<br><br>Informasi selengkapnya, lihat pada gambar di bawah ini.<br><br><a href=\"foto_berita/LOMBA_JKPKA_2009_b.jpg\" target=\"_blank\"><img src=\"foto_berita/LOMBA_JKPKA_2009_s.jpg\" title=\"klik untuk memperbesar\"></a><br>', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pantau`
--

CREATE TABLE `pantau` (
  `id_pantau` int(10) UNSIGNED NOT NULL,
  `tgl_posting` datetime NOT NULL,
  `id_anggota` int(10) UNSIGNED NOT NULL,
  `lokasi` varchar(100) NOT NULL,
  `bid_pantau` enum('B','K') NOT NULL,
  `hasil_pantau` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `pantau`
--

INSERT INTO `pantau` (`id_pantau`, `tgl_posting`, `id_anggota`, `lokasi`, `bid_pantau`, `hasil_pantau`) VALUES
(1, '2009-07-01 09:50:56', 104, 'Kali Sengon', 'B', 'Kotor<<18<<06<<2009<<4 sore<<47<<11<<4.3'),
(2, '2009-08-07 09:32:01', 104, 'ali jombang kulon', 'B', 'Agak bersih sampai bersih<<07<<08<<2009<<08.00 sd 09.30<<42<<7<<6.0'),
(3, '2009-09-27 18:43:11', 104, 'Kali Sengon', 'B', 'Kotor<<10<<7<<2009<<12 siang<<71<<15<<4.7'),
(4, '2009-10-05 09:54:06', 117, 'Kali Mas Tawangsari', 'B', 'Sedang (rata-rata)<<05<<10<<2009<<07.00WIB-08.00WIB<<100<<19<<5.3'),
(5, '2009-10-06 12:14:50', 51, 'Sungai Brantas Sisosadar', 'B', 'Kotor<<06<<10<<2009<<08.00<<25<<6<<4.2'),
(6, '2009-10-18 11:59:09', 104, 'kali tugu', 'B', 'Kotor<<18<<10<<2009<<12.00<<24<<6<<4.0'),
(7, '2009-10-18 12:01:57', 105, 'sungai brantas', 'B', 'Sedang (rata-rata)<<18<<10<<2009<<00.00<<197<<35<<5.6'),
(8, '2009-10-19 08:59:55', 141, 'Kali Mas Tawangsari', 'B', 'Sedang (rata-rata)<<17<<10<<2009<<11.30<<22<<4<<5.5'),
(9, '2009-10-19 09:06:12', 141, 'Kali Mas Tawangsari', 'B', 'Kotor<<17<<10<<2009<<12.30<<14<<3<<4.7'),
(10, '2009-10-19 09:10:12', 141, 'Kali Mas Tawangsari', 'B', 'Kotor<<17<<10<<2009<<12<<6<<2<<3.0'),
(11, '2009-10-19 09:13:38', 117, 'Kali Mas Tawangsari', 'B', 'Sedang (rata-rata)<<17<<10<<2009<<12.15<<5<<1<<5.0'),
(12, '2009-10-19 09:24:47', 117, 'kali pereng, bebekan', 'B', 'Agak bersih sampai bersih<<19<<10<<2009<<06.50<<19<<3<<6.3'),
(13, '2009-10-19 09:29:40', 117, 'kali pereng, bebekan', 'B', 'Sangat Kotor<<18<<10<<2009<<07.00<<1<<1<<1.0'),
(14, '2009-10-20 16:22:41', 139, 'wriniginanom', 'B', 'Kotor<<3<<10<<2009<<12.00 wib<<30<<7<<4.3'),
(15, '2009-10-21 12:36:05', 141, 'Kali Mas, Tawangsari', 'B', 'Sedang (rata-rata)<<17<<10<<2009<<11.40 - 12.30<<25<<5<<5.0'),
(16, '2009-10-22 13:16:54', 107, 'Sungai  Kedung Rejo', 'B', 'Kotor<<11<<10<<2009<<09.30 WIB<<65<<14<<4.6'),
(17, '2009-10-22 17:25:33', 139, 'Kali Desa Sumengko (Hutan Tani Bantaran)', 'B', 'Sedang (rata-rata)<<3<<10<<2009<<12.10 wib <<57<<11<<5.2'),
(18, '2009-10-22 17:35:08', 139, 'Kali Desa Sumengko (lokasi pembuangan limbah pabrik kertas PT. Adiprima Suraprinta)', 'B', 'Kotor<<3<<10<<2009<<12.10 wib<<25<<6<<4.2'),
(19, '2009-10-22 17:38:17', 139, 'Kali Desa Lebani waras', 'B', 'Agak bersih sampai bersih<<22<<10<<2009<<14.00 wib<<30<<5<<6.0'),
(20, '2009-10-23 11:40:33', 107, 'sungai kedung rejo', 'B', 'Kotor<<11<<10<<2009<<09.30 WIB<<29<<6<<4.8'),
(21, '2009-10-23 12:31:44', 107, 'Rejo agung', 'K', '23<<10<<2009<<10.00 WIB<<27<<6,7<<0,5<<0,03<<0,25'),
(22, '2009-10-23 14:17:56', 117, 'sungai kletek', 'B', 'Kotor<<23<<10<<2009<<14.00<<10<<3<<3.3'),
(23, '2009-10-23 15:35:28', 105, 'brantas', 'B', 'Kotor<<23<<10<<2009<<12<<12<<3<<4.0'),
(24, '2009-10-23 16:17:47', 105, 'peceren', 'B', 'Kotor<<23<<10<<2009<<05,55<<12<<4<<3.0'),
(25, '2009-10-23 18:33:13', 101, 'sungai kalimalang', 'B', 'Sedang (rata-rata)<<23<<10<<2009<<16.30<<52<<10<<5.2'),
(26, '2009-10-23 18:48:25', 104, 'sungai kalimalang', 'B', 'Sedang (rata-rata)<<23<<10<<2009<<16.30<<52<<10<<5.2'),
(27, '2009-10-23 18:50:07', 104, 'sungai kalimalang', 'B', 'Kotor<<23<<10<<2009<<16.30<<60<<13<<4.6'),
(28, '2009-10-23 18:50:44', 104, 'sungai tugu', 'B', 'Kotor<<23<<10<<2009<<15.00<<58<<12<<4.8'),
(29, '2009-10-23 19:20:46', 104, 'Kali Talang', 'B', 'Kotor<<23<<10<<2009<<16.00<<26<<7<<3.7'),
(30, '2009-10-23 20:57:02', 104, 'dra', 'B', 'Sedang (rata-rata)<<23<<10<<2009<<daaabre<<197<<35<<5.6'),
(31, '2009-10-23 21:31:22', 54, 'SUNGAI BRANTAS', 'B', 'Sangat Kotor<<18<<10<<2009<<08.00<<11<<4<<2.8'),
(32, '2009-10-24 05:34:25', 104, 'kali sengon', 'B', 'Sedang (rata-rata)<<24<<10<<2009<<16.47<<46<<9<<5.1'),
(33, '2009-10-27 21:50:19', 50, 'brantas', 'B', 'Sangat Kotor<<27<<10<<2009<<10.00<<4<<2<<2.0'),
(34, '2009-10-27 21:51:42', 50, 'brantas', 'B', 'Kotor<<27<<10<<2009<<10.00<<17<<4<<4.3'),
(35, '2009-10-27 22:02:59', 158, 'buper', 'B', 'Kotor<<21<<10<<2009<<08.00<<17<<4<<4.3'),
(36, '2009-10-27 22:49:13', 52, 'jk', 'B', 'Kotor<<27<<10<<2009<<08.00<<9<<3<<3.0'),
(37, '2009-10-29 12:27:40', 107, 'dsn kedung rejo', 'B', 'Kotor<<11<<10<<2009<<09.00-10.30<<27<<6<<4.5'),
(38, '2009-11-01 20:51:28', 139, 'mlirip(sebelum muara pembuangan limbah PT.AJINOMOTO)', 'B', 'Sedang (rata-rata)<<25<<10<<2009<<09.45<<25<<5<<5.0'),
(39, '2009-11-01 20:55:41', 139, ' lokasi ke-2  pembuangan limbah PT.AJINOMOTO di daerah mlirip', 'B', 'Kotor<<25<<10<<2009<<10.00<<19<<4<<4.8'),
(40, '2009-11-01 20:58:38', 139, 'Lokasi ke-3 di daerah sebelum muara pembuangan limbah PT.AJINOMOTO', 'B', 'Agak bersih sampai bersih<<25<<10<<2009<<10.30<<20<<3<<6.7'),
(41, '2009-11-01 21:02:14', 139, 'lokasi ke-1 sesudah pembuangan PT.AJINOMOTO didaerah mlirip', 'B', 'Sedang (rata-rata)<<25<<10<<2009<<11.45<<20<<4<<5.0'),
(42, '2009-11-01 21:04:52', 139, 'lokasi ke-2 sesudah penbuangan PT.AJINOMOTO di daerah mlirip', 'B', 'Agak bersih sampai bersih<<25<<10<<2009<<12.25<<14<<2<<7.0'),
(43, '2009-11-01 21:08:58', 139, 'lokasi ke-1 sebelum ppembuangan limbah PT.AJINOMOTO Di daerah mlirip ', 'B', 'Kotor<<25<<10<<2009<<15.00<<24<<5<<4.8'),
(44, '2009-11-01 21:14:25', 139, 'pengambilan sampel ke-2 sebelum pembuangan limbah PT. AJINOMOTO Di bdaerah mlirip ', 'B', 'Sedang (rata-rata)<<25<<10<<2008<<15.20<<35<<7<<5.0'),
(45, '2009-11-01 21:18:52', 139, 'pengambilan sampel ke-3 muara sebelum pembuangan limbah PT. AJINOMOTO di daerah mlirip', 'B', 'Sedang (rata-rata)<<24<<10<<2009<<15.45<<31<<6<<5.2'),
(46, '2009-11-02 04:01:33', 140, 'Sungai Surabaya,lokasi di desa Krikilan.', 'B', 'Sedang (rata-rata)<<8<<8<<2009<<15.00<<30<<6<<5.0'),
(47, '2009-11-02 04:06:29', 140, 'Sungai Surabaya,lokasi desa Driyoreji', 'B', 'Sedang (rata-rata)<<26<<9<<2009<<15.15<<30<<6<<5.0'),
(48, '2009-11-03 19:37:25', 139, 'pengambilan sampel pertama di daerah perning(sebelum saluran pembuangan limbahPT.AAP)', 'B', 'Sedang (rata-rata)<<25<<10<<2009<<10.00 wib<<25<<5<<5.0'),
(49, '2009-11-03 19:41:19', 139, 'pengmbilan sampel ke-2 didaerah Perning (sebelum pembuangan limbah PT. AAP)', 'B', 'Kotor<<25<<9<<2009<<11.15 wib<<29<<6<<4.8'),
(50, '2009-11-03 19:45:23', 139, 'penganbilan sampel ke-3 di daerah Perning (sebelum pembuangan limbah PT. AAP)', 'B', 'Sedang (rata-rata)<<25<<10<<2009<<12.30<<36<<7<<5.1'),
(51, '2009-11-03 19:48:19', 139, 'pengambilan sampel ke-1 di daerah Perning (sesudah pembuangan limbah PT. AAP)', 'B', 'Kotor<<25<<10<<2009<<12.45<<12<<4<<3.0'),
(52, '2009-11-03 19:50:42', 139, 'pengambilan sampel ke-2 didaerah Pening (sesudah pembuangan limbah PT.AAP)', 'B', 'Kotor<<25<<9<<2009<<14.00<<12<<4<<3.0'),
(53, '2009-11-03 19:53:24', 139, 'pengambilan sampel ke-3 didaerah Perning (sesudah pembuangan limbah PT. AAP)', 'B', 'Kotor<<25<<10<<2009<<14.20<<15<<4<<3.8'),
(54, '2009-11-03 19:56:05', 139, 'pengambilan sampel ke-1 didaerah Perning (sebelum pembuangan limbah PT. AAP)', 'B', 'Kotor<<25<<10<<2009<<15.00<<21<<5<<4.2'),
(55, '2009-11-03 19:59:00', 139, 'pengambilan sampel ke-2 di daerah Perning(sebelum pembuangan limbah PT. AAP)', 'B', 'Agak bersih sampai bersih<<25<<10<<2009<<15.25<<19<<3<<6.3'),
(56, '2009-11-03 20:01:58', 139, 'pengambilan sampel ke-3 didaerah Perning (sebelum pembuangan limbah PT. AAP)', 'B', 'Kotor<<25<<10<<2009<<15.45<<20<<6<<3.3'),
(57, '2009-11-04 12:21:39', 104, 'Kali Sengon di sebelah utara STM Dwija Bhakti Kab. Jombang', 'B', 'Sedang (rata-rata)<<1<<11<<2009<<08.00 WIB<<42<<8<<5.3'),
(58, '2009-11-04 14:56:52', 139, 'pengambilan sampel ke-1 didearah Perning (sesudah pembuangan limbah PT. AAP)', 'B', 'Kotor<<25<<10<<2009<<16.OO<<15<<4<<3.8'),
(59, '2009-11-04 15:01:42', 139, 'pengambilan sampel ke-2 didearah Perning (sesudah pembuangan limbah PT. AAP)', 'B', 'Kotor<<25<<10<<2009<<16.OO<<16<<4<<4.0'),
(60, '2009-11-04 15:04:35', 139, 'pengambilan sampel ke-3 didearah Perning (sesudah pembuangan limbah PT. AAP)', 'B', 'Kotor<<25<<10<<2009<<16.55<<15<<4<<3.8'),
(61, '2009-11-04 15:09:23', 139, 'Sungai Lebaniwaras', 'B', 'Sedang (rata-rata)<<24<<10<<2009<<14.35 wib<<33<<6<<5.5'),
(62, '2009-11-04 15:14:15', 139, 'pengambilan sampel pertama di daerah mlirip', 'B', 'Kotor<<25<<10<<2009<<10.55 wib<<33<<7<<4.7'),
(63, '2009-11-04 15:19:09', 139, 'pengambilan sampel ke-2 di daerah mlirip', 'B', 'Sedang (rata-rata)<<25<<10<<2009<<11.15 wib<<35<<6<<5.8'),
(64, '2009-11-04 15:20:59', 139, 'pengambilan sampel ke-3 di daerah mlirip', 'B', 'Sedang (rata-rata)<<25<<10<<2009<<11.34 wib<<40<<8<<5.0'),
(65, '2009-11-04 15:22:53', 139, 'pengambilan sampel pertama di daerah mlirip', 'B', 'Sedang (rata-rata)<<25<<10<<2009<<15.40 wib<<30<<6<<5.0'),
(66, '2009-11-04 15:24:10', 139, 'pengambilan sampel ke-2 di daerah mlirip', 'B', 'Sedang (rata-rata)<<25<<10<<2009<<16. 25 wib<<17<<3<<5.7'),
(67, '2009-11-04 15:25:13', 139, 'pengambilan sampel ke-3 di daerah mlirip', 'B', 'Kotor<<25<<10<<2009<<16.55wib<<17<<4<<4.3'),
(68, '2009-11-04 15:30:48', 139, 'pengambilan sampel ke-1 di daerah sumengko (sebelum saluran pembuangan limbah PT. ADIPRIMA)', 'B', 'Kotor<<25<<10<<2009<<10.11wib<<17<<4<<4.3'),
(69, '2009-11-04 15:32:27', 139, 'pengambilan sampel ke-2 di daerah sumengko (sebelum saluran pembuangan limbah PT. ADIPRIMA)', 'B', 'Sedang (rata-rata)<<25<<10<<2009<<10.25wib<<20<<4<<5.0'),
(70, '2009-11-04 15:34:52', 139, 'pengambilan sampel ke-3 di daerah sumengko (sebelum saluran pembuangan limbah PT. ADIPRIMA)', 'B', 'Sedang (rata-rata)<<25<<10<<2009<<11.30wib<<17<<3<<5.7'),
(71, '2009-11-04 15:39:57', 139, 'pengambilan sampel ke-3 di daerah sumengko (sebelum sebelum pembuangan limbah PT. ADIPRIMA)', 'B', 'Kotor<<25<<10<<2009<<16.45wib<<22<<6<<3.7'),
(72, '2009-11-04 15:41:22', 139, 'pengambilan sampel ke-2 di daerah sumengko (sebelum sebelum pembuangan limbah PT. ADIPRIMA)', 'B', 'Kotor<<25<<10<<2009<<16.00wib<<10<<3<<3.3'),
(73, '2009-11-04 15:42:59', 139, 'pengambilan sampel ke-1 di daerah sumengko (sebelum sebelum pembuangan limbah PT. ADIPRIMA)', 'B', 'Kotor<<25<<10<<2009<<15.45wib<<10<<3<<3.3'),
(74, '2009-11-04 15:44:08', 139, 'pengambilan sampel ke-1 di daerah sumengko (sebelum sebelum pembuangan limbah PT. ADIPRIMA)', 'B', 'Kotor<<25<<10<<2009<<15.15wib<<19<<4<<4.8'),
(75, '2009-11-04 15:46:41', 139, 'pengambilan sampel ke-2 di daerah sumengko (sebelum saluran pembuangan limbah PT. ADIPRIMA)', 'B', 'Sedang (rata-rata)<<25<<10<<2009<<13.25<<20<<4<<5.0'),
(76, '2009-11-04 15:49:53', 139, 'pengambilan sampel ke-3 di daerah sumengko (sebelum saluran pembuangan limbah PT. ADIPRIMA)', 'B', 'Kotor<<25<<10<<2009<<13.45<<12<<3<<4.0'),
(77, '2009-11-04 15:53:46', 139, 'pengambilan sampel ke-1 didaerah sumberame (sebelum pembuangan limbah DOMESTIK)', 'B', 'Sedang (rata-rata)<<25<<10<<2009<<10.25<<11<<2<<5.5'),
(78, '2009-11-04 15:54:18', 139, 'pengambilan sampel ke-2 didaerah sumberame (sebelum pembuangan limbah DOMESTIK)', 'B', 'Sedang (rata-rata)<<25<<10<<2009<<10.45<<17<<3<<5.7'),
(79, '2009-11-04 15:55:25', 139, 'pengambilan sampel ke-2 didaerah sumberame (sebelum pembuangan limbah DOMESTIK)', 'B', 'Sedang (rata-rata)<<25<<10<<2009<<10.45<<17<<3<<5.7'),
(81, '2009-11-09 12:56:39', 161, 'anak kali surabaya', 'B', 'Kotor<<09<<11<<2009<<11.00 - 12.00<<15<<5<<3.0'),
(82, '2009-11-09 13:16:38', 160, 'selokan jambangan', 'B', 'Kotor<<09<<11<<2009<<11.00 - 12.00<<13<<4<<3.3'),
(83, '2009-11-09 13:28:21', 161, 'sejambanganlokan ', 'B', 'Kotor<<09<<11<<2009<<11.00 - 12.00<<28<<6<<4.7'),
(84, '2009-11-09 13:31:36', 162, 'Selokan jambangan', 'B', 'Kotor<<09<<11<<2009<<11.00 - 12.00<<13<<4<<3.3'),
(85, '2009-11-09 13:34:21', 159, 'selokan jambangan', 'B', 'Kotor<<09<<11<<2009<<11.00 - 12.00<<13<<4<<3.3'),
(86, '2009-11-10 13:13:21', 164, 'Kali Petojo', 'B', 'Kotor<<10<<11<<2009<<11.30-12.30<<9<<3<<3.0'),
(87, '2009-11-10 13:16:49', 165, 'Kali Petojo', 'B', 'Kotor<<10<<11<<2009<<11.30-12.30<<12<<4<<3.0'),
(88, '2009-11-10 13:19:12', 163, 'Kali Petojo', 'B', 'Kotor<<10<<11<<2009<<11.30-12.30<<9<<3<<3.0'),
(89, '2009-11-10 13:25:51', 166, 'Kali Petojo', 'B', 'Kotor<<10<<11<<2009<<11.30-12.30<<12<<4<<3.0'),
(90, '2009-11-11 13:26:43', 166, 'Kali Petojo', 'B', 'Kotor<<11<<11<<2009<<11.30-12.30<<9<<3<<3.0'),
(91, '2009-11-16 12:45:48', 169, 'SUNGAI KALI PAPAT', 'B', 'Kotor<<16<<11<<2009<<11.00 - 12.00<<25<<6<<4.2'),
(92, '2009-11-16 14:06:40', 170, 'SUNGAI KALI PAPAT', 'B', 'Kotor<<16<<11<<2009<<11.00 - 12.00<<31<<7<<4.4'),
(93, '2009-11-17 11:22:49', 174, 'KALI KASBAH', 'B', 'Sangat Kotor<<17<<11<<2009<<12.00-13.00<<10<<4<<2.5'),
(94, '2009-11-17 14:10:30', 174, 'KALI KASBAH', 'B', 'Kotor<<17<<11<<2009<<12.00-13.00<<15<<4<<3.8'),
(95, '2009-11-20 10:24:44', 104, 'SUNGAI K. BRANTAS TAMBANGAN KESAMBEN X-3 G1', 'B', 'Kotor<<20<<11<<2009<<09.00-10.00<<57<<12<<4.8'),
(96, '2009-11-23 12:06:30', 54, 'SUNGAI NGAGLIK', 'B', 'Sangat Kotor<<23<<11<<2009<<09.00-10.00<<9<<4<<2.3'),
(97, '2009-11-23 12:15:37', 179, 'SUNGAI KASIN', 'B', 'Kotor<<23<<11<<2009<<09.00-10.00<<12<<4<<3.0'),
(98, '2009-11-23 13:09:44', 58, 'SUNGAI KASIN', 'B', 'Sedang (rata-rata)<<23<<11<<2009<<09.00-10.00<<22<<4<<5.5'),
(99, '2010-07-29 11:22:07', 78, 'K. Brantas-Pendem', 'B', 'Kotor<<29<<07<<2010<<11.00<<18<<5<<3.6'),
(100, '2010-08-07 10:49:07', 153, 'kali madiun', 'B', 'Kotor<<07<<08<<2010<<10.00 - 11.00<<15<<5<<3.0'),
(101, '2010-10-28 10:46:06', 104, 'gumulan', 'K', '28<<10<<2010<<07.00 sd 08.00<<25<<6<<4<<3<<6'),
(102, '2010-10-28 10:49:06', 104, 'gumulan', 'B', 'Kotor<<28<<10<<2010<<07.00 sd 08.00<<58<<14<<4.1'),
(103, '2010-12-08 14:23:29', 105, 'kali cokenongo', 'B', 'Kotor<<08<<12<<2010<<13.40<<25<<7<<3.6'),
(104, '2010-12-08 14:29:18', 110, 'kali cokenongo', 'B', 'Kotor<<08<<12<<2010<<13.40<<18<<6<<3.0'),
(105, '2010-12-08 14:32:19', 106, 'kali cokenongo', 'B', 'Kotor<<08<<12<<2010<<13.40<<14<<4<<3.5'),
(106, '2011-05-11 15:54:44', 104, 'Kali Jombang kulon', 'K', '11<<05<<2011<<09.00 sd 10.30<<25<<6.75<<5<<3<<10'),
(107, '2011-05-11 16:01:45', 104, 'Kali Jombang kulon', 'B', 'Kotor<<11<<05<<2011<<09.00 sd 10.00<<51<<11<<4.6'),
(108, '2011-05-12 00:31:37', 50, 'Selorejo', 'K', '11<<05<<2011<<10.00<<20<<7<<5<<67<<2'),
(109, '2011-05-24 07:19:58', 66, 'Kali sari (anak sungai Brantas)', 'B', 'Sedang (rata-rata)<<20<<05<<2011<<13.00-15.30<<36<<7<<5.1'),
(110, '2011-07-22 22:15:31', 66, 'Kali sari Wendit', 'B', 'Sedang (rata-rata)<<21<<07<<2011<<14.00-16.00<<47<<8<<5.9'),
(111, '2011-07-27 18:41:58', 89, 'Desa Jeblog Kec. Talun Kab. Blitar', 'B', 'Sedang (rata-rata)<<27<<6<<2011<<08.00-10.00<<56<<11<<5.1'),
(112, '2011-10-07 12:07:49', 149, 'sungai ds pelempayung', 'B', 'Sedang (rata-rata)<<07<<10<<2011<<13.00-14.00<<36<<7<<5.1'),
(113, '2011-10-09 10:56:58', 151, 'Kali Madiun', 'B', 'Sedang (rata-rata)<<6<<10<<2011<<10.30-12.00<<51<<10<<5.1'),
(114, '2011-10-17 14:20:46', 67, 'sungai brantas', 'B', 'Sedang (rata-rata)<<7<<10<<2011<<09.30<<77<<15<<5.1'),
(115, '2011-10-26 14:39:01', 153, 'sungai sesek', 'B', 'Agak bersih sampai bersih<<21<<10<<2011<<15.00<<84<<11<<7.6'),
(116, '2011-10-28 08:47:29', 151, 'sungai mejayan', 'B', 'Sedang (rata-rata)<<16<<10<<2011<<08.00-11.00<<80<<14<<5.7'),
(117, '2011-10-31 23:28:09', 66, 'DAS Jagung Suprapto', 'B', 'Sedang (rata-rata)<<15<<10<<2011<<14.00 - 16.00<<46<<8<<5.8'),
(118, '2011-10-31 23:33:34', 66, 'Spindit Kota Mlang', 'B', 'Sedang (rata-rata)<<22<<10<<2011<<14.00 - 16.00<<26<<5<<5.2'),
(119, '2011-10-31 23:39:23', 66, 'DAS di Jalan Brantas', 'B', 'Agak bersih sampai bersih<<29<<10<<2011<<14.00-16.00<<38<<6<<6.3'),
(120, '2011-10-31 23:41:12', 66, 'DAS di Jl. Brantas', 'B', 'Kotor<<29<<10<<2011<<14.00-16.00<<17<<4<<4.3'),
(121, '2011-11-01 04:58:24', 63, 'betek, malang', 'B', 'Kotor<<26<<10<<2011<<16.00<<36<<9<<4.0'),
(122, '2012-02-11 14:07:45', 153, 'Desa Banaran, Dusun Norame, Geger', 'B', 'Agak bersih sampai bersih<<29<<1<<2012<<08.00-10.00<<31<<5<<6.2'),
(123, '2012-02-11 14:08:08', 153, 'banaran', 'B', 'Sedang (rata-rata)<<29<<1<<2012<<08.00<<40<<8<<5.0'),
(124, '2012-02-11 14:13:48', 153, 'Desa Banaran, Dusun Norame, Geger', 'B', 'Sedang (rata-rata)<<29<<1<<2012<<08.00<<50<<10<<5.0'),
(125, '2012-02-15 19:26:04', 119, 'Sungai kenjeran', 'B', 'Kotor<<13<<02<<2012<<12<<12<<3<<4.0'),
(126, '2012-02-15 19:33:40', 119, 'Sungai kenjeran', 'B', 'Sedang (rata-rata)<<13<<02<<2012<<12<<155<<27<<5.7'),
(127, '2012-02-15 19:33:55', 119, 'Sungai kenjeran', 'B', 'Sedang (rata-rata)<<13<<02<<2012<<12<<197<<35<<5.6'),
(128, '2012-02-15 19:40:33', 163, 'Sungai kenjeran', 'B', 'Sedang (rata-rata)<<14<<02<<2012<<15<<15<<3<<5.0'),
(129, '2012-02-28 14:34:40', 53, 'kali ceweng', 'B', 'Sedang (rata-rata)<<28<<02<<2012<<342764371235<<25<<5<<5.0'),
(130, '2012-03-01 16:27:02', 105, 'Sungai Kedawong', 'B', 'Sedang (rata-rata)<<29<<2<<2012<<11.00-12.00<<23<<4<<5.8'),
(131, '2012-03-09 16:03:21', 105, 'Sungai Sawonggaling', 'B', 'Kotor<<09<<03<<2012<<16.00<<19<<4<<4.8'),
(132, '2012-03-09 16:07:18', 105, 'Sungai Depan SMK DB', 'B', 'Kotor<<09<<03<<2012<<16.00<<28<<6<<4.7'),
(133, '2012-05-12 09:21:06', 147, 'Kali Catur', 'B', 'Kotor<<12<<05<<2012<<10.00 - 12.00<<29<<7<<4.1'),
(134, '2012-05-12 09:51:43', 104, 'Kali Jombang Kulon', 'K', '12<<05<<2012<<10.00 - 12.00<<27<<6.5<<5<<1<<1'),
(135, '2012-05-16 08:22:05', 209, 'Sungai Pengkol', 'B', 'Kotor<<16<<05<<2012<<08.00<<52<<12<<4.3'),
(136, '2012-05-16 08:43:38', 228, 'Kali Catur', 'B', 'Kotor<<12<<05<<2012<<10.00<<38<<8<<4.8'),
(137, '2012-05-16 08:51:14', 220, 'Kali Catur', 'B', 'Agak bersih sampai bersih<<12<<05<<2012<<10.00<<61<<10<<6.1'),
(138, '2012-06-05 17:50:10', 88, 'sungai pabrik tiwul desa jatilengger', 'B', 'Sedang (rata-rata)<<25<<5<<2012<<08.00<<49<<9<<5.4'),
(139, '2012-06-24 20:43:51', 105, 'Danau Ngebel Ponorogo', 'B', 'Sedang (rata-rata)<<20<<06<<2012<<13.00<<43<<8<<5.4'),
(140, '2012-06-24 20:49:50', 105, 'Air Panas Ngebel', 'B', 'Kotor<<20<<06<<2012<<15.00<<6<<2<<3.0'),
(141, '2012-07-31 22:42:26', 209, 'sungai', 'B', 'Sangat Bersih<<31<<07<<2012<<12.00<<8<<1<<8.0'),
(142, '2014-03-19 17:01:58', 104, 'KALI JOMBANG KULON', 'B', 'Agak bersih sampai bersih<<19<<03<<2014<<09.00 - 11.00<<56<<8<<7.0'),
(143, '2014-03-19 17:06:54', 119, 'Kali Mas', 'K', '19<<03<<2014<<09.00 - 11.00<<24<<5<<3<<7<<5'),
(144, '2014-03-24 21:30:35', 104, 'Kali Brantas Kesamben', 'B', 'Kotor<<24<<03<<2014<<09.00 - 11.00<<60<<13<<4.6'),
(145, '2014-10-20 00:17:56', 104, 'Sungai Brantas Kesamben', 'B', 'Kotor<<3<<10<<2014<<08.00 sd 10.00<<23<<6<<3.8'),
(146, '2015-04-07 15:16:47', 104, 'sungai kali brantas ploso', 'B', 'Kotor<<07<<04<<2015<<08.45 s/d 10.00<<42<<10<<4.2'),
(147, '2015-06-15 23:55:38', 55, 'brantas', 'B', 'Kotor<<15<<06<<2015<<10:00-13:00<<20<<5<<4.0'),
(148, '2015-06-16 11:54:13', 149, 'kare', 'B', 'Kotor<<16<<06<<2015<<11.00-12.00<<23<<5<<4.6'),
(149, '2015-06-16 11:59:49', 235, 'kare', 'K', '16<<06<<2015<<11.00-12.00<<30<<7<<6<<2<<2'),
(150, '2015-07-11 09:41:04', 104, 'keplaksari', 'B', 'Kotor<<11<<07<<2015<<10.30 - 11.00<<25<<6<<4.2'),
(151, '2015-07-11 09:52:34', 104, 'Dam Kayangan', 'K', '11<<07<<2015<<08.00 - 09.00<<25<<7<<2<<5<<8'),
(152, '2015-08-22 14:53:29', 104, 'Ngoro jombang', 'K', '22<<08<<2015<<09.00 - 10.00<<28<<7<<7<<5<<20'),
(153, '2015-11-24 20:23:23', 62, 'jon', 'B', 'Sangat Kotor<<24<<11<<2015<<20.00<<18<<7<<2.6'),
(154, '2016-08-12 10:13:03', 238, 'sungai mejayan', 'B', 'Kotor<<11<<08<<2016<<11.30-11.45<<53<<11<<4.8'),
(155, '2016-09-06 10:51:22', 104, 'Kali Brantas Kesamben', 'K', '06<<09<<2016<<10.00-11.00<<27<<7,5<<10<<5<<15'),
(156, '2016-09-06 10:53:35', 50, 'Metro Brantas', 'B', 'Kotor<<06<<09<<2016<<10.00-11.00<<44<<9<<4.9');

-- --------------------------------------------------------

--
-- Table structure for table `picture`
--

CREATE TABLE `picture` (
  `id_pict` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `lit_pict` varchar(45) NOT NULL,
  `big_pict` varchar(45) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `picture`
--

INSERT INTO `picture` (`id_pict`, `title`, `lit_pict`, `big_pict`) VALUES
(1, 'Jelajah sungai Kali Brantas sambil penelitian', '../galeri/brantas1.jpg', '../galeri/brantas1_big.jpg'),
(2, 'Jelajah sungai Kali Brantas sambil penelitian', '../galeri/brantas2.jpg', '../galeri/brantas2_big.jpg'),
(3, 'Dialog interaktif di Bandung dalam rangka HUT', '../galeri/pusair1.jpg', '../galeri/pusair1_big.jpg'),
(4, 'Dialog interaktif di Bandung dalam rangka HUT', '../galeri/pusair2.jpg', '../galeri/pusair2_big.jpg'),
(5, 'Wakil JKPKA dalam peringatan Hari Air Sedunia', '../galeri/mexico1.jpg', '../galeri/mexico1_big.jpg'),
(6, 'Wakil JKPKA dalam peringatan Hari Air Sedunia', '../galeri/mexico2.jpg', '../galeri/mexico2_big.jpg'),
(7, 'Peringatan Hari Air Sedunia di Mexico', '../galeri/mexico3.jpg', '../galeri/mexico3_big.jpg'),
(8, 'Pelatihan E-Learning bagi Guru', '../galeri/late_guru1.jpg', '../galeri/late_guru1_big.jpg'),
(9, 'Pelatihan E-Learning bagi Guru', '../galeri/late_guru2.jpg', '../galeri/late_guru2_big.jpg'),
(10, 'Pelatihan E-Learning bagi Siswa', '../galeri/late_siswa1.jpg', '../galeri/late_siswa1_big.jpg'),
(11, 'Pelatihan E-Learning bagi Siswa', '../galeri/late_siswa2.jpg', '../galeri/late_siswa2_big.jpg'),
(12, 'Perkemahan Tirta Bhakti', '../galeri/pekerti1.jpg', '../galeri/pekerti1_big.jpg'),
(13, 'Perkemahan Tirta Bhakti', '../galeri/pekerti2.jpg', '../galeri/pekerti2_big.jpg'),
(15, 'Penyerahan hadiah Lomba Pelestarian Sumber Da', '../galeri/penyerahan.jpg', '../galeri/penyerahan_big.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`id_anggota`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`kodeberita`);

--
-- Indexes for table `pantau`
--
ALTER TABLE `pantau`
  ADD PRIMARY KEY (`id_pantau`);

--
-- Indexes for table `picture`
--
ALTER TABLE `picture`
  ADD PRIMARY KEY (`id_pict`);


--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anggota`
--
ALTER TABLE `anggota`
  MODIFY `id_anggota` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=297;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `kodeberita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `pantau`
--
ALTER TABLE `pantau`
  MODIFY `id_pantau` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=157;

--
-- AUTO_INCREMENT for table `picture`
--
ALTER TABLE `picture`
  MODIFY `id_pict` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
