$(document).ready(function() {
   	$("#otherSurroundingEnvironment").attr("disabled", "");
   	$('#part1Button').on('click', function() {
   		$("#part2").removeClass("hide");
   		$("#part1Button").addClass("hide");
   	});
   	$('#part2Button').on('click', function() {
   		$("#part3").removeClass("hide");
   		$("#part2Button").addClass("hide");
   	});
   	$('#part3Button').on('click', function() {
   		$("#part4").removeClass("hide");
   		$("#part3Button").addClass("hide");
   	});
   	$("#otherSurroundingEnvironment").blur(function() {
        $("#otherSurroundingEnvironmentRadio").val($(this).val());
    });
    $('#otherSurroundingEnvironmentRadio').on('ifChecked', function() {
        $("#otherSurroundingEnvironment").removeAttr("disabled");
    });
    $('#otherSurroundingEnvironmentRadio').on('ifUnchecked', function() {
        $("#otherSurroundingEnvironment").attr("disabled", "");
        $("#otherSurroundingEnvironment").val("");
    });
});
