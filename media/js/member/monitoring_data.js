$(document).ready(function() {
    $('#post_list').DataTable( {
        "processing": true,
        "serverSide": true,
        "columns": [
            { "data": "date" },
            { "data": "location" },
            { "data": "biology", "orderable": false, "searchable": false },
            { "data": "chemistry", "orderable": false, "searchable": false },
            { "data": "menu", "orderable": false, "searchable": false }
        ],
        "order": [[ 0, "desc" ]],
        "ajax": {
            "url": base_url + "member/Monitoring/data",
            "type": "POST"
        }
            
    } );
} );
