$(document).ready(function() {
    $('#post_list').DataTable( {
        "processing": true,
        "serverSide": true,
        "columns": [
            { "data": "name" },
            { "data": "surface" },
            { "data": "image", "orderable": false, "searchable": false },
            { "data": "menu", "orderable": false, "searchable": false }
        ],
        "ajax": {
            "url": base_url + "member/Location/data",
            "type": "POST"
        }
            
    } );
} );
