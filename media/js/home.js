function homeAbout(id, content) {
	console.log(id);
	$(".home-about .nav-link").removeClass("active");
	$("#" + id).addClass("active");
	$(".home-about-content").html(content);
	return false;
}

$(document).ready(function(){
	$('.home-moto').slick({
		infinite: true,
		dots: true,
		slidesToShow: 4,
		slidesToScroll: 4,
		height: 100,
		autoplay: true,
		autoplaySpeed: 2000,
		responsive: [
			{
			breakpoint: 992,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
				dots: true
			}
			},
			{
			breakpoint: 768,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2,
				dots: true
			}
			},
			{
			breakpoint: 576,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				dots: true
			}
			}
		]
	});
});