$(document).ready(function() {
    console.log(dataFooter);
    $("#date").datepicker( {
        format: "yyyy-mm",
        viewMode: "months", 
        minViewMode: "months"
    });
    $('#post_list').DataTable( {
        "processing": true,
        "serverSide": true,
        "order": [[ 0, "desc" ]],
        "columns": [
        { "data": "date" },
        { "data": "name" },
        { "data": "biology", "orderable": false, "searchable": false },
        { "data": "chemistry", "orderable": false, "searchable": false },
        { "data": "menu", "orderable": false, "searchable": false }
        ],
        "ajax": {
            "url": base_url + "admin/Dashboard/data",
            "type": "POST"
        }

    } );
    Morris.Line({
      element: 'line-example',
      data: dataFooter.monitoringGraph.query,
      xkey: dataFooter.monitoringGraph.xkey,
      ykeys: dataFooter.monitoringGraph.ykeys,
      labels: dataFooter.monitoringGraph.labels
  });
} );
