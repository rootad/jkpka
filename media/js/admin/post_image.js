$(document).ready(function() {
	// bootbox.confirm("This is the default confirm!", function(result){ console.log('This was logged in the callback: ' + result); });
    $('#image_list').DataTable( {
        "processing": true,
        "serverSide": true,
        "columns": [
            { "data": "ID", "searchable": false },
            { "data": "post_title" },
            { "data": "image", "orderable": false, "searchable": false },
            { "data": "guid" },
            { "data": "menu", "orderable": false, "searchable": false }
        ],
        "order": [[ 0, "desc" ]],
        "ajax": {
            "url": base_url + "admin/data/PostImage/data",
            "type": "POST"
        }
	        
    } );
} );