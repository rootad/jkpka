$(document).ready(function() {
    $('#post_list').DataTable( {
        "processing": true,
        "serverSide": true,
        "columns": [
            { "data": "nama_anggota" },
            { "data": "wilayah" },
            { "data": "almt_sekolah" },
            { "data": "telp_anggota" },
            { "data": "kp_anggota" },
            { "data": "menu", "orderable": false, "searchable": false }
        ],
        "order": [[ 0, "asc" ]],
        "ajax": {
            "url": base_url + "admin/School/data",
            "type": "POST"
        }
            
    } );
} );
