$(document).ready(function() {
    
	$('#post_list').DataTable( {
        "processing": true,
        "serverSide": true,
        "columns": [
            { "data": "post_title" },
            { "data": "post_date" },
            { "data": "menu", "orderable": false, "searchable": false }
        ],
        "ajax": {
            "url": base_url + "admin/data/PostData/data",
            "type": "POST"
        }
            
    } );
} );
