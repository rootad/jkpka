$(document).ready(function() {
    $("#otherSurface").attr("disabled", "");
    $("#map").css("display", "none");

    var x = document.getElementById("location_info");
    function getLocation() {
        $('#location_radio').iCheck('disable');
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition, showError);
        } else {
            x.innerHTML = "(Browser ini tidak mendukung geolocation.)";
        }
    }

    function showPosition(position) {
        $('#location_radio').iCheck('enable');
        $('#location_radio').iCheck('check');
        var uluru = {lat: position.coords.latitude, lng: position.coords.longitude};
		map = new google.maps.Map(document.getElementById('map'), {
		  center: uluru,
		  zoom: 18,
		  mapTypeId: 'satellite'
		});
		var marker = new google.maps.Marker({
			position: uluru,
			map: map,
			title: ''
		});
        x.innerHTML = "(Latitude: " + position.coords.latitude +
        "<br>Longitude: " + position.coords.longitude + ")";
        $("#locationLon").val(position.coords.longitude);
        $("#locationLat").val(position.coords.latitude);
    }

    function showError(error) {
        $("#location_radio").attr("disabled", "");
        switch(error.code) {
            case error.PERMISSION_DENIED:
                x.innerHTML = "(Browser ini tidak mendukung geolocation.)"
                break;
            case error.POSITION_UNAVAILABLE:
                x.innerHTML = "(Informasi lokasi tidak tersedia.)"
                break;
            case error.TIMEOUT:
                x.innerHTML = "(Gagal mendapatkan lokasi karena tidak ada respon.)"
                break;
            case error.UNKNOWN_ERROR:
                x.innerHTML = "(Gagal mendapatkan lokasi.)"
                break;
        }
    }

    var tempLon = $("#locationLon").val();
    var tempLat = $("#locationLat").val();
    var surface = $("#otherSurfaceRadio").val();

    if (surface.length > 0) {
        $("#otherSurface").removeAttr("disabled");
    }
    if (tempLon.length > 0 && tempLat.length > 0) {
        $('#map_radio').iCheck('check');
        $("#map").css("display", "block");
        homeMap(parseFloat(tempLat), parseFloat(tempLon), true);
    } else {
        getLocation();
    }

    $("#otherSurface").blur(function() {
        $("#otherSurfaceRadio").val($(this).val());
    });
    $('#otherSurfaceRadio').on('ifChecked', function() {
        console.log("ifChecked");
        $("#otherSurface").removeAttr("disabled");
    });
    $('#otherSurfaceRadio').on('ifUnchecked', function() {
        console.log("ifUnchecked");
        $("#otherSurface").attr("disabled", "");
        $("#otherSurface").val("");
    });
    $('#map_radio').on('ifChecked', function() {
        homeMap();
        $("#map").css("display", "block");
    });
    $('#location_radio').on('ifChecked', function() {
        $("#map").css("display", "block");
    });

    function homeMap(lat = -7.2646818, lng = 112.7510508, addMarker = false) {
        console.log(lat);
        console.log(lng);
        var uluru = {lat: lat, lng: lng};
		map = new google.maps.Map(document.getElementById('map'), {
		  center: uluru,
		  zoom: 14,
		  mapTypeId: 'satellite'
		});
		var marker = null;

        if (addMarker) {
            marker = new google.maps.Marker({
                position: uluru, 
                map: map
            });
        }

        google.maps.event.addListener(map, 'click', function(event) {
           placeMarker(event.latLng);
        });

        function placeMarker(location) {
            $("#locationLon").val(location.lng());
            $("#locationLat").val(location.lat());
            if (marker != null) {
                marker.setMap(null);
            }
            marker = new google.maps.Marker({
                position: location, 
                map: map
            });
            map.panTo(location);
        }
	}
});
