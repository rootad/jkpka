$(document).ready(function() {
    $('#post_list').DataTable( {
        "processing": true,
        "serverSide": true,
        "columns": [
            { "data": "date" },
            { "data": "name" },
            { "data": "biology", "orderable": false, "searchable": false },
            { "data": "chemistry", "orderable": false, "searchable": false },
            { "data": "menu", "orderable": false, "searchable": false }
        ],
        "ajax": {
            "url": base_url + "admin/Monitoring/data",
            "type": "POST"
        }
            
    } );
} );
