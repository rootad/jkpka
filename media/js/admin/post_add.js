$(document).ready(function() {
	$('#image_list').DataTable( {
        "processing": true,
        "serverSide": true,
        "columns": [
            { "data": "post_title" },
            { "data": "image", "orderable": false, "searchable": false },
            { "data": "guid" }
        ],
        "ajax": {
            "url": base_url + "admin/data/PostImage/data",
            "type": "POST"
        }
	        
    } );
	CKEDITOR.replace( 'post_content' );
	// initSample();
	// CKEDITOR.config.removePlugins = 'image';
} );
